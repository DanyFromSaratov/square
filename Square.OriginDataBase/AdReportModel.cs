//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Square.OriginDataBase
{
    using System;
    using System.Collections.Generic;
    
    public partial class AdReportModel
    {
        public int AdReportID { get; set; }
        public int AdID { get; set; }
        public Nullable<bool> AdFromRealtor { get; set; }
        public Nullable<bool> AdLeased { get; set; }
        public Nullable<bool> AdIncorrectData { get; set; }
        public Nullable<bool> IncorrectPhoneNumber { get; set; }
    }
}
