﻿using System;
using System.Collections.Generic;

namespace Square.AvitoParser
{
    public class AvitoXlsx
    {
        public string Title { get; set; }

        public string PhoneNumber { get; set; }

        public string Fio { get; set; }

        public bool IsOwner { get; set; }

        public string Description { get; set; }

        public double Price { get; set; }

        public string Metro { get; set; }

        public string Address { get; set; }

        public int RoomsCount { get; set; }

        public double Longtitude { get; set; }

        public double Latitude { get; set; }

        public List<string> ImgLinks { get; set; }
    }
}
