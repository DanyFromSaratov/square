﻿using ExcelDataReader;
using Square.Common.Entities.Ad;
using Square.Common.Entities.Common;
using Square.Services.Implementation.Repositories;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Square.AvitoParser
{
    public class AvitoParser
    {
        private const string originalFileName = @"C:\temp\avito_Prod_251220.xlsx";

        public async Task ParseAsync()
        {
            using (var stream = File.Open(originalFileName, FileMode.Open, FileAccess.Read))
            {
                IExcelDataReader reader;

                System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
                reader = ExcelDataReader.ExcelReaderFactory.CreateReader(stream);

                var conf = new ExcelDataSetConfiguration
                {
                    ConfigureDataTable = _ => new ExcelDataTableConfiguration
                    {
                        UseHeaderRow = true
                    }
                };

                var dataSet = reader.AsDataSet(conf);

                var dataTable = dataSet.Tables[0];

                var list = new List<AvitoXlsx>();

                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    var avitoAd = new AvitoXlsx();

                    avitoAd.PhoneNumber = dataTable.Rows[i]["Телефон"] != DBNull.Value
                        ? (string)dataTable.Rows[i]["Телефон"]
                        : null;


                    try
                    {
                        avitoAd.Description = dataTable.Rows[i]["Описание"] != DBNull.Value
                            ? (string)dataTable.Rows[i]["Описание"]
                            : null;
                    }
                    catch (Exception)
                    {
                        continue;
                    }


                    avitoAd.Fio = dataTable.Rows[i]["Продавец"] != DBNull.Value
                        ? (string)dataTable.Rows[i]["Продавец"]
                        : null;

                    avitoAd.IsOwner = dataTable.Rows[i]["Тип продавца"] != DBNull.Value
                        ? (string)dataTable.Rows[i]["Тип продавца"] == "Арендодатель" ? true : false
                        : false;



                    if (dataTable.Rows[i]["Адрес"] != DBNull.Value)
                    {
                        var address = (string)dataTable.Rows[i]["Адрес"];
                        address = address.Replace("Москва, ", "");

                        if (address == null)
                        {
                            continue;
                        }
                        avitoAd.Address = address;
                    }
                    else
                    {
                        avitoAd.Address = null;
                        continue;
                    }

                    avitoAd.Price = dataTable.Rows[i]["Цена"] != DBNull.Value
                        ? (double)dataTable.Rows[i]["Цена"]
                        : 0;


                    if (dataTable.Rows[i]["Метро"] != DBNull.Value)
                    {
                        var metro = (string)dataTable.Rows[i]["Метро"];
                        metro = metro.Replace("м. ", "");
                        avitoAd.Metro = metro;
                    }
                    else
                    {
                        avitoAd.Metro = null;
                    }


                    if (dataTable.Rows[i]["Ссылки на картинки"] != DBNull.Value)
                    {
                        var linksImgs = (string)dataTable.Rows[i]["Ссылки на картинки"];

                        avitoAd.ImgLinks = linksImgs.Replace(" ", "").Split(",").ToList();

                    }
                    else
                    {
                        avitoAd.ImgLinks = null;
                    }

                    string roomsCountStr = dataTable.Rows[i]["Параметры"] != DBNull.Value
                        ? (string)dataTable.Rows[i]["Параметры"]
                        : null;
                    if (!string.IsNullOrEmpty(roomsCountStr))
                    {
                        var ar = roomsCountStr.Split(";");
                        var room = ar.FirstOrDefault(r => r.Contains("Количество комнат"));
                        if (!string.IsNullOrEmpty(room))
                        {
                            var temp = room.Split(":");
                            var roomCountParsedStr = temp[1].Trim();
                            int.TryParse(roomCountParsedStr, out int roomc);

                            if (roomc != 0)
                            {
                                avitoAd.RoomsCount = roomc;
                            }
                            else
                            {
                                avitoAd.RoomsCount = 1;
                            }

                        }
                        else
                        {
                            avitoAd.RoomsCount = 1;
                        }
                    }
                    else
                    {
                        avitoAd.RoomsCount = 1;
                    }



                    list.Add(avitoAd);
                }

                list = list.Where(l => l.IsOwner
                && l.RoomsCount > 0 
                && l.ImgLinks != null
                && l.Description.Length < 2000).ToList();

                var ads = await MapAvitoAdsToAds(list);
                var r = new AdRepository();

                var phones = await r.GetAllPhones();
                phones = phones
                    .Select(p => p.Replace("-", "").Replace("(", "").Replace(")", ""))
                    .ToList();

                ads = ads
                    .Where(adInfo => !phones.Contains(adInfo.Phone.PhoneNumber))
                    .ToList();

                foreach (var ad in ads)
                {
                    await r.AddAvitoAdAsync(ad);
                }

            }
        }

        public async Task<List<Ad>> MapAvitoAdsToAds(List<AvitoXlsx> avitoAds)
        {
            var r = new AdRepository();
            var c = new CityRepository();

            var cities = await c.GetCitiesAsync();
            var city = cities.First();
            var metroes = city.Districts;

            var ads = new List<Ad>();
            var count = 0;

            foreach (var avitoAd in avitoAds)
            {
                var metro = metroes.FirstOrDefault(m => m.Name == avitoAd.Metro);
                if (metro == null)
                {
                    continue;
                }

                var ad = new Ad
                {
                    Title = avitoAd.Address,
                    Description = avitoAd.Description,
                    Phone = new AdPhone
                    {
                        PhoneNumber = avitoAd.PhoneNumber
                    },
                    Fio = avitoAd.Fio,
                    Price = (int)avitoAd.Price,
                    RoomsCount = avitoAd.RoomsCount,
                    Comfort = new Comfort
                    {
                        Internet = true,
                        Bath = true,
                        Fridge = true,
                        KitchenFurniture = true,
                        Washer = true
                    },
                    IsAvito = true,

                    ImageAttachments = avitoAd.ImgLinks
                        .Select(item => new ImageAttachment
                        {
                            FileName = item
                        })
                        .ToList(),

                    LocationInfo = new AdLocationInfo
                    {
                        CityID = 1,
                        DistrictID = metro.DistrictID
                    }
                };

                var ia = ad.ImageAttachments.First();
                ia.IsMain = true;

                ads.Add(ad);
            }

            return ads;
        }
    }
}
