﻿using System.Collections.Generic;

namespace Square.Common.AdEntities
{
    /// <summary>
    /// Контакт (люди, с которыми можно связать и получить ниформацию).
    /// </summary>
    public class Contact
    {
        /// <summary>
        /// Идентификатор.
        /// </summary>
        public int ContactID { get; set; }

        /// <summary>
        /// Фамилия.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Имя.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Отчество.
        /// </summary>
        public string MiddleName { get; set; }

        /// <summary>
        /// Список контактных телефонов.
        /// </summary>
        public IEnumerable<Phone> ContactPhones { get; set; }

        /// <summary>
        /// Должность.
        /// </summary>
        public string Rank { get; set; }

        /// <summary>
        /// Email.
        /// </summary>
        public string Email { get; set; }
    }
}
