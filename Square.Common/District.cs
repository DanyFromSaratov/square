﻿namespace Square.Common.AdEntities
{
    public class District
    {
        public int DistrictID { get; set; }

        public string Name { get; set; }
    }
}
