﻿namespace Square.Common
{
    public class User
    {
        public int UserID { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public string PhoneNumber { get; set; }
    }
}
