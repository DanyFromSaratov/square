﻿using System.Collections.Generic;

namespace Square.Common.AdEntities
{
    public class City
    {
        public int CityID { get; set; }

        public string Name { get; set; }

        public IEnumerable<District> Districts { get; set; }
    }
}
