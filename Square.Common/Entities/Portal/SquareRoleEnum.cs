﻿namespace Square.Common.Entities.Portal
{
    /// <summary>
    /// Роль.
    /// </summary>
    public enum SquareRoleEnum
    {
        /// <summary>
        /// Администратор(имеет полный доступ).
        /// </summary>
        Admin = 1,

        /// <summary>
        /// Пользователь(имеет ограниченный доступ).
        /// </summary>
        User = 2,

        /// <summary>
        /// Подписант(имеет доступ по подписке).
        /// </summary>
        Subscriber = 4,

        Guest = 5,
    }
}