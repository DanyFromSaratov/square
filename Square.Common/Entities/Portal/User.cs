﻿using System;

namespace Square.Common.Entities.Portal
{
    /// <summary>
    /// Пользователь.
    /// </summary>
    public class User
    {
        /// <summary>
        /// Идентификатор.
        /// </summary>
        public int UserID { get; set; }

        /// <summary>
        /// Фамилия.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Имя.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Email.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Пароль.
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Номер телефона (используется в качестве логина).
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Роль пользователя.
        /// </summary>
        public Role Role { get; set; }

        /// <summary>
        /// Текущая подписка (опциональна).
        /// </summary>
        public Subscription CurrentSubscription { get; set; }

        public int OrderNumber { get; set; }

        public DateTime RegistrationDate { get; set; }

        public string NewPhoneNumber { get; set; }

        public string MainImage { get; set; }
    }
}
