﻿namespace Square.Common.Entities.Portal
{
    public class Favorite
    {
        public int FavoriteID { get; set; }

        public Ad.Ad Ad { get; set; }

        public int UserID { get; set; }

        public int AdID { get; set; }
    }
}
