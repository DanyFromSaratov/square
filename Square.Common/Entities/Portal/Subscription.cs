﻿using System;

namespace Square.Common.Entities.Portal
{
    /// <summary>
    /// Подписка.
    /// </summary>
    public class Subscription
    {
        /// <summary>
        /// Идентификатор.
        /// </summary>
        public int SubscriptionID { get; set; }

        /// <summary>
        /// Тип подписки(базовый, оптимальный, максимальный).
        /// </summary>
        public SubscriptionEnum Type { get; set; }

        /// <summary>
        /// Идентификатор пользователя(кому принадлежит текущая подписка).
        /// </summary>
        public int UserID { get; set; }

        /// <summary>
        /// Заголовок.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Цена.
        /// </summary>
        public int Price { get; set; }

        /// <summary>
        /// Порядок сортировки.
        /// </summary>
        public int SortOrder { get; set; }

        /// <summary>
        /// Дата начала действия.
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Дата окончания действия.
        /// </summary>
        public DateTime? EndDate { get; set; }

        public string Info { get; set; }

        public int OrderNumber { get; set; }

        public bool IsActive { get; set; }
    }
}
