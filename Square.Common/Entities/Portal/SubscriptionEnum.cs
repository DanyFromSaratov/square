﻿namespace Square.Common.Entities.Portal
{
    /// <summary>
    /// Тип подписки.
    /// </summary>
    public enum SubscriptionEnum
    {
        /// <summary>
        /// Базовый.
        /// </summary>
        Base = 1,

        /// <summary>
        /// Оптимальный.
        /// </summary>
        Optimal = 2,

        /// <summary>
        /// Максимальный.
        /// </summary>
        Maximal = 3
    }
}
