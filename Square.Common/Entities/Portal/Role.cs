﻿namespace Square.Common.Entities.Portal
{
    /// <summary>
    /// Роль пользователя.
    /// </summary>
    public class Role
    {
        /// <summary>
        /// Идентификатор.
        /// </summary>
        public int RoleID { get; set; } = (int)SquareRoleEnum.User;

        /// <summary>
        /// Название.
        /// </summary>
        public string Name { get; set; }
    }
}
