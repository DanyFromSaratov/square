﻿using System.Collections.Generic;

namespace Square.Common.Entities.City
{
    /// <summary>
    /// Информация о городе.
    /// </summary>
    public class City
    {
        /// <summary>
        /// Идентификатор города.
        /// </summary>
        public int CityID { get; set; }
        
        /// <summary>
        /// Название города.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Список районов города.
        /// </summary>
        public IEnumerable<District> Districts { get; set; }
    }
}
