﻿namespace Square.Common.Entities.City
{
    /// <summary>
    /// Район города.
    /// </summary>
    public class District
    {
        /// <summary>
        /// Идентификатор района.
        /// </summary>
        public int DistrictID { get; set; }

        /// <summary>
        /// Наименование.
        /// </summary>
        public string Name { get; set; }
    }
}
