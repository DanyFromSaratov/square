﻿namespace Square.Common.Entities.Common
{
    /// <summary>
    /// Таблица данных для пагинации.
    /// </summary>
    public class DataTablePagination
    {
        private int _from { get; set; }

        public int From
        {
            get
            {
                return _from > 0
                    ? _from
                    : 0;
            }
            set { _from = value; }
        }

        public int Take { get; set; }

        public int CurrentPageNumber { get; set; }

        public int PrevPageNumber { get; set; }

        public int NextPageNumber { get; set; }

        public int Total { get; set; }

        public int TakeOrDefault(int defaultTake, int? maxTake = null)
        {
            return Take > 0
                ? (maxTake.HasValue && Take > maxTake
                    ? maxTake.Value
                    : Take)
                : defaultTake;
        }
    }
}
