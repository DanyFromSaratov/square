﻿using System.Collections.Generic;

namespace Square.Common.Entities.Common
{
    /// <summary>
    /// Таблица объектов.
    /// </summary>
    public class DataTable<T>
    {
        public IEnumerable<T> Items { get; set; }

        public int  Total { get; set; }
    }
}
