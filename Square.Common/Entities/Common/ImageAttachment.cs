﻿namespace Square.Common.Entities.Common
{
    /// <summary>
    /// Приложенное изображение.
    /// </summary>
    public class ImageAttachment
    {
        public int ImageAttachmentID { get; set; }

        public byte[] Content { get; set; }

        public string ContentType { get; set; }

        public string FileName { get; set; }

        public int AdID { get; set; }

        public bool IsMain { get; set; }

        public string OldFileName { get; set; }

        public string NewFileName { get; set; }
    }
}
