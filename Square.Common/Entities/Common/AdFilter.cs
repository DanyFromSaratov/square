﻿using System.Collections.Generic;

namespace Square.Common.Entities.Common
{
    /// <summary>
    /// Фильтр для объявлений.
    /// </summary>
    public class AdFilter
    {
        /// <summary>
        /// Город.
        /// </summary>
        public int CityID { get; set; }

        /// <summary>
        /// Район.
        /// </summary>
        public int DistrictID { get; set; }

        /// <summary>
        /// Цена от.
        /// </summary>
        public int PriceFrom { get; set; }

        /// <summary>
        /// Цена до.
        /// </summary>
        public int PriceTo { get; set; }

        /// <summary>
        /// Количество комнат.
        /// </summary>
        public int RoomsCount { get; set; }

        /// <summary>
        /// Поиск.
        /// </summary>
        public string Search { get; set; }

        public int OrderedBy { get; set; }

        public string PhoneNumber { get; set; }

        public List<string> RoomsCountSelect { get; set; }

        public int UserID { get; set; }
    }
}
