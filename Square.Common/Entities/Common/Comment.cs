﻿namespace Square.Common.Entities.Common
{
    public class Comment
    {
        public int CommentID { get; set; }

        public int UserID { get; set; }

        public string Text { get; set; }

        public string Fio { get; set; }

        public string UserImage { get; set; }
    }
}
