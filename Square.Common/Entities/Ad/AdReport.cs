﻿namespace Square.Common.Entities.Ad
{
    public class AdReport
    {
        public bool AdFromRealtor { get; set; }

        public bool AdLeased { get; set; }

        public bool AdIncorrectData { get; set; }

        public bool IncorrectPhoneNumber { get; set; }

        public int AdId { get; set; }

        public int AdReportId { get; set; }
    }
}
