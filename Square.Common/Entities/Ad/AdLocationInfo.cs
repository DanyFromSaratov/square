﻿namespace Square.Common.Entities.Ad
{
    /// <summary>
    /// Информация о местонахождении объявления.
    /// </summary>
    public class AdLocationInfo
    {
        /// <summary>
        /// Идентификатор города.
        /// </summary>
        public int CityID { get; set; }

        /// <summary>
        /// Наименование города.
        /// </summary>
        public string CityName { get; set; }

        /// <summary>
        /// Идентификатор района текущего города.
        /// </summary>
        public int DistrictID { get; set; }

        /// <summary>
        /// Наименование района.
        /// </summary>
        public string DistrictName { get; set; }
    }
}
