﻿namespace Square.Common.Entities.Ad
{
    /// <summary>
    /// Комфорт.
    /// </summary>
    public class Comfort
    {
        /// <summary>
        /// Идентификатор.
        /// </summary>
        public int ComfortID { get; set; }

        /// <summary>
        /// Кондиционер.
        /// </summary>
        public bool AirConditioning { get; set; }

        /// <summary>
        /// Интернет (wifi).
        /// </summary>
        public bool Internet { get; set; }

        /// <summary>
        /// Ванная комната.
        /// </summary>
        public bool Bath { get; set; }

        /// <summary>
        /// Душевая кабина.
        /// </summary>
        public bool Shower { get; set; }

        /// <summary>
        /// Кухонная гарнитура.
        /// </summary>
        public bool KitchenFurniture { get; set; }

        /// <summary>
        /// Мебель в комнатах.
        /// </summary>
        public bool RoomsFurniture { get; set; }

        /// <summary>
        /// Холодильник.
        /// </summary>
        public bool Fridge { get; set; }

        /// <summary>
        /// Стиральная машина.
        /// </summary>
        public bool Washer { get; set; }
    }
}
