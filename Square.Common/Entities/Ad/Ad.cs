﻿using System;
using System.Collections.Generic;
using System.Linq;

using Square.Common.Entities.Common;

namespace Square.Common.Entities.Ad
{
    /// <summary>
    /// Объявление.
    /// </summary>
    public class Ad
    {
        private IEnumerable<ImageAttachment> _imageAttachments;
        private IEnumerable<Ad> _similarAds;

        /// <summary>
        /// Идентификатор.
        /// </summary>
        public int AdID { get; set; }

        /// <summary>
        /// Наименование.
        /// </summary>
        public string Title { get; set; }

        public string Kladr { get; set; }

        /// <summary>
        /// Описание.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// ФИО автора объявления.
        /// </summary>
        public string Fio { get; set; }

        /// <summary>
        /// Дата обновления объявления.
        /// </summary>
        public DateTime? UpdatedDate { get; set; }

        /// <summary>
        /// Дополнительная информация.
        /// </summary>
        public string AdditionalInfo { get; set; }

        /// <summary>
        /// Контактный телефон.
        /// </summary>
        public AdPhone Phone { get; set; }

        /// <summary>
        /// Информация о месте нахождения объявления.
        /// </summary>
        public AdLocationInfo LocationInfo { get; set; }

        /// <summary>
        /// Приложенные изображения.
        /// </summary>
        public IEnumerable<ImageAttachment> ImageAttachments
        {
            get => _imageAttachments ?? Enumerable.Empty<ImageAttachment>();
            set => _imageAttachments = value;
        }

        /// <summary>
        /// Количество просмотров.
        /// </summary>
        public int ViewsCount { get; set; }

        /// <summary>
        /// Цена.
        /// </summary>
        public int Price { get; set; }

        /// <summary>
        /// Количество комнат.
        /// </summary>
        public int RoomsCount { get; set; }

        /// <summary>
        /// Главное изображение.
        /// </summary>
        public ImageAttachment MainImage { get; set; }

        /// <summary>
        /// Комфорт (наличие: кондиционера, душа, wifi и тп.).
        /// </summary>
        public Comfort Comfort { get; set; }

        /// <summary>
        /// Топ-6 похожих объявлений.
        /// </summary>
        public IEnumerable<Ad> SimilarAds
        {
            get => _similarAds ?? Enumerable.Empty<Ad>();
            set => _similarAds = value;
        }

        public bool IsFavorite { get; set; }

        public bool IsAvito { get; set; }
    }
}
