﻿namespace Square.Common.Entities.Ad
{
    /// <summary>
    /// Телефон.
    /// </summary>
    public class AdPhone
    {
        /// <summary>
        /// Идентификатор.
        /// </summary>
        public int PhoneID { get; set; }

        /// <summary>
        /// Номер телефона.
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Добавочный номер телефона.
        /// </summary>
        public string AdditionalPhoneNumber { get; set; }
    }
}
