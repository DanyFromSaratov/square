﻿using System;
using System.Linq;

using Square.OriginDataBase;
using Square.Common.Entities.Portal;

namespace Square.Common.Mappers
{
    public class UserMapper
    {
        public static SquareUserModel MapToUserModel(User user)
        {
            if (user == null)
            {
                return null;
            }

            var roleModel = MapToRoleModel(user.Role);

            var userModel = new SquareUserModel
            {
                Email = user.Email,
                Password = user.Password,
                FirstName = user.FirstName,
                LastName = user.LastName,
                PhoneNumber = user.PhoneNumber,
                SquareRole = roleModel,
                RegistrationDate = DateTime.UtcNow
            };

            return userModel;
        }

        public static User MapToUser(SquareUserModel userModel)
        {
            if (userModel == null)
            {
                return null;
            }

            var role = MapToRole(userModel.SquareRole);

            var userSubscriptionModel = userModel.UserSubscriptions.FirstOrDefault(userSubscriptionFromDB => userSubscriptionFromDB.IsActive);

            var subscription = MapToUserSubscription(userSubscriptionModel);

            var user = new User
            {
                UserID = userModel.SquareUserID,
                Email = userModel.Email,
                Password = userModel.Password,
                FirstName = userModel.FirstName,
                LastName = userModel.LastName,
                PhoneNumber = userModel.PhoneNumber,
                Role = role,
                CurrentSubscription = subscription,
                RegistrationDate = userModel.RegistrationDate.GetValueOrDefault(),
                MainImage = userModel.MainImage
            };

            return user;
        }

        private static Role MapToRole(SquareRoleModel roleModel)
        {
            if (roleModel == null)
            {
                return null;
            }

            var role = new Role
            {
                RoleID = roleModel.RoleID,
                Name = roleModel.Name
            };

            return role;
        }

        private static SquareRoleModel MapToRoleModel(Role role)
        {
            if (role == null)
            {
                return null;
            }

            var roleModel = new SquareRoleModel
            {
                RoleID = role.RoleID,
                Name = role.Name
            };

            return roleModel;
        }

        public static Subscription MapToSubscription(SubscriptionModel subscriptionModel)
        {
            if (subscriptionModel == null)
            {
                return null;
            }

            var subscription = new Subscription
            {
                SubscriptionID = subscriptionModel.SubscriptionID,
                Title = subscriptionModel.Title,
                Price = subscriptionModel.Price,
                SortOrder = subscriptionModel.SortOrder,
                Info = subscriptionModel.Info
            };

            return subscription;
        }

        public static Subscription MapToSubscription(UserSubscriptionModel subscriptionModel)
        {
            if (subscriptionModel == null)
            {
                return null;
            }

            var subscription = new Subscription
            {
                SubscriptionID = subscriptionModel.SubscriptionID,
                Title = subscriptionModel.Subscription.Title,
                Price = subscriptionModel.Subscription.Price,
                Info = subscriptionModel.Subscription.Info,
                StartDate = subscriptionModel.DateFrom,
                EndDate = subscriptionModel.DateTo,
                IsActive = subscriptionModel.IsActive
            };

            return subscription;
        }

        public static Subscription MapToUserSubscription(UserSubscriptionModel userSubscriptionModel)
        {
            if (userSubscriptionModel == null)
            {
                return null;
            }

            var subscription = new Subscription
            {
                SubscriptionID = userSubscriptionModel.SubscriptionID,
                StartDate = userSubscriptionModel.DateFrom,
                EndDate = userSubscriptionModel.DateTo
            };

            return subscription;
        }
    }
}
