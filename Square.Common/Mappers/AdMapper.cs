﻿using Square.OriginDataBase;
using Square.Common.Entities.Ad;
using Comfort = Square.Common.Entities.Ad.Comfort;

namespace Square.Common.Mappers
{
    public class AdMapper
    {
        public static Ad MapToAd(AdModel adModel)
        {
            if (adModel == null)
            {
                return null;
            }

            var phone = MapToPhone(adModel.Phone);

            var locationInfo = new AdLocationInfo
            {
                CityID = adModel.CityID ?? 0,
                DistrictID = adModel.DistrictID ?? 0,
            };

            var comfort = MapToComfort(adModel.Comfort);

            var ad = new Ad
            {
                AdID = adModel.AdID,
                Title = adModel.Title,
                Kladr = adModel.Kladr,
                Description = adModel.Description,
                Phone = phone,
                LocationInfo = locationInfo,
                Fio = adModel.Fio,
                AdditionalInfo = adModel.AdditionalInfo,
                UpdatedDate = adModel.UpdatedDate,
                ViewsCount = adModel.ViewsCount,
                Price = adModel.Price,
                RoomsCount = adModel.RoomsCount,
                Comfort = comfort,
                IsFavorite = adModel.IsFavorite,
                IsAvito = adModel.IsAvito
            };

            return ad;
        }

        public static AdModel MapToAdModel(Ad ad)
        {
            if (ad == null)
            {
                return null;
            }

            var phoneModel = MapToPhoneModel(ad.Phone);
            var comfortModel = MapToComfortModel(ad.Comfort);

            var adModel = new AdModel
            {
                AdID = ad.AdID,
                Title = ad.Title,
                Kladr = ad.Kladr,
                Description = ad.Description,
                Phone = phoneModel,
                CityID = ad.LocationInfo.CityID,
                DistrictID = ad.LocationInfo.DistrictID,
                Fio = ad.Fio,
                AdditionalInfo = ad.AdditionalInfo,
                Price = ad.Price,
                RoomsCount = ad.RoomsCount,
                Comfort = comfortModel,
                IsAvito = ad.IsAvito
            };

            return adModel;
        }

        private static AdPhone MapToPhone(PhoneModel phoneModel)
        {
            if (phoneModel == null)
            {
                return null;
            }

            var phone = new AdPhone
            {
                PhoneID = phoneModel.PhoneID,
                PhoneNumber = phoneModel.PhoneNumber,
                AdditionalPhoneNumber = phoneModel.AdditionalPhoneNumber
            };

            return phone;
        }

        private static PhoneModel MapToPhoneModel(AdPhone phone)
        {
            if (phone == null)
            {
                return null;
            }

            var phoneModel = new PhoneModel
            {
                PhoneID = phone.PhoneID,
                PhoneNumber = phone.PhoneNumber,
                AdditionalPhoneNumber = phone.AdditionalPhoneNumber
            };

            return phoneModel;
        }

        public static ComfortModel MapToComfortModel(Comfort comfort)
        {
            if (comfort == null)
            {
                return null;
            }

            var comfortModel = new ComfortModel
            {
                ComfortID = comfort.ComfortID,
                AirConditioning = comfort.AirConditioning,
                Bath = comfort.Bath,
                Fridge = comfort.Fridge,
                Internet = comfort.Internet,
                KitchenFurniture = comfort.KitchenFurniture,
                RoomsFurniture = comfort.RoomsFurniture,
                Shower = comfort.Shower,
                Washer = comfort.Washer
            };

            return comfortModel;
        }

        private static Comfort MapToComfort(ComfortModel comfortModel)
        {
            if (comfortModel == null)
            {
                return null;
            }

            var comfort = new Comfort
            {
                ComfortID = comfortModel.ComfortID,
                AirConditioning = comfortModel.AirConditioning,
                Bath = comfortModel.Bath,
                Fridge = comfortModel.Fridge,
                Internet = comfortModel.Internet,
                KitchenFurniture = comfortModel.KitchenFurniture,
                RoomsFurniture = comfortModel.RoomsFurniture,
                Shower = comfortModel.Shower,
                Washer = comfortModel.Washer
            };

            return comfort;
        }
    }
}
