﻿using System.Linq;

using Square.Common.Entities.City;
using Square.OriginDataBase;

namespace Square.Common.Mappers
{
    public static class CityMapper
    {
        public static City MapToCity(CityModel cityModel)
        {
            if (cityModel == null)
            {
                return null;
            }

            var districts = cityModel.Districts
                .Select(MapToDistrict)
                .ToList();

            var city = new City
            {
                CityID = cityModel.CityID,
                Name = cityModel.Name,
                Districts = districts
            };

            return city;
        }

        private static District MapToDistrict(DistrictModel districtModel)
        {
            if (districtModel == null)
            {
                return null;
            }

            var district = new District
            {
                DistrictID = districtModel.DistrictID,
                Name = districtModel.Name
            };

            return district;
        }
    }
}
