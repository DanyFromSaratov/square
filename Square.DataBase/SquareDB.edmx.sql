
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 06/21/2019 11:47:50
-- Generated from EDMX file: C:\Square\Square.DataBase\SquareDB.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [SquareDB];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_Images_Ads]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Images] DROP CONSTRAINT [FK_Images_Ads];
GO
IF OBJECT_ID(N'[dbo].[FK_Phone_Contact]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Phones] DROP CONSTRAINT [FK_Phone_Contact];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Ads]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Ads];
GO
IF OBJECT_ID(N'[dbo].[Contacts]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Contacts];
GO
IF OBJECT_ID(N'[dbo].[Images]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Images];
GO
IF OBJECT_ID(N'[dbo].[Phones]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Phones];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Ads'
CREATE TABLE [dbo].[Ads] (
    [AdID] int IDENTITY(1,1) NOT NULL,
    [Title] nvarchar(300)  NOT NULL,
    [Description] nvarchar(2000)  NOT NULL,
    [ImageID] int  NULL
);
GO

-- Creating table 'Images'
CREATE TABLE [dbo].[Images] (
    [ImageID] int IDENTITY(1,1) NOT NULL,
    [Data] varbinary(max)  NOT NULL,
    [AdID] int  NULL
);
GO

-- Creating table 'Contacts'
CREATE TABLE [dbo].[Contacts] (
    [ContactID] int IDENTITY(1,1) NOT NULL,
    [FirstName] nvarchar(100)  NOT NULL,
    [LastName] nvarchar(100)  NOT NULL,
    [MiddleName] nvarchar(100)  NULL,
    [Email] nvarchar(200)  NULL,
    [Rank] nvarchar(200)  NULL
);
GO

-- Creating table 'Phones'
CREATE TABLE [dbo].[Phones] (
    [PhoneID] int IDENTITY(1,1) NOT NULL,
    [PhoneNumber] nvarchar(200)  NOT NULL,
    [AdditionalPhoneNumber] nvarchar(200)  NULL,
    [ContactID] int  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [AdID] in table 'Ads'
ALTER TABLE [dbo].[Ads]
ADD CONSTRAINT [PK_Ads]
    PRIMARY KEY CLUSTERED ([AdID] ASC);
GO

-- Creating primary key on [ImageID] in table 'Images'
ALTER TABLE [dbo].[Images]
ADD CONSTRAINT [PK_Images]
    PRIMARY KEY CLUSTERED ([ImageID] ASC);
GO

-- Creating primary key on [ContactID] in table 'Contacts'
ALTER TABLE [dbo].[Contacts]
ADD CONSTRAINT [PK_Contacts]
    PRIMARY KEY CLUSTERED ([ContactID] ASC);
GO

-- Creating primary key on [PhoneID] in table 'Phones'
ALTER TABLE [dbo].[Phones]
ADD CONSTRAINT [PK_Phones]
    PRIMARY KEY CLUSTERED ([PhoneID] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [ContactID] in table 'Phones'
ALTER TABLE [dbo].[Phones]
ADD CONSTRAINT [FK_Phone_Contact]
    FOREIGN KEY ([ContactID])
    REFERENCES [dbo].[Contacts]
        ([ContactID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Phone_Contact'
CREATE INDEX [IX_FK_Phone_Contact]
ON [dbo].[Phones]
    ([ContactID]);
GO

-- Creating foreign key on [AdID] in table 'Images'
ALTER TABLE [dbo].[Images]
ADD CONSTRAINT [FK_Images_Ads]
    FOREIGN KEY ([AdID])
    REFERENCES [dbo].[Ads]
        ([AdID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Images_Ads'
CREATE INDEX [IX_FK_Images_Ads]
ON [dbo].[Images]
    ([AdID]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------