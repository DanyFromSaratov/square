﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Square.Common;

namespace Square.Services.Contracts
{
    /// <summary>
    /// Сервис для работы с объявлениями.
    /// </summary>
    public interface IAdService
    {
        /// <summary>
        /// Получить список объявлений.
        /// </summary>
        Task<IEnumerable<Ad>> GetAdsAsync();

        /// <summary>
        /// Добавить новое объявление.
        /// </summary>
        Task<int> AddNewAd(Ad ad);
    }
}
