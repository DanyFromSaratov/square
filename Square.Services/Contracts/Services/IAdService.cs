﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Square.Common;
using Square.Common.Entities.Ad;
using Square.Common.Entities.Common;

namespace Square.Services.Contracts.Services
{
    /// <summary>
    /// Сервис для работы с объявлениями.
    /// </summary>
    public interface IAdService
    {
        /// <summary>
        /// Возвращает список объявлений.
        /// </summary>
        Task<DataTable<Ad>> GetAdsAsync(AdFilter filter, int page, int take);

        Task<DataTable<Ad>> GetAdRequestsAsync(AdFilter filter, int page, int take);

        /// <summary>
        /// Возвращает объявление по идентификатору.
        /// </summary>
        Task<Ad> GetAdByIDAsync(int adID);

        Task<Ad> GetAdRequestByIDAsync(int adID);

        /// <summary>
        /// Добавляет новое объявление.
        /// </summary>
        Task<int> AddNewAdAsync(Ad ad);

        /// <summary>
        /// Обновляет объявление.
        /// </summary>
        Task UpdateAdAsync(Ad ad);

        /// <summary>
        /// Удаляет объявление по идентификатору.
        /// </summary>
        Task DeleteAdByIDAsync(int adID);

        /// <summary>
        /// Обновляет количество просмотров у объявления.
        /// </summary>param>
        /// <returns></returns>
        Task UpdateViewsCountAsync(int adID);

        Task<int> AddAdRequestAsync(Ad ad);

        Task<DataTable<Ad>> GetAdArchiveAsync(AdFilter filter, int page, int take);

        Task MoveAdFromRequestsToAdsAsync(int adID);

        Task MoveAdFromArchiveToAdsAsync(int adID);

        Task<IEnumerable<int>> GetAdIds();

        Task UpdateImageAttachments(int adId, IEnumerable<ImageAttachment> imageAttachments);

        /// <summary>
        /// Возвращает список объявлений из архива.
        /// </summary>
        Task<DataTable<Ad>> GetAdsFromArchiveAsync(AdFilter filter, int page, int take);

        /// <summary>
        /// Возвращает объявление их архива по идентификатору.
        /// </summary>
        Task<Ad> GetAdArchiveByIDAsync(int adID);

        Task AddAdReportAsync(AdReport adReport);

        Task<IEnumerable<AdReport>> GetAdReportsAsync();
    }
}
