﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Square.Common.Entities.Ad;
using Square.Common.Entities.City;

namespace Square.Services.Contracts.Services
{
    /// <summary>
    /// Сервис для работы с городами.
    /// </summary>
    public interface ICityService
    {
        /// <summary>
        /// Возвращает список городов.
        /// </summary>
        Task<IEnumerable<City>> GetCitiesAsync();
    }
}
