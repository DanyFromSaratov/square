﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Square.Common;
using Square.Common.Entities.Common;
using Square.Common.Entities.Portal;

namespace Square.Services.Contracts.Services
{
    public interface IUserService
    {
        Task RegisterUserAsync(User user);

        Task<User> GetUserByEmailAsync(string email);

        Task<User> GetUserByPhoneNumberAsync(string phoneNumber);

        Task<User> GetUserByEmailAndPasswordAsync(string email, string password);

        Task<User> GetUserByPhoneNumberAndPasswordAsync(string phoneNumber, string password);

        Task<User> GetUserByIDAsync(int userID);

        Task<bool> IsUserExistedAsync(string email);

        Task<bool> IsUserExistedByPhoneNumberAsync(string phoneNumber);

        bool IsUserInRole(string username, string roleName);

        IEnumerable<string> GetRolesForUser(string username);

        IEnumerable<Subscription> GetSubscriptionsAsync();

        Task<Subscription> GetUserCurrentSubscriptionAsync(int userID);

        Task ConnectSubscriptionToUserAsync(Subscription subscription);

        Task OrderCallAsync(string phoneNumber);


        Task<IEnumerable<string>> GetOrderedCallsAsync();

        Task DeleteOrderedCallAsync(string phoneNumber);

        Task<IEnumerable<User>> GetUsersAsync();

        Task RemoveUserAsync(int userID);

        Task ChangeUserRole(int userID, SquareRoleEnum role);

        string GetPolicy();

        IEnumerable<Subscription> GetSubscriptionsByUserIdAsync(int userID);

        Task UpdateUserInfoAsync(User userInfo);

        Task<IEnumerable<Comment>> GetCommentsAsync();

        Task AddCommentAsync(Comment comment);

        Task AddNewsAsync(News news);

        Task<IEnumerable<News>> GetNewsAsync();

        Task<News> GetNewsByIDAsync(int newsID);

        Task AddSpecialistHelpRequestAsync(SpecialistHelper specialistHelper);

        Task<IEnumerable<SpecialistHelper>> GetSpecialistHelpRequestsAsync();

        Task AddAdToFavoriteAsync(Favorite favorite);

        Task RemoveAdFromFavoriteAsync(int userId, int adId);

        Task<IEnumerable<Favorite>> GetFavoritesAsync(int userID);

        Task UpdateFreeViewsAsync(int userID);
    }
}
