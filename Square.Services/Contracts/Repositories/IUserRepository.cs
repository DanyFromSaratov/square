﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Square.Common;
using Square.Common.Entities.Common;
using Square.Common.Entities.Portal;

namespace Square.Services.Contracts.Repositories
{
    /// <summary>
    /// Репозиторий для работы с пользователями.
    /// </summary>
    public interface IUserRepository
    {
        /// <summary>
        /// Регистрирует нового пользователя.
        /// </summary>
        Task RegisterUserAsync(User user);

        /// <summary>
        /// Возвращает пользователя по номеру телефона.
        /// </summary>
        Task<User> GetUserByPhoneNumberAsync(string phoneNumber);

        /// <summary>
        /// Возвращает пользователя по Email.
        /// </summary>
        Task<User> GetUserByEmailAsync(string email);

        /// <summary>
        /// Возвращает пользователя по номеру телефона и паролю.
        /// </summary>
        Task<User> GetUserByPhoneNumberAndPasswordAsync(string phoneNumber, string password);
        
        /// <summary>
        /// Возвращает пользователя по номеру телефона и паролю.
        /// </summary>
        Task<User> GetUserByIDAsync(int userID);

        /// <summary>
        /// Возвращает признак того, содержится ли пользователь с текущим Email.
        /// </summary>
        Task<bool> IsUserExistedAsync(string email);

        /// <summary>
        /// Возвращает признак того, имеет ли пользователь заданную роль.
        /// </summary>
        bool IsUserInRole(string username, string roleName);

        /// <summary>
        /// Возвращает роли пользователя по логину(username).
        /// </summary>
        IEnumerable<string> GetRolesForUser(string username);

        /// <summary>
        /// Возвращает список подписок.
        /// </summary>
        IEnumerable<Subscription> GetSubscriptionsAsync();

        /// <summary>
        /// Возвращает действующую подписку пользователя.
        /// </summary>
        Task<Subscription> GetUserCurrentSubscriptionAsync(int userID);

        /// <summary>
        /// Подключает подписку для пользователя.
        /// </summary>
        Task ConnectSubscriptionToUserAsync(Subscription subscription);

        /// <summary>
        /// Возвращает признак того,
        /// содержится ли пользователь по номеру телефона(логину).
        /// </summary>
        Task<bool> IsUserExistedByPhoneNumberAsync(string phoneNumber);

        /// <summary>
        /// Заказывает звонок.
        /// </summary>
        Task OrderCallAsync(string phoneNumber);

        /// <summary>
        /// Возвращает список заказанных звонков.
        /// </summary>
        Task<IEnumerable<string>> GetOrderedCallsAsync();

        /// <summary>
        /// Удаляет заказанный звонок по номеру телефона.
        /// </summary>
        Task DeleteOrderedCallAsync(string phoneNumber);

        /// <summary>
        /// Возвращает список пользователей.
        /// </summary>
        Task<IEnumerable<User>> GetUsersAsync();

        /// <summary>
        /// Удаляет пользователя по идентификатору.
        /// </summary>
        Task RemoveUserAsync(int userID);

        /// <summary>
        /// Изменяет роль пользователю.
        /// </summary>
        Task ChangeUserRole(int userID, SquareRoleEnum role);

        IEnumerable<Subscription> GetSubscriptionsByUserIdAsync(int userId);


        Task UpdateUserInfoAsync(User userInfo);

        Task<IEnumerable<Comment>> GetCommentsAsync();

        Task AddCommentAsync(Comment comment);

        Task AddNewsAsync(News news);

        Task<IEnumerable<News>> GetNewsAsync();

        Task<News> GetNewsByIDAsync(int newsID);

        Task AddSpecialistHelpRequestAsync(SpecialistHelper user);

        Task<IEnumerable<SpecialistHelper>> GetSpecialistHelpRequestsAsync();

        Task AddAdToFavoriteAsync(Favorite favorite);

        Task RemoveAdFromFavoriteAsync(int userId, int adId);

        Task<IEnumerable<Favorite>> GetFavoritesAsync(int userID);

        Task UpdateFreeViewsAsync(int userID);
    }
}
