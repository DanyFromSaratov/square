﻿using System.Collections.Generic;
using System.Threading.Tasks;

using Square.Common.Entities.Ad;
using Square.Common.Entities.Common;

namespace Square.Services.Contracts.Repositories
{
    /// <summary>
    /// Репозиторий для работы с объявлениями.
    /// </summary>
    public interface IAdRepository
    {
        /// <summary>
        /// Возвращает список объявлений.
        /// </summary>
        Task<DataTable<Ad>> GetAdsAsync(AdFilter filter, int page, int take);

        /// <summary>
        /// Возвращает список заявок.
        /// </summary>
        Task<DataTable<Ad>> GetAdRequestsAsync(AdFilter filter, int page, int take);

        /// <summary>
        /// Возвращает объявление по идентификатору.
        /// </summary>
        Task<Ad> GetAdByIDAsync(int adID);

        /// <summary>
        /// Возвращает заявку по идентификатору.
        /// </summary>
        Task<Ad> GetAdRequestByIDAsync(int adID);

        /// <summary>
        /// Добавляет новое объявление.
        /// </summary>
        Task<int> AddNewAdAsync(Ad ad);

        /// <summary>
        /// Обновляет объявление.
        /// </summary>
        Task UpdateAdAsync(Ad ad);

        /// <summary>
        /// Удаляет объявление по идентификатору.
        /// </summary>
        Task DeleteAdByIDAsync(int adID);

        /// <summary>
        /// Обновляет количество просмотров у объявления.
        /// </summary>
        Task UpdateViewsCountAsync(int adID);

        /// <summary>
        /// Добавляет заявку.
        /// </summary>
        Task<int> AddAdRequestAsync(Ad ad);

        /// <summary>
        /// Возвращает архив.
        /// </summary>
        Task<DataTable<Ad>> GetAdArchiveAsync(AdFilter filter, int page, int take);

        /// <summary>
        /// Перемещает заявку в основной список объявлений.
        /// </summary>
        Task MoveAdFromRequestsToAdsAsync(int adID);

        /// <summary>
        /// Перемещает заявку из архива в основной список объявлений.
        /// </summary>
        Task MoveAdFromArchiveToAdsAsync(int adID);

        Task<IEnumerable<int>> GetAdIds();

        Task UpdateImageAttachments(int adId, IEnumerable<ImageAttachment> imageAttachments);

        /// <summary>
        /// Возвращает список объявлений из архива.
        /// </summary>
        Task<DataTable<Ad>> GetAdsFromArchiveAsync(AdFilter filter, int page, int take);


        /// <summary>
        /// Возвращает объявление их архива по идентификатору.
        /// </summary>
        Task<Ad> GetAdArchiveByIDAsync(int adID);

        Task AddAdReportAsync(AdReport adReport);

        Task<IEnumerable<AdReport>> GetAdReportsAsync();
    }
}
