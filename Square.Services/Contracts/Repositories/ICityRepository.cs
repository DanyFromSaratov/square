﻿using System.Collections.Generic;
using System.Threading.Tasks;

using Square.Common.Entities.City;

namespace Square.Services.Contracts.Repositories
{
    /// <summary>
    /// Репозиторий для работы с городами.
    /// </summary>
    public interface ICityRepository
    {
        /// <summary>
        /// Возвращает список городов.
        /// </summary>
        Task<IEnumerable<City>> GetCitiesAsync();
    }
}
