﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

using Square.Common.Entities.City;
using Square.Common.Mappers;
using Square.OriginDataBase;
using Square.Services.Contracts.Repositories;

namespace Square.Services.Implementation.Repositories
{
    /// <summary>
    /// Репозиторий для работы с городами.
    /// </summary>
    public class CityRepository : ICityRepository
    {
        /// <summary>
        /// Возвращает список городов.
        /// </summary>
        public async Task<IEnumerable<City>> GetCitiesAsync()
        {
            using (var db = new SquareDBEntities())
            {
                var cityModels = await db.Cities
                    .Select(cityModel => cityModel)
                    .ToListAsync();

                var cities = cityModels
                    .Select(CityMapper.MapToCity)
                    .ToList();

                return cities;
            }
        }
    }
}
