﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Metadata.Edm;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using ServiceStack.Script;
using Square.Common.Entities.Ad;
using Square.Common.Entities.Common;
using Square.Common.Mappers;
using Square.OriginDataBase;
using Square.Services.Contracts.Repositories;

namespace Square.Services.Implementation.Repositories
{ 
    /// <summary>
    /// Репозиторий для работы с объявлениями.
    /// </summary>
    public class AdRepository : IAdRepository
    {
        /// <summary>
        /// Возвращает список объявлений.
        /// </summary>
        public async Task<DataTable<Ad>> GetAdsAsync(AdFilter filter, int page, int take)
        {
            using (var db = new SquareDBEntities())
            {
                var adModels = db.Ads
                    .Where(adModel => !adModel.IsRequest && !adModel.IsArchive)
                    .Select(adModel => new Ad
                    {
                        AdID = adModel.AdID,
                        Title = adModel.Title,
                        Kladr = adModel.Kladr,
                        Description = adModel.Description,
                        Phone = new AdPhone
                        {
                            PhoneID = adModel.Phone.PhoneID,
                            PhoneNumber = adModel.Phone.PhoneNumber,
                            AdditionalPhoneNumber = adModel.Phone.AdditionalPhoneNumber
                        },
                        LocationInfo = new AdLocationInfo
                        {
                            CityID = adModel.CityID ?? 0,
                            DistrictID = adModel.DistrictID ?? 0,
                        },
                        Fio = adModel.Fio,
                        AdditionalInfo = adModel.AdditionalInfo,
                        UpdatedDate = adModel.UpdatedDate,
                        ViewsCount = adModel.ViewsCount,
                        Price = adModel.Price,
                        RoomsCount = adModel.RoomsCount,
                        IsAvito = adModel.IsAvito,
                        Comfort = new Comfort
                        {
                            ComfortID = adModel.Comfort.ComfortID,
                            AirConditioning = adModel.Comfort.AirConditioning,
                            Bath = adModel.Comfort.Bath,
                            Fridge = adModel.Comfort.Fridge,
                            Internet = adModel.Comfort.Internet,
                            KitchenFurniture = adModel.Comfort.KitchenFurniture,
                            RoomsFurniture = adModel.Comfort.RoomsFurniture,
                            Shower = adModel.Comfort.Shower,
                            Washer = adModel.Comfort.Washer
                        },
                    });

                if (filter.CityID > 0)
                {
                    adModels = adModels.Where(adModel => adModel.LocationInfo.CityID == filter.CityID);
                }

                if (filter.DistrictID > 0)
                {
                    adModels = adModels.Where(adModel => adModel.LocationInfo.DistrictID == filter.DistrictID);
                }

                if (filter.PriceFrom > 0)
                {
                    adModels = adModels.Where(adModel => adModel.Price >= filter.PriceFrom);
                }

                if (filter.PriceTo > 0)
                {
                    adModels = adModels.Where(adModel => adModel.Price <= filter.PriceTo);
                }

                if (filter.RoomsCountSelect != null && filter.RoomsCountSelect.Count > 0)
                {
                    var roomsList = GetRoomsCount(filter.RoomsCountSelect);

                    adModels = adModels.Where(adModel => roomsList.Contains(adModel.RoomsCount));
                }

                if (!string.IsNullOrWhiteSpace(filter.Search) && filter.Search.Length > 1)
                {
                    filter.Search = filter.Search.Trim();

                    var searchComponents = SplitSearch(filter.Search.ToLower()).ToList();

                    adModels = adModels.Where(adModel =>
                        searchComponents.Any(word => adModel.Title.ToLower().Contains(word)));
                }

                if (!string.IsNullOrWhiteSpace(filter.PhoneNumber))
                {
                    adModels = adModels.Where(adModel => adModel.Phone.PhoneNumber.Contains(filter.PhoneNumber));
                }

                adModels = adModels.Where(adModel => db.Blobs.Any(imgAtt =>
                        adModel.AdID == imgAtt.AdInfoID
                        && imgAtt.IsMain == true 
                        && !imgAtt.NewFileName.ToLower().Contains("png")));

                var query = adModels.AsNoTracking();

                var total = await query.CountAsync();
                var ads = new List<Ad>();

                switch (filter.OrderedBy)
                {
                    case 1:
                        ads = await query
                            .OrderByDescending(adModel => adModel.Price)
                            .Skip((page - 1) * take)
                            .Take(take)
                            .ToListAsync();
                        break;
                    case 2:
                        ads = await query
                            .OrderBy(adModel => adModel.Price)
                            .Skip((page - 1) * take)
                            .Take(take)
                            .ToListAsync();
                        break;
                    case 3:
                        ads = await query
                            .OrderBy(adModel => adModel.UpdatedDate)
                            .Skip((page - 1) * take)
                            .Take(take)
                            .ToListAsync();
                        break;
                    default:
                        ads = await query
                            .OrderByDescending(adModel => adModel.UpdatedDate)
                            .Skip((page - 1) * take)
                            .Take(take)
                            .ToListAsync();
                        break;
                }

                var favoriteAdIds = new List<int>();

                if (filter.UserID != 0)
                {
                    var favoriteAdIdsModel = await db.FavoriteModels
                        .Where(favoriteModel => favoriteModel.UserID == filter.UserID)
                        .Select(favoriteModel => favoriteModel.AdID)
                        .ToListAsync();

                    favoriteAdIds = favoriteAdIdsModel;
                }

                foreach (var ad in ads)
                {
                    if (favoriteAdIds.Contains(ad.AdID))
                    {
                        ad.IsFavorite = true;
                    }

                    ad.ImageAttachments = await db.Blobs
                        .Where(blob => blob.AdInfoID == ad.AdID)
                        .Select(blobModel =>
                            new ImageAttachment
                            {
                                ImageAttachmentID = blobModel.BlobID,
                                NewFileName = blobModel.NewFileName,
                                FileName = blobModel.FileName,
                                AdID = blobModel.AdInfoID ?? 0,
                                IsMain = blobModel.IsMain ?? false
                            })
                        .ToListAsync();
                }

                var dataTable = new DataTable<Ad>
                {
                    Items = ads,
                    Total = total
                };

                return dataTable;
            }
        }

        private List<int> GetRoomsCount(List<string> roomsCountSelect)
        {
            if (roomsCountSelect == null || roomsCountSelect.Count == 0)
            {
                return new List<int>();
            }

            var roomsCountList = new List<int>();

            foreach (var item in roomsCountSelect)
            {
                if (item == "1 комн.")
                {
                    roomsCountList.Add(1);
                }
                else if (item == "2 комн.")
                {
                    roomsCountList.Add(2);
                }
                else if (item == "3 комн.")
                {
                     roomsCountList.Add(3);
                }
                else if (item == "4+ комн.")
                {
                    roomsCountList.Add(4);
                }
            }

            return roomsCountList;
        }

        private IEnumerable<string> SplitSearch(string search)
        {
            if (string.IsNullOrWhiteSpace(search))
            {
                return Enumerable.Empty<string>();
            }

            var searchComponents = search
                .Trim()
                .ToLower()
                .Split(' ', ',', '.')
                .Where(word => word.Length > 2
                               && word != "ул"
                               && word != "им"
                               && !word.Contains('(')
                               && !word.Contains(')'));

            return searchComponents.ToList();
        }

        public async Task<IEnumerable<int>> GetAdIds()
        {
            using (var db = new SquareDBEntities())
            {
                var adIds = await db.Ads
                    .Where(adModel => !adModel.IsRequest && !adModel.IsArchive)
                    .Select(adModel => adModel.AdID)
                    .ToListAsync();

                return adIds;
            }
        }

        public async Task UpdateImageAttachments(int adId, IEnumerable<ImageAttachment> imageAttachments)
        {
            using (var db = new SquareDBEntities())
            {
                var blobs = await db.Blobs.Where(b => b.AdInfoID == adId).ToListAsync();

                foreach (var blob in blobs)
                {
                    var newImg = imageAttachments.FirstOrDefault(im => im.OldFileName == blob.FileName);
                    if (newImg != null)
                    {
                        blob.NewFileName = newImg.FileName;
                    }
                }

                await db.SaveChangesAsync();
            }
        }

        /// <summary>
        /// Возвращает список объявлений из архива.
        /// </summary>
        public async Task<DataTable<Ad>> GetAdsFromArchiveAsync(AdFilter filter, int page, int take)
        {
            using (var db = new SquareDBEntities())
            {
                var adModels = db.Ads
                    .Where(adModel => adModel.IsArchive)
                    .Select(adModel => new Ad
                    {
                        AdID = adModel.AdID,
                        Title = adModel.Title,
                        Kladr = adModel.Kladr,
                        Description = adModel.Description,
                        Phone = new AdPhone
                        {
                            PhoneID = adModel.Phone.PhoneID,
                            PhoneNumber = adModel.Phone.PhoneNumber,
                            AdditionalPhoneNumber = adModel.Phone.AdditionalPhoneNumber
                        },
                        LocationInfo = new AdLocationInfo
                        {
                            CityID = adModel.CityID ?? 0,
                            DistrictID = adModel.DistrictID ?? 0,
                        },
                        Fio = adModel.Fio,
                        AdditionalInfo = adModel.AdditionalInfo,
                        UpdatedDate = adModel.UpdatedDate,
                        ViewsCount = adModel.ViewsCount,
                        Price = adModel.Price,
                        RoomsCount = adModel.RoomsCount,
                        IsAvito = adModel.IsAvito,
                        Comfort = new Comfort
                        {
                            ComfortID = adModel.Comfort.ComfortID,
                            AirConditioning = adModel.Comfort.AirConditioning,
                            Bath = adModel.Comfort.Bath,
                            Fridge = adModel.Comfort.Fridge,
                            Internet = adModel.Comfort.Internet,
                            KitchenFurniture = adModel.Comfort.KitchenFurniture,
                            RoomsFurniture = adModel.Comfort.RoomsFurniture,
                            Shower = adModel.Comfort.Shower,
                            Washer = adModel.Comfort.Washer
                        }
                    });

                if (filter.CityID > 0)
                {
                    adModels = adModels.Where(adModel => adModel.LocationInfo.CityID == filter.CityID);
                }

                if (filter.DistrictID > 0)
                {
                    adModels = adModels.Where(adModel => adModel.LocationInfo.DistrictID == filter.DistrictID);
                }

                if (filter.PriceFrom > 0)
                {
                    adModels = adModels.Where(adModel => adModel.Price >= filter.PriceFrom);
                }

                if (filter.PriceTo > 0)
                {
                    adModels = adModels.Where(adModel => adModel.Price <= filter.PriceTo);
                }

                if (filter.RoomsCountSelect != null && filter.RoomsCountSelect.Count > 0)
                {
                    var roomsList = GetRoomsCount(filter.RoomsCountSelect);

                    adModels = adModels.Where(adModel => roomsList.Contains(adModel.RoomsCount));
                }

                if (!string.IsNullOrWhiteSpace(filter.Search) && filter.Search.Length > 1)
                {
                    filter.Search = filter.Search.Trim();

                    var searchComponents = SplitSearch(filter.Search.ToLower()).ToList();

                    adModels = adModels.Where(adModel =>
                        searchComponents.Any(word => adModel.Title.ToLower().Contains(word)));
                }

                if (!string.IsNullOrWhiteSpace(filter.PhoneNumber))
                {
                    adModels = adModels.Where(adModel => adModel.Phone.PhoneNumber.Contains(filter.PhoneNumber));
                }

                adModels = adModels.Where(adModel => db.Blobs.Any(imgAtt =>
                    adModel.AdID == imgAtt.AdInfoID
                    && imgAtt.IsMain == true));

                var query = adModels.AsNoTracking();

                var total = await query.CountAsync();
                var ads = new List<Ad>();


                switch (filter.OrderedBy)
                {
                    case 1:
                        ads = await query
                            .OrderByDescending(adModel => adModel.Price)
                            .Skip((page - 1) * take)
                            .Take(take)
                            .ToListAsync();
                        break;
                    case 2:
                        ads = await query
                            .OrderBy(adModel => adModel.Price)
                            .Skip((page - 1) * take)
                            .Take(take)
                            .ToListAsync();
                        break;
                    case 3:
                        ads = await query
                            .OrderBy(adModel => adModel.UpdatedDate)
                            .Skip((page - 1) * take)
                            .Take(take)
                            .ToListAsync();
                        break;
                    default:
                        ads = await query
                            .OrderByDescending(adModel => adModel.UpdatedDate)
                            .Skip((page - 1) * take)
                            .Take(take)
                            .ToListAsync();
                        break;
                }

                foreach (var ad in ads)
                {
                    ad.ImageAttachments = await db.Blobs
                        .Where(blob => blob.AdInfoID == ad.AdID)
                        .Select(blobModel =>
                            new ImageAttachment
                            {
                                ImageAttachmentID = blobModel.BlobID,
                                NewFileName = blobModel.NewFileName,
                                FileName = blobModel.FileName,
                                AdID = blobModel.AdInfoID ?? 0,
                                IsMain = blobModel.IsMain ?? false
                            })
                        .ToListAsync();
                }

                var dataTable = new DataTable<Ad>
                {
                    Items = ads,
                    Total = total
                };

                return dataTable;
            }
        }

        public async Task<Ad> GetAdArchiveByIDAsync(int adID)
        {
            using (var db = new SquareDBEntities())
            {
                var adModelInfo = await db.Ads.FirstOrDefaultAsync(adModel => adModel.AdID == adID && !adModel.IsRequest);

                var similarAdModels = await db.Ads
                    .Where(adModel => !adModel.IsArchive && !adModel.IsRequest && adModel.AdID != adModelInfo.AdID
                                      && adModel.CityID == adModelInfo.CityID
                                      && adModel.DistrictID == adModelInfo.DistrictID
                                      && adModel.RoomsCount == adModelInfo.RoomsCount
                                      && ((adModelInfo.Price - 2000) <= adModel.Price && adModel.Price <= (adModelInfo.Price + 2000)))
                    .Take(3)
                    .ToListAsync();

                var ad = AdMapper.MapToAd(adModelInfo);

                var blobs = await db.Blobs
                .Where(blob => blob.AdInfoID == adID)
                .Select(blobModel =>
                    new ImageAttachment
                    {
                        ImageAttachmentID = blobModel.BlobID,
                        NewFileName = blobModel.NewFileName,
                        FileName = blobModel.FileName,
                        AdID = blobModel.AdInfoID ?? 0,
                        IsMain = blobModel.IsMain ?? false
                    })
                .ToListAsync();

                ad.ImageAttachments = blobs;
                return ad;
            }
        }

        public async Task AddAdReportAsync(AdReport adReport)
        {
            using (var db = new SquareDBEntities())
            {
                var adReportModel = new AdReportModel
                {
                    AdFromRealtor = adReport.AdFromRealtor,
                    AdLeased = adReport.AdLeased,
                    AdIncorrectData = adReport.AdIncorrectData,
                    IncorrectPhoneNumber = adReport.IncorrectPhoneNumber,
                    AdID = adReport.AdId
                };

                db.AdReportModels.Add(adReportModel);

                await db.SaveChangesAsync();
            }
        }

        public async Task<IEnumerable<AdReport>> GetAdReportsAsync()
        {
            using (var db = new SquareDBEntities())
            {
                var adReportModels =  await  db.AdReportModels.ToListAsync();

                var adReports = adReportModels
                    .Select(adReportModel => new AdReport
                    {
                        AdId = adReportModel.AdID,
                        AdReportId = adReportModel.AdReportID,
                        AdFromRealtor = adReportModel.AdFromRealtor ?? false,
                        AdLeased = adReportModel.AdLeased ?? false,
                        AdIncorrectData = adReportModel.AdIncorrectData ?? false,
                        IncorrectPhoneNumber = adReportModel.IncorrectPhoneNumber ?? false
                    })
                    .ToList();

                return adReports;
            }
        }

        /// <summary>
        /// Возвращает список заявок.
        /// </summary>
        public async Task<DataTable<Ad>> GetAdRequestsAsync(AdFilter filter, int page, int take)
        {
                using (var db = new SquareDBEntities())
                {
                    var adModels = db.Ads
                        .Where(adModel => adModel.IsRequest && !adModel.IsArchive)
                        .Select(adModel => new Ad
                        {
                            AdID = adModel.AdID,
                            Title = adModel.Title,
                            Kladr = adModel.Kladr,
                            Description = adModel.Description,
                            Phone = new AdPhone
                            {
                                PhoneID = adModel.Phone.PhoneID,
                                PhoneNumber = adModel.Phone.PhoneNumber,
                                AdditionalPhoneNumber = adModel.Phone.AdditionalPhoneNumber
                            },
                            LocationInfo = new AdLocationInfo
                            {
                                CityID = adModel.CityID ?? 0,
                                DistrictID = adModel.DistrictID ?? 0,
                            },
                            Fio = adModel.Fio,
                            AdditionalInfo = adModel.AdditionalInfo,
                            UpdatedDate = adModel.UpdatedDate,
                            ViewsCount = adModel.ViewsCount,
                            Price = adModel.Price,
                            RoomsCount = adModel.RoomsCount,
                            Comfort = new Comfort
                            {
                                ComfortID = adModel.Comfort.ComfortID,
                                AirConditioning = adModel.Comfort.AirConditioning,
                                Bath = adModel.Comfort.Bath,
                                Fridge = adModel.Comfort.Fridge,
                                Internet = adModel.Comfort.Internet,
                                KitchenFurniture = adModel.Comfort.KitchenFurniture,
                                RoomsFurniture = adModel.Comfort.RoomsFurniture,
                                Shower = adModel.Comfort.Shower,
                                Washer = adModel.Comfort.Washer
                            }
                        });

                    if (filter.CityID > 0)
                    {
                        adModels = adModels.Where(adModel => adModel.LocationInfo.CityID == filter.CityID);
                    }

                    if (filter.DistrictID > 0)
                    {
                        adModels = adModels.Where(adModel => adModel.LocationInfo.DistrictID == filter.DistrictID);
                    }

                    if (filter.PriceFrom > 0)
                    {
                        adModels = adModels.Where(adModel => adModel.Price >= filter.PriceFrom);
                    }

                    if (filter.PriceTo > 0)
                    {
                        adModels = adModels.Where(adModel => adModel.Price <= filter.PriceTo);
                    }

                    if (filter.RoomsCountSelect != null && filter.RoomsCountSelect.Count > 0)
                    {
                        var roomsList = GetRoomsCount(filter.RoomsCountSelect);

                        adModels = adModels.Where(adModel => roomsList.Contains(adModel.RoomsCount));
                    }

                    if (!string.IsNullOrWhiteSpace(filter.Search) && filter.Search.Length > 1)
                    {
                        filter.Search = filter.Search.Trim();

                        var searchComponents = SplitSearch(filter.Search.ToLower()).ToList();

                        adModels = adModels.Where(adModel =>
                            searchComponents.Any(word => adModel.Title.ToLower().Contains(word)));
                    }

                    if (!string.IsNullOrWhiteSpace(filter.PhoneNumber))
                    {
                        adModels = adModels.Where(adModel => adModel.Phone.PhoneNumber.Contains(filter.PhoneNumber));
                    }

                    var query = adModels.AsNoTracking();

                    var total = await query.CountAsync();
                    var ads = new List<Ad>();


                    switch (filter.OrderedBy)
                    {
                        case 1:
                            ads = await query
                                .OrderByDescending(adModel => adModel.Price)
                                .Skip((page - 1) * take)
                                .Take(take)
                                .ToListAsync();
                            break;
                        case 2:
                            ads = await query
                                .OrderBy(adModel => adModel.Price)
                                .Skip((page - 1) * take)
                                .Take(take)
                                .ToListAsync();
                            break;
                        case 3:
                            ads = await query
                                .OrderBy(adModel => adModel.UpdatedDate)
                                .Skip((page - 1) * take)
                                .Take(take)
                                .ToListAsync();
                            break;
                        default:
                            ads = await query
                                .OrderByDescending(adModel => adModel.UpdatedDate)
                                .Skip((page - 1) * take)
                                .Take(take)
                                .ToListAsync();
                            break;
                    }

                    foreach (var ad in ads)
                    {
                        ad.ImageAttachments = await db.Blobs
                            .Where(blob => blob.AdInfoID == ad.AdID)
                            .Select(blobModel =>
                                new ImageAttachment
                                {
                                    ImageAttachmentID = blobModel.BlobID,
                                    NewFileName = blobModel.NewFileName,
                                    FileName = blobModel.FileName,
                                    AdID = blobModel.AdInfoID ?? 0,
                                    IsMain = blobModel.IsMain ?? false
                                })
                            .ToListAsync();
                    }

                    var dataTable = new DataTable<Ad>
                    {
                        Items = ads,
                        Total = total
                    };

                    return dataTable;
                }
        }

        /// <summary>
        /// Возвращает объявление по идентификатору.
        /// </summary>
        public async Task<Ad> GetAdByIDAsync(int adId)
        {
            using (var db = new SquareDBEntities())
            {
                var adModelInfo = await db.Ads.FirstOrDefaultAsync(adModel => adModel.AdID == adId && !adModel.IsRequest);

                var similarAdModels = await db.Ads
                    .Where(adModel => !adModel.IsArchive && !adModel.IsRequest && adModel.AdID != adModelInfo.AdID
                                      && adModel.CityID == adModelInfo.CityID
                                      && adModel.DistrictID == adModelInfo.DistrictID
                                      && adModel.RoomsCount == adModelInfo.RoomsCount
                                      && ((adModelInfo.Price - 2000) <= adModel.Price && adModel.Price <= (adModelInfo.Price + 2000)))
                    .Take(3)
                    .ToListAsync();

                var ad = AdMapper.MapToAd(adModelInfo);

                var blobs = await db.Blobs
                .Where(blob => blob.AdInfoID == adId)
                .Select(blobModel =>
                    new ImageAttachment
                    {
                        ImageAttachmentID = blobModel.BlobID,
                        NewFileName = blobModel.NewFileName,
                        FileName = blobModel.FileName,
                        AdID = blobModel.AdInfoID ?? 0,
                        IsMain = blobModel.IsMain ?? false
                    })
                .ToListAsync();

                var similarAds = similarAdModels.Select(AdMapper.MapToAd).ToList();


                foreach (var similarAd in similarAds)
                {
                    similarAd.ImageAttachments = await db.Blobs
                        .Where(blob => blob.AdInfoID == similarAd.AdID)
                        .Select(blobModel =>
                            new ImageAttachment
                            {
                                ImageAttachmentID = blobModel.BlobID,
                                NewFileName = blobModel.NewFileName,
                                FileName = blobModel.FileName,
                                AdID = blobModel.AdInfoID ?? 0,
                                IsMain = blobModel.IsMain ?? false
                            })
                        .ToListAsync();
                }

                ad.ImageAttachments = blobs;
                ad.SimilarAds = similarAds;
                return ad;
            }
        }

        /// <summary>
        /// Возвращает заявку по идентификатору.
        /// </summary>
        public async Task<Ad> GetAdRequestByIDAsync(int adId)
        {
            using (var db = new SquareDBEntities())
            {
                var adModelInfo = await db.Ads.FirstOrDefaultAsync(adModel => adModel.AdID == adId && adModel.IsRequest);

                var ad = AdMapper.MapToAd(adModelInfo);
                var blobs = await db.Blobs
                    .Where(blob => blob.AdInfoID == adId)
                    .Select(blobModel =>
                        new ImageAttachment
                        {
                            ImageAttachmentID = blobModel.BlobID,
                            NewFileName = blobModel.NewFileName,
                            FileName = blobModel.FileName,
                            AdID = blobModel.AdInfoID ?? 0
                        })
                    .ToListAsync();

                ad.ImageAttachments = blobs;

                return ad;
            }
        }

        /// <summary>
        /// Добавляет новое объявление.
        /// </summary>
        public async Task<int> AddNewAdAsync(Ad ad)
        {
            using (var db = new SquareDBEntities())
            {
                var adModel = AdMapper.MapToAdModel(ad);
                var comfortModel = AdMapper.MapToComfortModel(ad.Comfort);

                var dateTimeNow = DateTime.UtcNow;
                adModel.UpdatedDate = dateTimeNow;
                adModel.Comfort = comfortModel;

                db.Ads.Add(adModel);

                 await db.SaveChangesAsync();

                var blobs = new List<Blob>();

                //var bitMap = new System.Drawing.Bitmap();

                foreach (var imageAttachment in ad.ImageAttachments)
                {
                    var blob = new Blob
                    {
                        AdInfoID = adModel.AdID,
                        FileName = imageAttachment.FileName,
                        NewFileName = imageAttachment.FileName,
                        IsMain = imageAttachment.IsMain
                    };

                    blobs.Add(blob);
                }

                db.Blobs.AddRange(blobs);

                await db.SaveChangesAsync();

                return adModel.AdID;
            }
        }

        /// <summary>
        /// Обновляет объявление.
        /// </summary>
        public async Task UpdateAdAsync(Ad ad)
        {
            using (var db = new SquareDBEntities())
            { 
                var adModelInfo = await db.Ads.FirstOrDefaultAsync(adModel => adModel.AdID == ad.AdID);
                if (adModelInfo == null)
                {
                   throw new Exception("не нашел объявления для редактирования");
                }

                FillAdModel(ad, adModelInfo);

                await db.SaveChangesAsync();
            }
        }

        /// <summary>
        /// Удаляет объявление по идентификатору.
        /// </summary>
        public async Task DeleteAdByIDAsync(int adId)
        {
            using (var db = new SquareDBEntities())
            {
                var adModelInfo = await db.Ads.FirstOrDefaultAsync(adModel => adModel.AdID == adId);
                if (adModelInfo == null)
                {
                    throw new Exception("не нашел объявления для редактирования");
                }

                adModelInfo.IsArchive = true;

                await db.SaveChangesAsync();
            }
        }

        /// <summary>
        /// Обновляет количество просмотров у объявления.
        /// </summary>
        public async Task UpdateViewsCountAsync(int adID)
        {
            using (var db = new SquareDBEntities())
            {
                var adModelInfo = await db.Ads.FirstOrDefaultAsync(adModel => adModel.AdID == adID);
                if (adModelInfo == null)
                {
                    throw new Exception("не нашел объявления для редактирования");
                }

                adModelInfo.ViewsCount++;

                await db.SaveChangesAsync();
            }
        }
        
        /// <summary>
        /// Добавляет заявку.
        /// </summary>
        public async Task<int> AddAdRequestAsync(Ad ad)
        {
            using (var db = new SquareDBEntities())
            {
                var adModel = AdMapper.MapToAdModel(ad);

                var dateTimeNow = DateTime.UtcNow;
                adModel.UpdatedDate = dateTimeNow;
                adModel.IsRequest = true;

                db.Ads.Add(adModel);

                await db.SaveChangesAsync();

                var blobs = new List<Blob>();

                //var bitMap = new System.Drawing.Bitmap();

                foreach (var imageAttachment in ad.ImageAttachments)
                {

                    var blob = new Blob
                    {
                        AdInfoID = adModel.AdID,
                        FileName = imageAttachment.FileName,
                        NewFileName = imageAttachment.FileName,
                        IsMain = imageAttachment.IsMain
                    };

                    blobs.Add(blob);
                }

                db.Blobs.AddRange(blobs);
                await db.SaveChangesAsync();

                return adModel.AdID;
            }
        }

        /// <summary>
        /// Возвращает архив.
        /// </summary>
        public async Task<DataTable<Ad>> GetAdArchiveAsync(AdFilter filter, int page, int take)
        {
            using (var db = new SquareDBEntities())
            {
                var adModels = db.Ads
                    .Where(adModel => adModel.IsArchive)
                    .Select(adModel => new Ad
                    {
                        AdID = adModel.AdID,
                        Title = adModel.Title,
                        Kladr = adModel.Kladr,
                        Description = adModel.Description,
                        Phone = new AdPhone
                        {
                            PhoneID = adModel.Phone.PhoneID,
                            PhoneNumber = adModel.Phone.PhoneNumber,
                            AdditionalPhoneNumber = adModel.Phone.AdditionalPhoneNumber
                        },
                        LocationInfo = new AdLocationInfo
                        {
                            CityID = adModel.CityID ?? 0,
                            DistrictID = adModel.DistrictID ?? 0,
                        },
                        Fio = adModel.Fio,
                        AdditionalInfo = adModel.AdditionalInfo,
                        UpdatedDate = adModel.UpdatedDate,
                        ViewsCount = adModel.ViewsCount,
                        Price = adModel.Price,
                        RoomsCount = adModel.RoomsCount,
                        Comfort = new Comfort
                        {
                            ComfortID = adModel.Comfort.ComfortID,
                            AirConditioning = adModel.Comfort.AirConditioning,
                            Bath = adModel.Comfort.Bath,
                            Fridge = adModel.Comfort.Fridge,
                            Internet = adModel.Comfort.Internet,
                            KitchenFurniture = adModel.Comfort.KitchenFurniture,
                            RoomsFurniture = adModel.Comfort.RoomsFurniture,
                            Shower = adModel.Comfort.Shower,
                            Washer = adModel.Comfort.Washer
                        }
                    });

                if (filter.CityID > 0)
                {
                    adModels = adModels.Where(adModel => adModel.LocationInfo.CityID == filter.CityID);
                }

                if (filter.DistrictID > 0)
                {
                    adModels = adModels.Where(adModel => adModel.LocationInfo.DistrictID == filter.DistrictID);
                }

                if (filter.PriceFrom > 0)
                {
                    adModels = adModels.Where(adModel => adModel.Price >= filter.PriceFrom);
                }

                if (filter.PriceTo > 0)
                {
                    adModels = adModels.Where(adModel => adModel.Price <= filter.PriceTo);
                }

                if (filter.RoomsCountSelect != null && filter.RoomsCountSelect.Count > 0)
                {
                    var roomsList = GetRoomsCount(filter.RoomsCountSelect);

                    adModels = adModels.Where(adModel => roomsList.Contains(adModel.RoomsCount));
                }

                if (!string.IsNullOrWhiteSpace(filter.Search) && filter.Search.Length > 1)
                {
                    filter.Search = filter.Search.Trim();

                    var searchComponents = SplitSearch(filter.Search.ToLower()).ToList();

                    adModels = adModels.Where(adModel =>
                        searchComponents.Any(word => adModel.Title.ToLower().Contains(word)));
                }

                if (!string.IsNullOrWhiteSpace(filter.PhoneNumber))
                {
                    adModels = adModels.Where(adModel => adModel.Phone.PhoneNumber.Contains(filter.PhoneNumber));
                }

                var query = adModels.AsNoTracking();

                var total = await query.CountAsync();
                var ads = new List<Ad>();


                switch (filter.OrderedBy)
                {
                    case 1:
                        ads = await query
                            .OrderByDescending(adModel => adModel.Price)
                            .Skip((page - 1) * take)
                            .Take(take)
                            .ToListAsync();
                        break;
                    case 2:
                        ads = await query
                            .OrderBy(adModel => adModel.Price)
                            .Skip((page - 1) * take)
                            .Take(take)
                            .ToListAsync();
                        break;
                    case 3:
                        ads = await query
                            .OrderBy(adModel => adModel.UpdatedDate)
                            .Skip((page - 1) * take)
                            .Take(take)
                            .ToListAsync();
                        break;
                    default:
                        ads = await query
                            .OrderByDescending(adModel => adModel.UpdatedDate)
                            .Skip((page - 1) * take)
                            .Take(take)
                            .ToListAsync();
                        break;
                }

                foreach (var ad in ads)
                {
                    ad.ImageAttachments = await db.Blobs
                        .Where(blob => blob.AdInfoID == ad.AdID)
                        .Select(blobModel =>
                            new ImageAttachment
                            {
                                ImageAttachmentID = blobModel.BlobID,
                                NewFileName = blobModel.NewFileName,
                                FileName = blobModel.FileName,
                                AdID = blobModel.AdInfoID ?? 0,
                                IsMain = blobModel.IsMain ?? false
                            })
                        .ToListAsync();
                }

                var dataTable = new DataTable<Ad>
                {
                    Items = ads,
                    Total = total
                };

                return dataTable;
            }
        }

        /// <summary>
        /// Перемещает заявку в основной список объявлений.
        /// </summary>
        public async Task MoveAdFromRequestsToAdsAsync(int adID)
        {
            using (var db = new SquareDBEntities())
            {
                var adModelInfo = await db.Ads.FirstOrDefaultAsync(adModel => adModel.AdID == adID);
                if (adModelInfo == null)
                {
                    throw new Exception("не нашел объявления для редактирования");
                }

                adModelInfo.IsRequest = false;

                await db.SaveChangesAsync();
            }
        }

        /// <summary>
        /// Перемещает заявку из архива в основной список объявлений.
        /// </summary>
        public async Task MoveAdFromArchiveToAdsAsync(int adID)
        {
            using (var db = new SquareDBEntities())
            {
                var adModelInfo = await db.Ads.FirstOrDefaultAsync(adModel => adModel.AdID == adID);
                if (adModelInfo == null)
                {
                    throw new Exception("не нашел объявления для редактирования");
                }

                adModelInfo.ViewsCount = 0;
                adModelInfo.IsArchive = false;

                await db.SaveChangesAsync();
            }
        }

        public async Task AddAvitoAdAsync(Ad ad)
        {
            using (var db = new SquareDBEntities())
            {
                try
                {

                var adModel = AdMapper.MapToAdModel(ad);
                var comfortModel = AdMapper.MapToComfortModel(ad.Comfort);

                var dateTimeNow = DateTime.UtcNow;
                adModel.UpdatedDate = dateTimeNow;
                adModel.Comfort = comfortModel;

                db.Ads.Add(adModel);

                await db.SaveChangesAsync();

                var blobs = new List<Blob>();

                //var bitMap = new System.Drawing.Bitmap();

                foreach (var imageAttachment in ad.ImageAttachments)
                {
                    var blob = new Blob
                    {
                        AdInfoID = adModel.AdID,
                        FileName = imageAttachment.FileName,
                        NewFileName = imageAttachment.FileName,
                        IsMain = imageAttachment.IsMain
                    };

                    blobs.Add(blob);
                }

                db.Blobs.AddRange(blobs);

                await db.SaveChangesAsync();

                }
                catch (Exception)
                {

                    throw;
                }
            }
        }

        public async Task<List<string>> GetAllPhones()
        {
            using (var db = new SquareDBEntities())
            {
                var phns = await db.Phones
                    .Select(pModel => pModel.PhoneNumber)
                    .ToListAsync();

                return phns;
            }
        }

        /// <summary>
        /// Заполняет объект adModel.
        /// </summary>
        private static void FillAdModel(Ad ad, AdModel adModel)
        {
            adModel.Title = ad.Title;
            adModel.Kladr = ad.Kladr;
            adModel.Description = ad.Description;
            adModel.UpdatedDate = DateTime.UtcNow;
            adModel.CityID = ad.LocationInfo.CityID;
            adModel.DistrictID = ad.LocationInfo.DistrictID;
            adModel.AdditionalInfo = ad.AdditionalInfo;
            adModel.Fio = ad.Fio;
            adModel.Price = ad.Price;
            adModel.RoomsCount = ad.RoomsCount;

            if (adModel.Phone != null && ad.Phone != null)
            {
                adModel.Phone.PhoneNumber = ad.Phone.PhoneNumber;
                adModel.Phone.AdditionalPhoneNumber = ad.Phone.AdditionalPhoneNumber;
            }
        }
    }
}
