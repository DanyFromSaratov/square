﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Threading.Tasks;
using Square.Common;
using Square.Common.Entities.Ad;
using Square.Common.Entities.Portal;
using Square.Common.Mappers;
using Square.OriginDataBase;
using Square.Services.Contracts.Repositories;
using Comment = Square.Common.Entities.Common.Comment;

namespace Square.Services.Implementation.Repositories
{
    /// <summary>
    /// Репозиторий для работы с пользователями.
    /// </summary>
    public class UserRepository : IUserRepository
    {
        

        public UserRepository()
        {

        }

        /// <summary>
        /// Регистрирует нового пользователя.
        /// </summary>
        public async Task RegisterUserAsync(User user)
        {


            using (var db = new SquareDBEntities())
            {
                var userModel = UserMapper.MapToUserModel(user);

                userModel.RoleID = (int)SquareRoleEnum.User;

                db.SquareUserModels.Add(userModel);

                await db.SaveChangesAsync();
            }
        }

        /// <summary>
        /// Возвращает пользователя по Email.
        /// </summary>
        public async Task<User> GetUserByEmailAsync(string email)
        {
            using (var db = new SquareDBEntities())
            {
                var userModel = await db.SquareUserModels.FirstOrDefaultAsync(userFromDB => userFromDB.Email == email);

                var user = UserMapper.MapToUser(userModel);

                return user;
            }
        }

        /// <summary>
        /// Возвращает пользователя по номеру телефона и паролю.
        /// </summary>
        public async Task<User> GetUserByIDAsync(int userID)
        {
            using (var db = new SquareDBEntities())
            {
                var userModel = await db.SquareUserModels.FirstOrDefaultAsync(userFromDB => userFromDB.SquareUserID == userID);

                var user = UserMapper.MapToUser(userModel);

                return user;
            }
        }

        /// <summary>
        /// Возвращает признак того, содержится ли пользователь с текущим Email.
        /// </summary>
        public async Task<bool> IsUserExistedAsync(string email)
        {
            using (var db = new SquareDBEntities())
            {
                var userModel = await db.SquareUserModels.FirstOrDefaultAsync(userFromDB => userFromDB.Email == email);

                return userModel != null;
            }
        }

        /// <summary>
        /// Возвращает признак того, имеет ли пользователь заданную роль.
        /// </summary>
        public bool IsUserInRole(string username, string roleName)
        {
            using (var db = new SquareDBEntities())
            {
                var userModel = db.SquareUserModels.FirstOrDefault(userFromDB => userFromDB.PhoneNumber == username);
                if (userModel == null)
                {
                    return false;
                }

                return userModel.SquareRole.Name == roleName;
            }
        }

        /// <summary>
        /// Возвращает роли пользователя по логину(username).
        /// </summary>
        public IEnumerable<string> GetRolesForUser(string username)
        {
            using (var db = new SquareDBEntities())
            {
                var roleModels = db.SquareRoleModels
                    .Where(roleFromDB => roleFromDB.SquareUsers.Any(userFromDB => userFromDB.PhoneNumber == username))
                    .ToList();

                var roles = roleModels
                    .Select(roleModel => roleModel.Name)
                    .ToList();

                return roles;
            }
        }

        /// <summary>
        /// Возвращает список подписок.
        /// </summary>
        public IEnumerable<Subscription> GetSubscriptionsAsync()
        {
            using (var db = new SquareDBEntities())
            {
                var subscriptions = db.SubscriptionModels
                    .Select(UserMapper.MapToSubscription)
                    .ToList();

                return subscriptions;
            }
        }

        /// <summary>
        /// Возвращает действующую подписку пользователя.
        /// </summary>
        public async Task<Subscription> GetUserCurrentSubscriptionAsync(int userID)
        {
            using (var db = new SquareDBEntities())
            {
                var subscriptionModel = await db.UserSubscriptionModels.FirstOrDefaultAsync(subscriptionFromDB => subscriptionFromDB.IsActive
                        && subscriptionFromDB.UserID == userID);

                var subscription = UserMapper.MapToUserSubscription(subscriptionModel);

                return subscription;
            }
        }

        /// <summary>
        /// Подключает подписку для пользователя.
        /// </summary>
        public async Task ConnectSubscriptionToUserAsync(Subscription subscription)
        {
            using (var db = new SquareDBEntities())
            {
                var startEndDate = GetStartEndDate(subscription.Type);

                var userSubscriptionModel = new UserSubscriptionModel
                {
                    UserID = subscription.UserID,
                    SubscriptionID = (int)subscription.Type,
                    IsActive = true,
                    DateFrom = startEndDate.startDate,
                    DateTo = startEndDate.endDate
                };

                var userModelInfo = await db.SquareUserModels.FirstOrDefaultAsync(userModel => userModel.SquareUserID == subscription.UserID);
                if (userModelInfo == null)
                {
                    throw new Exception("User не найден");
                }

                db.UserSubscriptionModels.Add(userSubscriptionModel);

                userModelInfo.RoleID = (int)SquareRoleEnum.Subscriber;

                await db.SaveChangesAsync();
            }
        }

        public async Task CheckAndUpdateUserSubscriptionAsync()
        {
            // Саратовское время
            var dateTimeNowSaratov = DateTime.UtcNow.AddHours(4);

            using (var db = new SquareDBEntities())
            {
                var userSubscriptions = await db.UserSubscriptionModels
                    .Where(userSubscription => userSubscription.IsActive
                                               && userSubscription.DateTo < dateTimeNowSaratov)
                    .ToListAsync();

                foreach (var userSubscription in userSubscriptions)
                {
                    userSubscription.IsActive = false;
                    await db.SaveChangesAsync();

                    var userModel = await db.SquareUserModels.FirstOrDefaultAsync(userModelInfo => userModelInfo.SquareUserID == userSubscription.UserID);
                    if (userModel != null)
                    {
                        userModel.RoleID = (int)SquareRoleEnum.User;
                    }
                }

                await db.SaveChangesAsync();
            }
        }

        public async Task CheckAndUpdateFreeViewsAsync()
        {
            using (var db = new SquareDBEntities())
            {
                var freeViewsExpired = await db.FreeViews
                    .Where(freeView => freeView.ViewsCount > 1)
                    .ToListAsync();

                foreach (var freeViewExpired in freeViewsExpired)
                {
                    var userModel = await db.SquareUserModels.FirstOrDefaultAsync(userModelInfo => userModelInfo.SquareUserID == freeViewExpired.UserID);
                    if (userModel != null && userModel.SquareRole.RoleID == (int)SquareRoleEnum.User)
                    {
                        userModel.RoleID = (int) SquareRoleEnum.Guest;
                    }
                }

                await db.SaveChangesAsync();
            }
        }

        public async Task UpdateFreeViewsAsync(int userID)
        {
            using (var db = new SquareDBEntities())
            {
                var fv = await db.FreeViews.FirstOrDefaultAsync(f => f.UserID == userID);

                if (fv == null)
                {
                    db.FreeViews.Add(new FreeView {UserID = userID});
                    await db.SaveChangesAsync();

                    return;
                }

                fv.ViewsCount++;

                await db.SaveChangesAsync();
            }
        }

        private static (DateTime startDate, DateTime endDate) GetStartEndDate(SubscriptionEnum type)
        {
            var dateTimeSaratovNow = DateTime.UtcNow.AddHours(4);

            switch (type)
            {
                case SubscriptionEnum.Base:
                    return (dateTimeSaratovNow, dateTimeSaratovNow.AddDays(7));

                case SubscriptionEnum.Optimal:
                    return (dateTimeSaratovNow, dateTimeSaratovNow.AddDays(14));

                case SubscriptionEnum.Maximal:
                    return (dateTimeSaratovNow, dateTimeSaratovNow.AddDays(30));

                default:
                    throw new Exception("GetStartEndDate error");
            }
        }

        /// <summary>
        /// Возвращает пользователя по номеру телефона и паролю.
        /// </summary>
        public async Task<User> GetUserByPhoneNumberAndPasswordAsync(string phoneNumber, string password)
        {
            using (var db = new SquareDBEntities())
            {
                var userModel = await db.SquareUserModels.FirstOrDefaultAsync(userFromDB => userFromDB.PhoneNumber == phoneNumber
                                                                                 && userFromDB.Password == password);

                var user = UserMapper.MapToUser(userModel);

                return user;
            }
        }

        /// <summary>
        /// Возвращает признак того,
        /// содержится ли пользователь по номеру телефона(логину).
        /// </summary>
        public async Task<bool> IsUserExistedByPhoneNumberAsync(string phoneNumber)
        {
            using (var db = new SquareDBEntities())
            {
                var userModel = await db.SquareUserModels.FirstOrDefaultAsync(userFromDB => userFromDB.PhoneNumber == phoneNumber);

                return userModel != null;
            }
        }

        /// <summary>
        /// Заказывает звонок.
        /// </summary>
        public async Task OrderCallAsync(string phoneNumber)
        {
            if (string.IsNullOrWhiteSpace(phoneNumber))
            {
                return;
            }

            using (var db = new SquareDBEntities())
            {
                if (db.OrderedCalls.Any(callModel => callModel.PhoneNumber == phoneNumber))
                {
                    return;
                }

                db.OrderedCalls.Add(new OrderedCall {PhoneNumber = phoneNumber});

                await db.SaveChangesAsync();
            }
        }

        /// <summary>
        /// Возвращает пользователя по номеру телефона.
        /// </summary>
        public async Task<User> GetUserByPhoneNumberAsync(string phoneNumber)
        {
            using (var db = new SquareDBEntities())
            {
                var userModel = await db.SquareUserModels.FirstOrDefaultAsync(userFromDB => userFromDB.PhoneNumber == phoneNumber);

                var user = UserMapper.MapToUser(userModel);

                var orderModel = await db.Orders.FirstOrDefaultAsync(orderInfoModel => orderInfoModel.OrderID == 1);
                if (orderModel == null)
                {
                    throw new Exception("При покупке подписки произошла ошибка orderInfo не найден");
                }

                var newOrderNumber = orderModel.Info + 1;

                orderModel.Info = newOrderNumber;

                await db.SaveChangesAsync();

                user.OrderNumber = newOrderNumber;

                return user;
            }
        }

        /// <summary>
        /// Возвращает список заказанных звонков.
        /// </summary>
        public async Task<IEnumerable<string>> GetOrderedCallsAsync()
        {
            using (var db = new SquareDBEntities())
            {
                var calls = await  db.OrderedCalls
                    .Select(callModel => callModel.PhoneNumber)
                    .ToListAsync();

                return calls;
            }
        }

        /// <summary>
        /// Удаляет заказанный звонок по номеру телефона.
        /// </summary>
        public async Task DeleteOrderedCallAsync(string phoneNumber)
        {
            if (string.IsNullOrWhiteSpace(phoneNumber))
            {
                return;
            }

            using (var db = new SquareDBEntities())
            {
                var orderedCallModels = await db.OrderedCalls
                    .Where(callModel => callModel.PhoneNumber == phoneNumber)
                    .ToListAsync();

                db.OrderedCalls.RemoveRange(orderedCallModels);

                await db.SaveChangesAsync();
            }
        }

        /// <summary>
        /// Возвращает список пользователей.
        /// </summary>
        public async Task<IEnumerable<User>> GetUsersAsync()
        {
            using (var db = new SquareDBEntities())
            {
                var userModels = await db.SquareUserModels
                    .Select(userModel => userModel)
                    .ToListAsync();

                var users = userModels
                    .Select(userModel => UserMapper.MapToUser(userModel))
                    .ToList();

                return users;
            }
        }

        /// <summary>
        /// Удаляет пользователя по идентификатору.
        /// </summary>
        public async Task RemoveUserAsync(int userID)
        {
            using (var db = new SquareDBEntities())
            {
                var userModelInfo = await db.SquareUserModels.FirstOrDefaultAsync(userModel => userModel.SquareUserID == userID);
                if (userModelInfo == null)
                {
                    return;
                }
                db.SquareUserModels.Remove(userModelInfo);
                
                await db.SaveChangesAsync();
            }
        }

        /// <summary>
        /// Изменяет роль пользователю.
        /// </summary>
        public async Task ChangeUserRole(int userID, SquareRoleEnum role)
        {
            using (var db = new SquareDBEntities())
            {

                var userModelInfo = await db.SquareUserModels.FirstOrDefaultAsync(userModel => userModel.SquareUserID == userID);
                if (userModelInfo == null)
                {
                    return;
                }

                if ((int)role == userModelInfo.RoleID)
                {
                    return; 
                }

                userModelInfo.RoleID = (int) role;

                await db.SaveChangesAsync();
            }
        }

        public IEnumerable<Subscription> GetSubscriptionsByUserIdAsync(int userId)
        {
            using (var db = new SquareDBEntities())
            {
                var subscriptions = db.UserSubscriptionModels
                    .Where(subscriptionModel => subscriptionModel.UserID == userId)
                    .AsEnumerable()
                    .Select(UserMapper.MapToSubscription)
                    .OrderByDescending(subscription => subscription.IsActive)
                    .ThenByDescending(subscription => subscription.StartDate)
                    .Take(3)
                    .ToList();

                return subscriptions;
            }
        }

        public async Task UpdateUserInfoAsync(User userInfo)
        {
            if (userInfo == null)
            {
                return;
            }

            using (var db = new SquareDBEntities())
            {
                var userModelInfo = await db.SquareUserModels.FirstOrDefaultAsync(userModel => userModel.SquareUserID == userInfo.UserID);
                if (userModelInfo == null)
                {
                    throw new Exception("не нашел такого юзера для редактирования");
                }

                userModelInfo.LastName = userInfo.LastName;
                userModelInfo.FirstName = userInfo.FirstName;
                userModelInfo.Email = userInfo.Email;

                if (!string.IsNullOrWhiteSpace(userInfo.MainImage))
                {
                    userModelInfo.MainImage = userInfo.MainImage;
                }

                await db.SaveChangesAsync();
            }
        }

        public async Task<IEnumerable<Comment>> GetCommentsAsync()
        {
            using (var db = new SquareDBEntities())
            {
                var comments = await db.CommentModels
                    .Select(commentModel => new Comment
                    {
                        CommentID = commentModel.CommentID,
                        UserID = commentModel.UserID,
                        Text = commentModel.Text,
                        Fio = commentModel.Fio
                    })
                    .ToListAsync();

                foreach (var comment in comments)
                {
                    var userInfo = await db.SquareUserModels.FirstOrDefaultAsync(user => user.SquareUserID == comment.UserID);
                    if (userInfo != null)
                    {
                        comment.UserImage = userInfo.MainImage;
                    }
                }

                return comments;
            }
        }

        public async Task AddCommentAsync(Comment comment)
        {
            if (comment == null)
            {
                throw new Exception("Ошибка в добавлении комментария");
            }

            using (var db = new SquareDBEntities())
            {
                var commentModel = new CommentModel
                {
                    UserID = comment.UserID,
                    Text = comment.Text,
                    Fio = comment.Fio
                };

                db.CommentModels.Add(commentModel);

                await db.SaveChangesAsync();
            }
        }

        public async Task AddNewsAsync(News news)
        {
            if (news == null)
            {
                throw new Exception("Ошибка в добавлении новости");
            }

            using (var db = new SquareDBEntities())
            {
                var newsModel = new NewsModel
                {
                   Title = news.NewsTitle,
                   Content = news.Content,
                   MainImgName = news.MainImgName
                };

                var dateTime = DateTime.UtcNow.AddHours(4);
                newsModel.UpdatedDate = dateTime;

                db.News.Add(newsModel);

                await db.SaveChangesAsync();
            }
        }

        public async Task<IEnumerable<News>> GetNewsAsync()
        {
            using (var db = new SquareDBEntities())
            {
                var news = await db.News
                    .Select(newsModel => new News
                    {
                       NewsID = newsModel.NewsID,
                       NewsTitle = newsModel.Title,
                       Content = newsModel.Content,
                       MainImgName = newsModel.MainImgName,
                       UpdatedDate = newsModel.UpdatedDate ?? DateTime.Now
                    })
                    .ToListAsync();

                return news;
            }
        }

        public async Task<News> GetNewsByIDAsync(int newsID)
        {
            using (var db = new SquareDBEntities())
            {
                var newsModel = await db.News.FirstOrDefaultAsync(newsModelInfo => newsModelInfo.NewsID == newsID);

                var news = new News
                {
                    NewsID = newsModel.NewsID,
                    NewsTitle = newsModel.Title,
                    Content = newsModel.Content,
                    MainImgName = newsModel.MainImgName,
                    UpdatedDate = newsModel.UpdatedDate ?? DateTime.Now
                };

                return news;
            }
        }

        public async Task AddSpecialistHelpRequestAsync(SpecialistHelper specialistHelper)
        {
            using (var db = new SquareDBEntities())
            {
                var specialistHelperModel = new SpecialistHelperModel
                {
                    UserName = specialistHelper.UserName,
                    UserPhoneNumber = specialistHelper.UserPhoneNumber
                };

                db.SpecialistHelperModels.Add(specialistHelperModel);

                await db.SaveChangesAsync();
            }
        }

        public async Task<IEnumerable<SpecialistHelper>> GetSpecialistHelpRequestsAsync()
        {
            using (var db = new SquareDBEntities())
            {
                var requests = await db.SpecialistHelperModels
                    .Select(specialistHelperModel => new SpecialistHelper
                    {
                        SepcialistHelperID = specialistHelperModel.SepcialistHelperID,
                        UserName = specialistHelperModel.UserName,
                        UserPhoneNumber = specialistHelperModel.UserPhoneNumber
                    })
                    .ToListAsync();

                return requests;
            }
        }

        public async Task AddAdToFavoriteAsync(Favorite favorite)
        {
            using (var db = new SquareDBEntities())
            {
                var favoriteModel = new FavoriteModel
                {
                    AdID = favorite.AdID,
                    UserID = favorite.UserID
                };

                var favoriteModelExisted = await db.FavoriteModels.FirstOrDefaultAsync(favoriteModelInfo =>
                    favoriteModelInfo.AdID == favoriteModel.AdID
                    && favoriteModelInfo.UserID == favoriteModel.UserID);
                if (favoriteModelExisted != null)
                {
                    return;
                }

                db.FavoriteModels.Add(favoriteModel);
                var adModel = db.Ads.FirstOrDefault(adModelInfo => adModelInfo.AdID == favorite.AdID);
                if (adModel != null)
                {
                    adModel.IsFavorite = true;
                }

                await db.SaveChangesAsync();
            }
        }

        public async Task RemoveAdFromFavoriteAsync(int userId, int adId)
        {
            using (var db = new SquareDBEntities())
            {
                var favoriteModel = await db.FavoriteModels.FirstOrDefaultAsync(favoriteModelInfo =>
                    favoriteModelInfo.AdID == adId
                    && favoriteModelInfo.UserID == userId);

                if (favoriteModel == null)
                {
                    return;
                }

                var adModel = db.Ads.FirstOrDefault(adModelInfo => adModelInfo.AdID == favoriteModel.AdID);
                if (adModel != null)
                {
                    adModel.IsFavorite = false;
                }

                db.FavoriteModels.Remove(favoriteModel);

                await db.SaveChangesAsync();
            }
        }

        public async Task<IEnumerable<Favorite>> GetFavoritesAsync(int userID)
        {
            using (var db = new SquareDBEntities())
            {
                var favorites = await db.FavoriteModels
                    .Where(favoriteModel => favoriteModel.UserID == userID)
                    .Select(favoriteModel => new Favorite
                    {
                        FavoriteID = favoriteModel.FavoriteID,
                        AdID = favoriteModel.AdID,
                        UserID = favoriteModel.UserID
                    })
                    .ToListAsync();

                foreach (var favorite in favorites)
                {
                    var adModelInfo =await db.Ads.FirstOrDefaultAsync(adModel => adModel.AdID == favorite.AdID);
                    if (adModelInfo != null)
                    {
                        favorite.Ad = new Ad
                        {
                            AdID = adModelInfo.AdID,
                            Title = adModelInfo.Title,
                            RoomsCount = adModelInfo.RoomsCount,
                            Price = adModelInfo.Price
                        };
                    }
                }

                return favorites;
            }
        }
    }
}
