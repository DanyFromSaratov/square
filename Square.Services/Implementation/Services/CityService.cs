﻿using System.Collections.Generic;
using System.Data.Common;
using System.Threading.Tasks;
using Square.Common.Entities.Ad;
using Square.Common.Entities.City;
using Square.Services.Contracts.Repositories;
using Square.Services.Contracts.Services;

namespace Square.Services.Implementation.Services
{
    /// <summary>
    /// Сервис для работы с городами.
    /// </summary>
    public class CityService : ICityService
    {
        private readonly ICityRepository _cityRepository;

        public CityService(ICityRepository cityRepository)
        {
            _cityRepository = cityRepository;
        }

        /// <summary>
        /// Возвращает список городов.
        /// </summary>
        public async Task<IEnumerable<City>> GetCitiesAsync()
        {
            return await _cityRepository.GetCitiesAsync();
        }
    }
}
