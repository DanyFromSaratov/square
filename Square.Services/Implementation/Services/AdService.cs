﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Square.Common;
using Square.Common.Entities.Ad;
using Square.Common.Entities.Common;
using Square.Services.Contracts.Repositories;
using Square.Services.Contracts.Services;

namespace Square.Services.Implementation.Services
{
    /// <summary>
    /// Сервис для работы с объявлениями.
    /// </summary>
    public class AdService : IAdService
    {
        private readonly IAdRepository _adRepository;

        public AdService(IAdRepository adRepository)
        {
            _adRepository = adRepository;
        }

        /// <summary>
        /// Возвращает список объявлений.
        /// </summary>
        public async Task<DataTable<Ad>> GetAdsAsync(AdFilter filter, int page, int take)
        {
            var dataTable = await _adRepository.GetAdsAsync(filter, page, take);

            return dataTable;
        }

        public async Task<DataTable<Ad>> GetAdRequestsAsync(AdFilter filter, int page, int take)
        {
            var dataTable = await _adRepository.GetAdRequestsAsync(filter, page, take);

            return dataTable;
        }

        /// <summary>
        /// Возвращает объявление по идентификатору.
        /// </summary>
        public Task<Ad> GetAdByIDAsync(int adID)
        {
            var ad = _adRepository.GetAdByIDAsync(adID);

            return ad;
        }

        public async Task<Ad> GetAdRequestByIDAsync(int adID)
        {
            return await _adRepository.GetAdRequestByIDAsync(adID);
        }

        /// <summary>
        /// Добавляет новое объявление.
        /// </summary>
        public async Task<int> AddNewAdAsync(Ad ad)
        {
            var adID = await _adRepository.AddNewAdAsync(ad);

            return adID;
        }

        /// <summary>
        /// Обновляет объявление.
        /// </summary>
        public async Task UpdateAdAsync(Ad ad)
        {
            await _adRepository.UpdateAdAsync(ad);
        }

        /// <summary>
        /// Удаляет объявление по идентификатору.
        /// </summary>
        public async Task DeleteAdByIDAsync(int adID)
        {
            await _adRepository.DeleteAdByIDAsync(adID);
        }

        public async Task UpdateViewsCountAsync(int adID)
        {
            await _adRepository.UpdateViewsCountAsync(adID);
        }

        public async Task<int> AddAdRequestAsync(Ad ad)
        {
            return await _adRepository.AddAdRequestAsync(ad);
        }

        public async Task<DataTable<Ad>> GetAdArchiveAsync(AdFilter filter, int page, int take)
        {
            var dataTable = await _adRepository.GetAdArchiveAsync(filter, page, take);

            return dataTable;
        }

        public async Task MoveAdFromRequestsToAdsAsync(int adID)
        {
            await _adRepository.MoveAdFromRequestsToAdsAsync(adID);
        }

        public async Task MoveAdFromArchiveToAdsAsync(int adID)
        {
            await _adRepository.MoveAdFromArchiveToAdsAsync(adID);
        }

        public async Task<IEnumerable<int>> GetAdIds()
        {
            return await _adRepository.GetAdIds();
        }

        public async Task UpdateImageAttachments(int adId, IEnumerable<ImageAttachment> imageAttachments)
        {
            await _adRepository.UpdateImageAttachments(adId, imageAttachments);
        }

        public async Task<DataTable<Ad>> GetAdsFromArchiveAsync(AdFilter filter, int page, int take)
        {
            var dataTable = await _adRepository.GetAdsFromArchiveAsync(filter, page, take);

            return dataTable;
        }

        public async Task<Ad> GetAdArchiveByIDAsync(int adID)
        {
            var ad = await _adRepository.GetAdArchiveByIDAsync(adID);

            return ad;
        }

        public async Task AddAdReportAsync(AdReport adReport)
        {
            await _adRepository.AddAdReportAsync(adReport);
        }

        public async Task<IEnumerable<AdReport>> GetAdReportsAsync()
        {
            return await _adRepository.GetAdReportsAsync();
        }
    }
}
