﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Square.Common;
using Square.Services.Contracts;

namespace Square.Services.Implementation
{
    /// <summary>
    /// Сервис для работы с объявлениями.
    /// </summary>
    public class AdService : IAdService
    {
        private readonly IAdRepository _adRepository;

        public AdService()
        {
            _adRepository = new AdRepository();
        }

        /// <summary>
        /// Получить список объявлений.
        /// </summary>
        public async Task<IEnumerable<Ad>> GetAdsAsync()
        {
            var adItems = await _adRepository.GetAdsAsync();

            return adItems.ToList();
        }

        /// <summary>
        /// Добавить новое объявление.
        /// </summary>
        public async Task<int> AddNewAd(Ad ad)
        {
            if (ad == null)
            {
                new ArgumentNullException("Параметр ad не должен равнятся null");
            }

            var adID = await _adRepository.AddNewAd(ad);

            return adID;
        }
    }
}
