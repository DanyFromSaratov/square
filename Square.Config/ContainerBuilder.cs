﻿using Unity;

using Square.Services;

namespace Square.Config
{
    /// <summary>
    /// Настраивает Unity, регистрируя в контейнере все используемые подсистемы и перехватчики вызовов для них.
    /// </summary>
    public class ContainerBuilder
    {
        /// <summary>
        /// Инициализация контейнера Unity.
        /// </summary>
        /// <returns>Контейнер Unity.</returns>
        public static IUnityContainer Build(IUnityContainer container = null)
        {
            container = container ?? new UnityContainer();

            container.RegisterSquareServices();

            return container;
        }
    }
}
