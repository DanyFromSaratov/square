﻿using System.Threading.Tasks;

using Quartz;

using Square.Services.Implementation.Repositories;

namespace Square.AdminService
{
    public class FreeViewsChecker : IJob
    {
        public async Task Execute(IJobExecutionContext context)
        {
            var userRepository = new UserRepository();

            await userRepository.CheckAndUpdateFreeViewsAsync();
        }
    }
}
