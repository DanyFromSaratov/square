﻿using System.Threading.Tasks;

using Quartz;

using Square.Services.Implementation.Repositories;

namespace Square.AdminService
{
    /// <summary>
    /// Job, для проверки подписок (актуальность по дате).
    /// </summary>
    public class SubscriptionsChecker : IJob
    {
        public async Task Execute(IJobExecutionContext context)
        {
            var userRepository = new UserRepository();

            await userRepository.CheckAndUpdateUserSubscriptionAsync();
        }
    }
}
