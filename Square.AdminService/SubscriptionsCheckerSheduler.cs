﻿using Quartz;
using Quartz.Impl;

namespace Square.AdminService
{
    public class SubscriptionsCheckerSheduler
    {
        public static async void Start()
        {
            var scheduler = await StdSchedulerFactory.GetDefaultScheduler();
            await scheduler.Start();

            IJobDetail job = JobBuilder
                .Create<SubscriptionsChecker>()
                .Build();

            ITrigger trigger = TriggerBuilder.Create()  // создаем триггер
                .WithIdentity("trigger1", "group1")     // идентифицируем триггер с именем и группой
                .StartNow()                            // запуск сразу после начала выполнения
                .WithSimpleSchedule(x => x            // настраиваем выполнение действия
                    .WithIntervalInMinutes(10)          // через 10 минут
                    .RepeatForever())                   // бесконечное повторение
                .Build();

            IJobDetail job2 = JobBuilder
                .Create<FreeViewsChecker>()
                .Build();

            ITrigger trigger2 = TriggerBuilder.Create()  // создаем триггер
                .WithIdentity("trigger2", "group2")     // идентифицируем триггер с именем и группой
                .StartNow()                            // запуск сразу после начала выполнения
                .WithSimpleSchedule(x => x            // настраиваем выполнение действия
                    .WithIntervalInMinutes(1)          // через 10 минут
                    .RepeatForever())                   // бесконечное повторение
                .Build();       // создаем триггер

            await scheduler.ScheduleJob(job, trigger);
            await scheduler.ScheduleJob(job2, trigger2); // начинаем выполнение работы
        }
    }
}
