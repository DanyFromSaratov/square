﻿using Square.Services.Contracts.Repositories;
using Square.Services.Contracts.Services;
using Square.Services.Implementation.Repositories;
using Square.Services.Implementation.Services;
using Ninject.Modules;

namespace Square.UI
{
    public class ContainerBuilder : NinjectModule
    {
        public override void Load()
        {
            Bind<IAdService>().To<AdService>();
            Bind<IAdRepository>().To<AdRepository>();
            Bind<ICityRepository>().To<CityRepository>();
            Bind<ICityService>().To<CityService>();
            Bind<IUserRepository>().To<UserRepository>();
            Bind<IUserService>().To<UserService>();
        }
        //public static IUnityContainer Build(IUnityContainer container = null)
        //{
        //    container = container ?? new UnityContainer();

        //    container.RegisterType<IAdService, AdService>();
        //    container.RegisterType<IAdRepository, AdRepository>();

        //    container.RegisterType<ICityRepository, CityRepository>();
        //    container.RegisterType<ICityService, CityService>();

        //    container.RegisterType<IUserRepository, UserRepository>();
        //    container.RegisterType<IUserService, UserService>();

        //    return container;
        //}
    }
}