﻿using Ninject.Modules;
using Ninject.Web.Mvc;
using Square.Services.Contracts.Repositories;
using Square.Services.Contracts.Services;
using Square.Services.Implementation.Repositories;
using Square.Services.Implementation.Services;
using System.Web.Mvc;

namespace Square.UI.Util
{
    public class NinjectRegistrations : NinjectModule
    {
        public override void Load()
        {
            Bind<IAdService>().To<AdService>();
            Bind<IAdRepository>().To<AdRepository>();
            Bind<ICityRepository>().To<CityRepository>();
            Bind<ICityService>().To<CityService>();
            Bind<IUserRepository>().To<UserRepository>();
            Bind<IUserService>().To<UserService>();
        }

        public static void RegisterComponents()
        {
            NinjectModule registrations = new NinjectRegistrations();
            var kernel = new Ninject.StandardKernel(registrations);
            DependencyResolver.SetResolver(new NinjectDependencyResolver(kernel));
        }
    }
}