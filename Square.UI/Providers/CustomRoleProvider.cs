﻿using System;
using System.Linq;
using System.Web.Security;
using Square.Services.Contracts;
using Square.Services.Contracts.Services;
using Square.Services.Implementation;
using Square.Services.Implementation.Repositories;
using Square.Services.Implementation.Services;

namespace Square.UI.Providers
{
    public class CustomRoleProvider : RoleProvider
    {
        private readonly IUserService _userService;

        public CustomRoleProvider()
        {
            _userService = new UserService(new UserRepository());
        }

        public override bool IsUserInRole(string username, string roleName)
        {
           return _userService.IsUserInRole(username, roleName);
        }

        public override string[] GetRolesForUser(string username)
        {
            return _userService
                .GetRolesForUser(username)
                .ToArray();
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string ApplicationName { get; set; }
    }
}