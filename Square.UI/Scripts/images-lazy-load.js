﻿(async () => {
    if ('loading' in HTMLImageElement.prototype) {
        const images = document.querySelectorAll("a.lazyload");
        images.forEach(img => {
            img.src = img.dataset.src;
        });
    } else {
        // Динамически импортируем библиотеку LazySizes
        const lazySizesLib = await import("lazysizes");
        // Инициализируем LazySizes (читаем data-src & class=lazyload)
        lazySizesLib.init(); // lazySizes применяется при обработке изображений, находящихся на странице.
    }
})();