﻿using System.Linq;
using Square.Common.Entities.Ad;
using Square.Common.Entities.City;
using Square.UI.Models.AdEntityModels;

namespace Square.UI.Mappers
{
    public class CityMapper
    {
        public static City MapToCity(CityModel cityModel)
        {
            if (cityModel == null)
            {
                return null;
            }

            var districts = cityModel.Districts
                .Select(MapToDistrict)
                .ToList();

            var city = new City
            {
                CityID = cityModel.CityID,
                Name = cityModel.Name,
                Districts = districts
            };

            return city;
        }

        public static District MapToDistrict(DistrictModel districtModel)
        {
            if (districtModel == null)
            {
                return null;
            }

            var district = new District
            {
                DistrictID = districtModel.DistrictID,
                Name = districtModel.Name
            };

            return district;
        }

        public static CityModel MapToCityModel(City city)
        {
            if (city == null)
            {
                return null;
            }

            var districtModels = city.Districts
                .Select(MapToDistrictModel)
                .ToList();

            var cityModel = new CityModel
            {
                CityID = city.CityID,
                Name = city.Name,
                Districts = districtModels
            };

            return cityModel;
        }

        public static DistrictModel MapToDistrictModel(District district)
        {
            if (district == null)
            {
                return null;
            }

            var districtModel = new DistrictModel
            {
                DistrictID = district.DistrictID,
                Name = district.Name
            };

            return districtModel;
        }
    }
}