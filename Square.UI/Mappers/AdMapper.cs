﻿using Square.UI.Models.AdEntityModels;
using System;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using Square.Common.Entities.Ad;
using Square.Common.Entities.Common;

namespace Square.UI.Mappers
{
    public class AdMapper
    {
        public static Ad MapToAd(AdModel adModel)
        {
            if (adModel == null)
            {
                return null;
            }

            var phone = MapToPhone(adModel.Phone);

            var locationInfo = MapToAdLocationInfo(adModel.LocationInfo);

            var comfort = MapToComfort(adModel.ComfortModel);

            var roomsCount = GetRoomsCountFromRoomsCountModel(adModel.RoomsCountModel);

            var ad = new Ad
            {
                AdID = adModel.AdID,
                Title = adModel.Title,
                Kladr = adModel.Kladr,
                Description = adModel.Description,
                Phone = phone,
                LocationInfo = locationInfo,
                Fio = adModel.Fio,
                AdditionalInfo = adModel.AdditionalInfo,
                Price = adModel.Price,
                RoomsCount = roomsCount ?? adModel.RoomsCount,
                Comfort = comfort
            };

            return ad;
        }

        private static int? GetRoomsCountFromRoomsCountModel(RoomsCountModel roomsCountModel)
        {
            if (roomsCountModel == null)
            {
                return null;
            }

            if (roomsCountModel.One)
            {
                return 1;
            }

            if (roomsCountModel.Two)
            {
                return 2;
            }

            if (roomsCountModel.Tree)
            {
                return 3;
            }

            if (roomsCountModel.FourAndMore)
            {
                return 4;
            }

            return 1;
        }

        public static AdPhone MapToPhone(PhoneModel phoneModel)
        {
            if (phoneModel == null)
            {
                return null;
            }

            var phone = new AdPhone
            {
                PhoneID = phoneModel.PhoneID,
                PhoneNumber = phoneModel.PhoneNumber,
                AdditionalPhoneNumber = phoneModel.AdditionalPhoneNumber
            };

            return phone;
        }

        public static AdModel MapToAdModel(Ad ad)
        {
            if (ad == null)
            {
                return null;
            }

            var phoneModel = MapToPhoneModel(ad.Phone);
            var locationInfoModel = MapToAdLocationInfoModel(ad.LocationInfo);
            var attahments = ad.ImageAttachments.Select(MapToImageAttachmentModel).ToList();

            //var mainIamge = MapToImageAttachmentModel(ad.MainImage);

            var comfortModel = MapToComfortModel(ad.Comfort);

            var adModel = new AdModel
            {
                AdID = ad.AdID,
                Title = ad.Title,
                Kladr = ad.Kladr,
                Description = ad.Description,
                Phone = phoneModel,
                LocationInfo = locationInfoModel,
                Fio = ad.Fio,
                AdditionalInfo = ad.AdditionalInfo,
                UpdatedDate = ad.UpdatedDate,
                ViewsCount = ad.ViewsCount,
                Price = ad.Price,
                RoomsCount = ad.RoomsCount,
                //MainImage = mainIamge,
                ImageAttachments = attahments,
                ComfortModel = comfortModel,
                FormattedPrice = ad.Price.ToString("### ###", CultureInfo.InvariantCulture),
                RoomsCountString = GetRoomsCountString(ad.RoomsCount),
                IsFavorite = ad.IsFavorite,
                IsAvito = ad.IsAvito
            };

            return adModel;
        }

        private static string GetRoomsCountString(int roomsCount)
        {
            if (roomsCount == 1)
            {
                return "1-комн.";
            }

            if (roomsCount == 2)
            {
                return "2-комн.";
            }

            if (roomsCount == 3)
            {
                return "3-комн.";
            }

            if (roomsCount > 3)
            {
                return "4-комн.";
            }

            return "1-комн. ";
        }

        public static PhoneModel MapToPhoneModel(AdPhone phone)
        {
            if (phone == null)
            {
                return null;
            }

            var phoneModel = new PhoneModel
            {
                PhoneID = phone.PhoneID,
                PhoneNumber = phone.PhoneNumber,
                AdditionalPhoneNumber = phone.AdditionalPhoneNumber
            };

            return phoneModel;
        }

        public static AdLocationInfo MapToAdLocationInfo(AdLocationInfoModel locationInfoModel)
        {
            if (locationInfoModel == null)
            {
                return null;
            }

            var locationInfo = new AdLocationInfo
            {
                CityID = locationInfoModel.CityID,
                DistrictID = locationInfoModel.DistrictID
            };

            return locationInfo;
        }

        public static AdLocationInfoModel MapToAdLocationInfoModel(AdLocationInfo locationInfo)
        {
            if (locationInfo == null)
            {
                return null;
            }

            var locationInfoModel = new AdLocationInfoModel
            {
                CityID = locationInfo.CityID,
                DistrictID = locationInfo.DistrictID
            };

            return locationInfoModel;
        }

        public static ImageAttachmentModel MapToImageAttachmentModel(ImageAttachment imageAttachment)
        {
            if (imageAttachment == null)
            {
                return null;
            }

            var imageAttachmentModel = new ImageAttachmentModel
            {
                ImageID = imageAttachment.ImageAttachmentID,
                IsMain = imageAttachment.IsMain,
                name = imageAttachment.FileName,
                newName = imageAttachment.NewFileName
            };

            return imageAttachmentModel;
        }

        public static ComfortModel MapToComfortModel(Comfort comfort)
        {
            if (comfort == null)
            {
                return null;
            }

            var comfortModel = new ComfortModel
            {
                ComfortID = comfort.ComfortID,
                AirConditioning = comfort.AirConditioning,
                Bath = comfort.Bath,
                Fridge = comfort.Fridge,
                Internet = comfort.Internet,
                KitchenFurniture = comfort.KitchenFurniture,
                RoomsFurniture = comfort.RoomsFurniture,
                Shower = comfort.Shower,
                Washer = comfort.Washer
            };

            return comfortModel;
        }

        public static Comfort MapToComfort(ComfortModel comfortModel)
        {
            if (comfortModel == null)
            {
                return null;
            }

            var comfort = new Comfort
            {
                ComfortID = comfortModel.ComfortID,
                AirConditioning = comfortModel.AirConditioning,
                Bath = comfortModel.Bath,
                Fridge = comfortModel.Fridge,
                Internet = comfortModel.Internet,
                KitchenFurniture = comfortModel.KitchenFurniture,
                RoomsFurniture = comfortModel.RoomsFurniture,
                Shower = comfortModel.Shower,
                Washer = comfortModel.Washer
            };

            return comfort;
        }
    }
}