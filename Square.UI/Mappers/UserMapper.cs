﻿using System;
using Square.Common.Entities.Portal;
using Square.UI.Models.AccountEntityModel;

namespace Square.UI.Mappers
{
    public class UserMapper
    {
        public static UserModel MapToUserModel(User user)
        {
            if (user == null)
            {
                return null;
            }

            var roleModel = MapToRoleModel(user.Role);

            string roleName = GetRoleName(roleModel.Name);

            var userModel = new UserModel
            {
                UserID = user.UserID,
                Email = user.Email,
                Password = user.Password,
                FirstName = user.FirstName,
                LastName = user.LastName,
                PhoneNumber = user.PhoneNumber,
                Role = roleModel,
                RoleName = roleName,
                RegistrationDate = user.RegistrationDate,
                MainImage = user.MainImage

            };

            return userModel;
        }

        public static User MapToUser(UserModel userModel)
        {
            if (userModel == null)
            {
                return null;
            }

            var role = MapToRole(userModel.Role);

            var user = new User
            {
                UserID = userModel.UserID,
                Email = userModel.Email,
                Password = userModel.Password,
                FirstName = userModel.FirstName,
                LastName = userModel.LastName,
                PhoneNumber = userModel.PhoneNumber,
                Role = role,
            };

            return user;
        }

        private static Role MapToRole(RoleModel roleModel)
        {
            if (roleModel == null)
            {
                return null;
            }

            var role = new Role
            {
                RoleID = roleModel.RoleID,
                Name = roleModel.Name
            };

            return role;
        }

        private static RoleModel MapToRoleModel(Role role)
        {
            if (role == null)
            {
                return null;
            }

            var roleModel = new RoleModel
            {
                RoleID = role.RoleID,
                Name = role.Name
            };

            return roleModel;
        }

        public static SubscriptionModel MapToSubscriptionModel(Subscription subscription)
        {
            if (subscription == null)
            {
                return null;
            }

            var subscriptionModel = new SubscriptionModel
            {
                SubscriptionID = subscription.SubscriptionID,
                Title = subscription.Title,
                Price = subscription.Price,
                SortOrder = subscription.SortOrder,
                Info = subscription.Info,
                StartDate = subscription.StartDate,
                EndDate = subscription.EndDate,
                IsActive = subscription.IsActive
            };

            return subscriptionModel;
        }

        public static SubscriptionModel MapToUserSubscriptionModel(Subscription subscription)
        {
            if (subscription == null)
            {
                return null;
            }

            var subscriptionModel = new SubscriptionModel
            {
                SubscriptionID = subscription.SubscriptionID,
                StartDate = subscription.StartDate,
                EndDate = subscription.EndDate
            };

            return subscriptionModel;
        }

        private static string GetRoleName(string roleName)
        {
            if (string.IsNullOrWhiteSpace(roleName) || roleName == null)
                return "Пользователь";
            if (roleName == "Admin")
                return "Администратор";
            if (roleName == "Subscriber")
                return "Пользователь (Подписчик)";
            return roleName == "User" ? "Пользователь" : "Пользователь";
        }
    }
}