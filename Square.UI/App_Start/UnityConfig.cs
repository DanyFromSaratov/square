using System.Web.Mvc;
using Ninject;
using Ninject.Web.Mvc;

namespace Square.UI
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
            var registrations = new ContainerBuilder();

            var kernel = new StandardKernel(registrations);

            DependencyResolver.SetResolver(new NinjectDependencyResolver(kernel));
        }
    }
}