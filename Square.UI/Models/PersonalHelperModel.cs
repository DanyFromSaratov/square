﻿using Square.UI.Models.AdEntityModels;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Square.UI.Models
{
    public class PersonalHelperModel
    {
        [Required(ErrorMessage = "Обязательно для заполнения")]
        [Range(minimum: 1, maximum: 1000000000, ErrorMessage = "Минимальное значение должно быть больше 0")]
        public int PriceFrom { get; set; }

        [Required(ErrorMessage = "Обязательно для заполнения")]
        [Range(minimum: 1, maximum: 1000000000, ErrorMessage = "Минимальное значение должно быть больше 0")]
        public int PriceTo { get; set; }

        public int RoomsCount { get; set; }

        public IEnumerable<CityModel> Cities { get; set; }


        /// <summary>
        /// Информация о месте нахождения объявления.
        /// </summary>
        public AdLocationInfoModel LocationInfo { get; set; }


        public RoomsCountModel RoomsCountModel { get; set; }
    }
}