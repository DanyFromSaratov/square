﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Square.UI.Models.AccountEntityModel
{
    public class UserModel
    {
        private IEnumerable<ErrorTypeEnum> _errors;

        public int UserID { get; set; }

        /// <summary>
        /// Фамилия.
        /// </summary>
       
        [Required(ErrorMessage = "Обязательно для заполнения"),
         MinLength(2, ErrorMessage = "Минимальное количество символов должно быть не меньше 2х")]

        public string LastName { get; set; }

        /// <summary>
        /// Имя.
        /// </summary>
        [Required(ErrorMessage = "Обязательно для заполнения"),
        MinLength(2, ErrorMessage = "Минимальное количество символов должно быть не меньше 2х")]
        public string FirstName { get; set; }

        /// <summary>
        /// Email.
        /// </summary>
        [EmailAddress(ErrorMessage = "Введите корректный Email")]
        public string Email { get; set; }

        /// <summary>
        /// Пароль.
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Номер телефона.
        /// </summary>
        //[Required(ErrorMessage = "Обязательно для заполнения")]
        public string PhoneNumber { get; set; }

        public RoleModel Role { get; set; }

        public string RoleName { get; set; }

        public DateTime RegistrationDate { get; set; }

        public IEnumerable<SubscriptionModel> Subscriptions { get; set; }


        public IEnumerable<ErrorTypeEnum> Errors
        {
            get => _errors ?? Enumerable.Empty<ErrorTypeEnum>();
            set => _errors = value;
        }

        public IEnumerable<string> Files { get; set; }

        public string MainImage { get; set; }
    }
}