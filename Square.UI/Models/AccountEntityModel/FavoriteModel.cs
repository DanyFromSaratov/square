﻿using Square.UI.Models.AdEntityModels;

namespace Square.UI.Models.AccountEntityModel
{
    public class FavoriteModel
    {
        public int FavoriteID { get; set; }

        public AdModel Ad { get; set; }

        public int UserID { get; set; }

        public int AdID { get; set; }
    }
}