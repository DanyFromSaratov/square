﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Security;

namespace Square.UI.Models.AccountEntityModel
{
    /// <summary>
    /// MVC-модель логина.
    /// </summary>
    public class LoginModel
    {
        private IEnumerable<ErrorTypeEnum> _errors;

        /// <summary>
        /// Логин.
        /// </summary>
        [Required(ErrorMessage = "Обязательно для заполнения"),
         MinLength(6, ErrorMessage = "Минимальное количество символов должно быть не меньше 6-и")]
        public string PhoneNumberLogin { get; set; }

        /// <summary>
        /// Пароль.
        /// </summary>
        [Required(ErrorMessage = "Обязательно для заполнения")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        // -------------------------

        public IEnumerable<ErrorTypeEnum> Errors
        {
            get => _errors ?? Enumerable.Empty<ErrorTypeEnum>();
            set => _errors = value;
        }
    }
}