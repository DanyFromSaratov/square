﻿namespace Square.UI.Models.AccountEntityModel
{
    public enum ErrorTypeEnum
    {
        PhoneNumberExisted = 1,

        LoginNotFound = 2,

        AdRequestExisted = 3,

        RoomsCountNull = 4,

    }
}