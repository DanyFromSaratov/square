﻿namespace Square.UI.Models.AccountEntityModel
{
    public class RoleModel
    {
        public int RoleID { get; set; }

        public string Name { get; set; }
    }
}