﻿using System;

namespace Square.UI.Models.AccountEntityModel
{
    public class SubscriptionModel
    {
        public int SubscriptionID { get; set; }

        public int UserID { get; set; }

        public string Title { get; set; }

        public int Price { get; set; }

        public int SortOrder { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string Info { get; set; }

        public Tuple<int?, DateTime> ConnectedSubscription { get; set; }

        public bool IsActive { get; set; }
    }
}