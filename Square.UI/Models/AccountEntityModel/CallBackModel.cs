﻿namespace Square.UI.Models.AccountEntityModel
{
    public class CallBackModel
    {
        public string PhoneNumber { get; set; }

        public string Name { get; set; }
    }
}