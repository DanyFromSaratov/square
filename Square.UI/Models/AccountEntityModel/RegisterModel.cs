﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Security;

namespace Square.UI.Models.AccountEntityModel
{
    /// <summary>
    /// MVC-модель регистрации пользователя.
    /// </summary>
    public class RegisterModel
    {
        private IEnumerable<ErrorTypeEnum> _errors;

        /// <summary>
        /// Логин - Номер телефона.
        /// </summary>
        [Required(ErrorMessage = "Обязательно для заполнения"), MinLength(6, ErrorMessage = "Минимальное количество символов должно быть не меньше 6-и")]
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Фамилия.
        /// </summary>
        [Required(ErrorMessage = "Обязательно для заполнения"), MinLength(2, ErrorMessage = "Минимальное количество символов должно быть не меньше 2х")]
        public string LastName { get; set; }

        /// <summary>
        /// Имя.
        /// </summary>
        [Required(ErrorMessage = "Обязательно для заполнения"), MinLength(2, ErrorMessage = "Минимальное количество символов должно быть не меньше 2х")]
        public string FirstName { get; set; }

        /// <summary>
        /// Email.
        /// </summary>
        [EmailAddress(ErrorMessage = "Введите корректный Email")]
        public string Email { get; set; }

        /// <summary>
        /// Пароль.
        /// </summary>
        [Required(ErrorMessage = "Обязательно для заполнения")]
        [StringLength(100, ErrorMessage = "Минимальное количество символов должно быть не меньше {2}", MinimumLength = 6)]
        [MembershipPassword(
            MinRequiredNonAlphanumericCharacters = 1,
            MinNonAlphanumericCharactersError = "Пароль должен содержать хотя бы один символ из списка (!, @, #, и тп).",
            ErrorMessage = "Минимальное количество символов должно быть не меньше 6 и пароль должен содержать хотя бы один символ из списка(!, @, #, etc)."
        )]
        [DataType(DataType.Password)]

        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Пароли не совпадают")]
        public string ConfirmPassword { get; set; }

        public string GoogleRecap { get; set; }

        // -------------------------

        public IEnumerable<ErrorTypeEnum> Errors
        {
            get => _errors ?? Enumerable.Empty<ErrorTypeEnum>();
            set => _errors = value;
        }

    }
}