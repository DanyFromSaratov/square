﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Square.UI.Models.AdEntityModels
{
    public class AddReportModel
    {
        public bool AdFromRealtor { get; set; }

        public bool AdLeased { get; set; }

        public bool AdIncorrectData { get; set; }

        public bool IncorrectPhoneNumber { get; set; }

        public int AdId { get; set; }

        public int AdReportId { get; set; }

    }
}