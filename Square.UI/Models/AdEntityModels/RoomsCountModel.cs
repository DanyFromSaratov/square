﻿namespace Square.UI.Models.AdEntityModels
{
    public class RoomsCountModel
    {
        public bool One { get; set; }

        public bool Two { get; set; }

        public bool Tree { get; set; }

        public bool FourAndMore { get; set; }
    }
}