﻿using Square.UI.Models.AccountEntityModel;

namespace Square.UI.Models.AdEntityModels
{
    public class SubscriptionModel
    {
        public Square.UI.Models.AccountEntityModel.SubscriptionModel SubscriptionInfo { get; set; }

        public int TotalPrice { get; set; }

        public UserModel UserInfo { get; set; }

        public int OrderNumber { get; set; }
    }
}