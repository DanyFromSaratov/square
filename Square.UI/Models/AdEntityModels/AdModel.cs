﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Square.UI.Models.AccountEntityModel;

namespace Square.UI.Models.AdEntityModels
{
    /// <summary>
    /// MVC-модель объявления.
    /// </summary>
    public class AdModel
    {
        private IEnumerable<ImageAttachmentModel> _imageAttachments;
        private IEnumerable<ErrorTypeEnum> _errors;

        /// <summary>
        /// Идентификатор.
        /// </summary>
        public int AdID { get; set; }

        /// <summary>
        /// Наименование.
        /// </summary>
        [Required(ErrorMessage = "Обязательно для заполнения")]
        [StringLength(200, ErrorMessage = "Минимальное количество символов должно быть не меньше {2}", MinimumLength = 2)]
        public string Title { get; set; }

        public string Kladr { get; set; }

        /// <summary>
        /// Описание.
        /// </summary>
        [Required(ErrorMessage = "Обязательно для заполнения")]
        [StringLength(3000, ErrorMessage = "Минимальное количество символов должно быть не меньше {2}", MinimumLength = 2)]
        public string Description { get; set; }

        /// <summary>
        /// ФИО автора объявления.
        /// </summary>
        [Required(ErrorMessage = "Обязательно для заполнения")]
        [StringLength(300, ErrorMessage = "Минимальное количество символов должно быть не меньше {2}", MinimumLength = 2)]
        public string Fio { get; set; }

        /// <summary>
        /// Дата обновления объявления.
        /// </summary>
        public DateTime? UpdatedDate { get; set; }

        /// <summary>
        /// Дополнительная информация.
        /// </summary>
        public string AdditionalInfo { get; set; }

        /// <summary>
        /// Контактный телефон.
        /// </summary>
        [Required(ErrorMessage = "Обязательно для заполнения")]
        public PhoneModel Phone { get; set; }

        /// <summary>
        /// Информация о месте нахождения объявления.
        /// </summary>
        public AdLocationInfoModel LocationInfo { get; set; }

        /// <summary>
        /// Приложенные изображения.
        /// </summary>
        public IEnumerable<ImageAttachmentModel> ImageAttachments
        {
            get => _imageAttachments ?? Enumerable.Empty<ImageAttachmentModel>();
            set => _imageAttachments = value;
        }

        /// <summary>
        /// Количество просмотров.
        /// </summary>
        public int ViewsCount { get; set; }

        [Required(ErrorMessage = "Обязательно для заполнения")]
        [Range(minimum: 1, maximum: 1000000000, ErrorMessage = "Минимальное значение должно быть больше 0")]
        public int Price { get; set; }

       public int RoomsCount { get; set; }

        public ComfortModel ComfortModel { get; set; }

        //-----------------------------------------------
        public IEnumerable<CityModel> Cities { get; set; }

        public IEnumerable<string> Files { get; set; }

        public ImageAttachmentModel MainImage { get; set; }

        public string FormattedPrice { get; set; }

        /// <summary>
        /// Топ-6 похожих объявлений.
        /// </summary>
        public IEnumerable<AdModel> SimilarAds { get; set; }


        public Pager Pager { get; set; }

        /// <summary>
        /// Кол-во комнат в формате (1-Комн) и тп.
        /// </summary>
        public string RoomsCountString { get; set; }

        public List<string> RoomsCountSelect { get; set; }

        public bool IsFavorite { get; set; }

        public RoomsCountModel RoomsCountModel { get; set; }


        public IEnumerable<ErrorTypeEnum> Errors
        {
            get => _errors ?? Enumerable.Empty<ErrorTypeEnum>();
            set => _errors = value;
        }

        public bool IsAvito { get; set; }

    }
}