﻿namespace Square.UI.Models.AdEntityModels
{
    /// <summary>
    /// MVC-модель информации о местонахождении объявления.
    /// </summary>
    public class AdLocationInfoModel
    {
        /// <summary>
        /// Идентификатор города.
        /// </summary>
        public int CityID { get; set; }

        /// <summary>
        /// Наименование города.
        /// </summary>
        public string CityName { get; set; }

        /// <summary>
        /// Идентификатор района текущего города.
        /// </summary>
        public int DistrictID { get; set; }

        /// <summary>
        /// Наименование района.
        /// </summary>
        public string DistrictName { get; set; }
    }
}