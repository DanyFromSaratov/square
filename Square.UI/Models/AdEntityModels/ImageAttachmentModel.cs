﻿namespace Square.UI.Models.AdEntityModels
{
    /// <summary>
    /// Api-модель приложенного файла-изображения.
    /// </summary>
    public class ImageAttachmentModel
    {
        public int ImageID { get; set; }

        public byte[] data { get; set; }

        public string type { get; set; }

        public int size { get; set; }

        public string name { get; set; }

        public string Src { get; set; }

        public string newName { get; set; }

        public bool IsMain { get; set; }
    }
}