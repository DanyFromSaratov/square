﻿namespace Square.UI.Models.AdEntityModels
{
    public class ComfortModel
    {
        public int ComfortID { get; set; }

        public bool AirConditioning { get; set; }

        public bool Internet { get; set; }

        public bool Bath { get; set; }

        public bool Shower { get; set; }

        public bool KitchenFurniture { get; set; }

        public bool RoomsFurniture { get; set; }

        public bool Fridge { get; set; }

        public bool Washer { get; set; }

        public bool HasAny()
        {
            return AirConditioning
                || Internet
                || Bath
                || Shower
                || KitchenFurniture
                || RoomsFurniture
                || Fridge
                || Washer;
        }
    }
}