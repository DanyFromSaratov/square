﻿namespace Square.UI.Models.AdEntityModels
{
    public class DistrictModel
    {
        public int DistrictID { get; set; }

        public string Name { get; set; }
    }
}