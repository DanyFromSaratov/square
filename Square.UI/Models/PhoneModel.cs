﻿namespace Square.UI.Models.AdEntityModels
{
    /// <summary>
    /// Api-модель телефона.
    /// </summary>
    public class PhoneModel
    {
        /// <summary>
        /// Идентификатор.
        /// </summary>
        public int PhoneID { get; set; }

        /// <summary>
        /// Номер телефона.
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Добавочный номер телефона.
        /// </summary>
        public string AdditionalePhoneNumber { get; set; }
    }
}