﻿using System;
using System.Collections.Generic;
using Square.Common.Entities.Common;
using Square.UI.Models.AdEntityModels;

namespace Square.UI.Models
{
    public class FilterModel
    {
        private static IEnumerable<Tuple<int, string>> orderedByList = new List<Tuple<int, string>>
        {
            new Tuple<int, string>(0, "По Умолчанию"),
            new Tuple<int, string>(1, "Дороже"),
            new Tuple<int, string>(2, "Дешевле"),
            new Tuple<int, string>(3, "По Дате"),
        };

        public int CityID { get; set; }

        public int DistrictID { get; set; }

        public int PriceFrom { get; set; }

        public int PriceTo { get; set; }

        public int RoomsCount { get; set; }

        public string Search { get; set; }

        public IEnumerable<CityModel> Cities { get; set; }

        public int OrderedBySelected { get; set; }

        public string PhoneNumber { get; set; }

        public IEnumerable<Tuple<int, string>> OrderedBy => orderedByList;

        public List<string> RoomsCountSelect { get; set; }
    }
}