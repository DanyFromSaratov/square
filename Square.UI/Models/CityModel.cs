﻿using System.Collections.Generic;

namespace Square.UI.Models.AdEntityModels
{
    public class CityModel
    {
        public int CityModelID { get; set; }

        public string Name { get; set; }

        public IEnumerable<DistrictModel> Districts { get; set; }
    }
}