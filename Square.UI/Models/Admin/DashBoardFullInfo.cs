﻿using System.Collections.Generic;
using Square.UI.Models.AccountEntityModel;

namespace Square.UI.Models.Admin
{
    public class DashBoardFullInfo
    {
        public IEnumerable<UserModel> Users { get; set; }

        public int CurrentSubscriptionsCount { get; set; }
    }
}