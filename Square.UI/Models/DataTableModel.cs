﻿using System.Collections.Generic;

using Square.Common;
using Square.Common.Entities.Common;

namespace Square.UI.Models
{
    public class DataTableModel<T>
    {
        public IEnumerable<T> Items { get; set; }

        public DataTablePagination Pagination { get; set; }

        public PageInfoModel PageInfo { get; set; }

        public FilterModel Filter { get; set; }

        public int Page { get; set; } = 1;

        public Pager Pager { get; set; }

    }
}