﻿using System;
using System.Collections.Generic;

namespace Square.UI.Models
{
    public class NewsModel
    {
        public int NewsID { get; set; }

        public string NewsTitle { get; set; }

        public string Content { get; set; }

        public DateTime UpdatedDate { get; set; }

        public IEnumerable<string> Files { get; set; }

        public string MainImgName { get; set; }
    }
}