﻿namespace Square.UI.Models.AdEntityModels
{
    /// <summary>
    /// Api-модель приложенного файла-изображения.
    /// </summary>
    public class ImageAttachmentModel
    {
        public int Id { get; set; }

        public byte[] Content { get; set; }

        public string ContentType { get; set; }

        public string FileName { get; set; }
    }
}