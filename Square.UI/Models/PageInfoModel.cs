﻿using System;

namespace Square.UI.Models
{
    public class PageInfoModel
    {
        /// <summary>
        /// Номер текущей страницы.
        /// </summary>
        public int PageNumber { get; set; }

        /// <summary>
        /// Количество объктов на странице.
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// Всего объектов.
        /// </summary>
        public int TotalItems { get; set; }

        /// <summary>
        /// Всего страниц.
        /// </summary>
        public int TotalPages
        {
            get
            {
                return (int)Math.Ceiling((decimal)TotalItems / PageSize);
            }
        }
    }
}