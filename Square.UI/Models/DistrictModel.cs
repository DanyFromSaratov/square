﻿namespace Square.UI.Models.AdEntityModels
{
    public class DistrictModel
    {
        public int DistrictModelID { get; set; }

        public string Name { get; set; }
    }
}