﻿using System.Collections.Generic;

namespace Square.UI.Models.AdEntityModels
{
    /// <summary>
    /// API-модель контактов (людей, с которыми можно связать и получить ниформацию).
    /// </summary>
    public class ContactModel
    {
        /// <summary>
        /// Идентификатор.
        /// </summary>
        public int ContactID { get; set; }

        /// <summary>
        /// Фамилия.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Имя.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Отчество.
        /// </summary>
        public string MiddleName { get; set; }

        /// <summary>
        /// Список контактных телефонов.
        /// </summary>
        public IEnumerable<PhoneModel> ContactPhones { get; set; }

        /// <summary>
        /// Должность.
        /// </summary>
        public string Rank { get; set; }

        /// <summary>
        /// Email.
        /// </summary>
        public string Email { get; set; }

        public string Fio
        {
            get { return string.Format($"{LastName} {FirstName} {MiddleName}").Trim(); }
        }
    }
}