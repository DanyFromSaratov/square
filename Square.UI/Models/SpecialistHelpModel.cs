﻿using System.ComponentModel.DataAnnotations;

namespace Square.UI.Models
{
    public class SpecialistHelpModel
    {
        public int SepcialistHelperID { get; set; }

        [Required(ErrorMessage = "Обязательно для заполнения"),
         MinLength(2, ErrorMessage = "Минимальное количество символов должно быть не меньше 2-х")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Обязательно для заполнения"),
         MinLength(6, ErrorMessage = "Минимальное количество символов должно быть не меньше 6-и")]
        public string UserPhoneNumber { get; set; }
    }
}