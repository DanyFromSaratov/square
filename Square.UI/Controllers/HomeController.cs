﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Square.Common;
using Square.Common.Entities.Ad;
using Square.Common.Entities.Common;
using Square.Common.Entities.Portal;
using Square.Services.Contracts;
using Square.Services.Contracts.Services;
using Square.UI.Mappers;
using Square.UI.Models;
using Square.UI.Models.AdEntityModels;

namespace Square.UI.Controllers
{
    [Route("home")]
    public class HomeController : Controller
    {
        private readonly IUserService _userService;
        private readonly ICityService _cityService;

        public HomeController(IUserService userService, ICityService cityService)
        {
            _userService = userService;
            _cityService = cityService;
        }

        /// <summary>
        /// Возвращает главную страницу.
        /// </summary>
        [HttpGet]
        [Route("")]
        public async Task<ActionResult> Index()
        {
            return RedirectToAction("Index", "Ad");

            if (User.Identity.IsAuthenticated)
            {
                return View();
            }

            return View("Index");
        }

     

        /// <summary>
        /// Возвращает страницу "Информация".
        /// </summary>
        [Route("about")]
        public ActionResult About()
        {
            return View();
        }


        [HttpGet]
        public async Task<ActionResult> News()
        {
            var news = await _userService.GetNewsAsync();
            var newsModels = news.Select(newsInfo => new NewsModel
            {
                NewsID = newsInfo.NewsID,
                NewsTitle = newsInfo.NewsTitle,
                Content = newsInfo.Content,
                MainImgName = newsInfo.MainImgName,
                UpdatedDate = newsInfo.UpdatedDate
            });


            return View("News", newsModels);
        }


        [HttpGet]
        public async Task<ActionResult> OrderCall(string phoneNumber)
        {
            if (!string.IsNullOrWhiteSpace(phoneNumber))
            {
                await _userService.OrderCallAsync(phoneNumber);
            }

            return Redirect(ControllerContext.HttpContext.Request.UrlReferrer.ToString());
        }

        [HttpGet]
        public async Task<ActionResult> GetOrderedCalls()
        {
            var orderedCalls = await _userService.GetOrderedCallsAsync();

            return View(orderedCalls);
        }

        [HttpGet]
        public async Task<ActionResult> RemoveCall(string phoneNumber)
        {
            if (!string.IsNullOrWhiteSpace(phoneNumber))
            {
                await _userService.DeleteOrderedCallAsync(phoneNumber);
            }

            var orderedCalls = await _userService.GetOrderedCallsAsync();

            return View("GetOrderedCalls", orderedCalls);
        }

        [HttpGet]
        public async Task<ActionResult> GetUsers()
        {
            var users = await _userService.GetUsersAsync();

            var userModels = users
                .Select(user => UserMapper.MapToUserModel(user))
                .ToList();

            return View("GetUsers", userModels);
        }

        [HttpGet]
        public async Task<ActionResult> RemoveUser(int userID)
        {
            await _userService.RemoveUserAsync(userID);

            return Redirect(ControllerContext.HttpContext.Request.UrlReferrer.ToString());
        }

        [HttpGet]
        public async Task<ActionResult> ChangeRoleUser(int userID, SquareRoleEnum role)
        {
            await _userService.ChangeUserRole(userID, role);

            return Redirect(ControllerContext.HttpContext.Request.UrlReferrer.ToString());
        }

        [HttpGet]
        [Route("policy")]
        public ViewResult Policy()
        {
            var policy = _userService.GetPolicy();

            return View("Policy", (object)policy);
        }

        [HttpGet]
        [Route("paymentrules")]
        public ViewResult PaymentRules()
        {
            return View("PaymentRules");
        }

        [HttpGet]
        public ActionResult DownloadFile()
        {
            string path = Server.MapPath("~/Documents/dogovor.pdf");
            byte[] mas = System.IO.File.ReadAllBytes(path);
            string file_type = "application/pdf";
            string file_name = "dogovor.pdf";
            return File(mas, file_type, file_name);
        }


        [HttpGet]
        public async Task<ViewResult> Comments()
        {
            var comments = await _userService.GetCommentsAsync();

            var commentModels = comments
                .Select(comment => new CommentModel
                {
                    CommentID = comment.CommentID,
                    UserID = comment.UserID,
                    Text = comment.Text,
                    Fio = comment.Fio,
                    UserImage = comment.UserImage
                    
                })
                .ToList();

            return View(commentModels);
        }

        [HttpGet]
        public ActionResult More()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult> AddComment(CommentModel commentModel)
        {
            if (commentModel == null || string.IsNullOrWhiteSpace(commentModel.Text))
            {
                return RedirectToAction("Comments");
            }

            var userModel = await _userService.GetUserByPhoneNumberAsync(User.Identity.Name);
            if (userModel == null)
            {
                return RedirectToAction("Comments");
            }

            var userFio = userModel.LastName + " " + userModel.FirstName;

            var comment = new Comment
            {
                Text = commentModel.Text,
                Fio = userFio,
                UserID = userModel.UserID
            };

            await _userService.AddCommentAsync(comment);

            return RedirectToAction("Comments");
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> AddNews()
        {
            return View("AddNews");
        }

        [HttpPost]
        [Authorize(Roles = "Admin"), ValidateInput(false)]
        public async Task<ActionResult> AddNews(NewsModel newsModel)
        {
            var news = new News
            {
                NewsTitle = newsModel.NewsTitle,
                Content = newsModel.Content
            };


            if (newsModel.Files != null && newsModel.Files.Any())
            {
                var mainImg = GetImageAttachmentForBlog(newsModel);
                if (mainImg != null)
                {
                    news.MainImgName = mainImg.FileName;
                }
            }

            await _userService.AddNewsAsync(news);

            return RedirectToAction("News");
        }

        [HttpGet]
        public async Task<ActionResult> Faq()
        {
            return View("Faq");
        }

        [HttpGet]
        public async Task<ActionResult> GetNewsByID(int newsID)
        {
            var news = await _userService.GetNewsByIDAsync(newsID);

            var newsModel = new NewsModel
            {
                NewsID = news.NewsID,
                NewsTitle = news.NewsTitle,
                Content = news.Content,
                MainImgName = news.MainImgName,
                UpdatedDate = news.UpdatedDate
            };

            return View("NewsInfo", newsModel);
        }

        [HttpGet]
        public async Task<ActionResult> Specialist()
        {
            return View("Specialist");
        }


        [HttpGet]
        public async Task<ActionResult> FreeAdd()
        {
            return View("FreeAdd");
        }

        [HttpGet]
        public async Task<ActionResult> SecurityPayment()
        {
            return View("SecurityPayment");
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public async Task<ActionResult> GetSepcialistHelpRequests()
        {
            var requests = await _userService.GetSpecialistHelpRequestsAsync();

            var requestModels = requests
                .Select(request => new SpecialistHelpModel
                {
                    SepcialistHelperID = request.SepcialistHelperID,
                    UserName = request.UserName,
                    UserPhoneNumber = request.UserPhoneNumber
                })
                .ToList();

            return View(requestModels);
        }

        [HttpGet]
        public async Task<ActionResult> GetSpecialistHelper()
        {
            return View(new SpecialistHelpModel());
        }

        [HttpGet]
        public async Task<ActionResult> AddMyComment()
        {
            return View();
        }


        [HttpPost]
        public async Task<ActionResult> GetSpecialistHelper(SpecialistHelpModel requestModel)
        {
            if (!ModelState.IsValid)
            {
                return View(requestModel);
            }

            var request = new SpecialistHelper
            {
                UserName = requestModel.UserName,
                UserPhoneNumber = requestModel.UserPhoneNumber
            };

            await _userService.AddSpecialistHelpRequestAsync(request);

            return View("SpecialistHelperNotification");
        }

        [HttpGet]
        public async Task<ActionResult> PersonalHelper()
        {
            var cities = await _cityService.GetCitiesAsync();

            var cityModels = cities
                .Select(CityMapper.MapToCityModel)
                .ToList();

            var personalHelperModel = new PersonalHelperModel
            {
                Cities = cityModels,
                LocationInfo = new AdLocationInfoModel(),
                RoomsCountModel = new RoomsCountModel
                {
                    One = true
                }
            };

            return View(personalHelperModel);
        }

        private ImageAttachment GetImageAttachmentForBlog(NewsModel newsModel)
        {
            if (newsModel == null || newsModel.Files == null)
            {
                return null;
            }


            foreach (var fileJson in newsModel.Files)
            {
                if (string.IsNullOrWhiteSpace(fileJson))
                {
                    continue;
                }

                var file = JsonConvert.DeserializeObject<ImageAttachmentModel>(fileJson);

                if (file != null || file.size != 0)
                {
                    var folderPath = Server.MapPath("~/BlogImgStore/");

                    var imagePath = folderPath + file.name;

                    using (var ms = new MemoryStream(file.data))
                    {
                        using (var img = Image.FromStream(ms))
                        {
                            img.Save(imagePath);
                        }
                    }

                    var imageAttachment = new ImageAttachment
                    {
                        FileName = file.name
                    };

                    return imageAttachment;
                }
            }

            return null;
        }
    }
}