﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

using Square.Common.Entities.Portal;
using Square.Services.Contracts.Services;
using Square.UI.Mappers;
using Square.UI.Models.AccountEntityModel;

namespace Square.UI.Controllers
{
    /// <summary>
    /// Контроллер, для работы с подписками.
    /// </summary>
    [Route("subscriptions")]
    public class PriceController : Controller
    {
        private readonly IUserService _userService;

        public PriceController(IUserService userService)
        {
            _userService = userService;
        }

        /// <summary>
        /// Возвращает страницу с подписками.
        /// </summary>
        [HttpGet]
        public async Task<ActionResult> Index()
        {
            var subscriptions = _userService.GetSubscriptionsAsync();

            var subscriptionModels = subscriptions
                .Select(UserMapper.MapToSubscriptionModel)
                .OrderBy(subscriptionModel => subscriptionModel.SortOrder)
                .ToList();

            if (User.Identity.IsAuthenticated)
            {
                var user = await _userService.GetUserByPhoneNumberAsync(User.Identity.Name);

                var subscriptionInfo = await _userService.GetUserCurrentSubscriptionAsync(user.UserID);
                if (subscriptionInfo != null)
                {
                    var subscriptionInfoModel = subscriptionModels.FirstOrDefault(subscription =>
                        subscription.SubscriptionID == subscriptionInfo.SubscriptionID);
                    if (subscriptionInfoModel != null)
                    {
                        subscriptionInfoModel.ConnectedSubscription =
                            new Tuple<int?, DateTime>(user.UserID, subscriptionInfo.EndDate.GetValueOrDefault());

                        return View("ConnectedSubscription", subscriptionModels);
                    }
                }
            }


            return View(subscriptionModels);
        }

        /// <summary>
        /// Возвращает страницу, для оплаты подписки.
        /// </summary>
        [HttpGet]
        public async Task<ActionResult> Pay(int subscribtionID = 1)
        {
            var subscriptionInfo = _userService
                .GetSubscriptionsAsync()
                .FirstOrDefault(subscription => subscription.SubscriptionID == subscribtionID);

            var userModel = new UserModel();
            var user = new User();

            if (User.Identity.IsAuthenticated)
            {
                user = await _userService.GetUserByPhoneNumberAsync(User.Identity.Name);
                userModel = UserMapper.MapToUserModel(user);
            }
        

            var subscriptionInfoModel = UserMapper.MapToSubscriptionModel(subscriptionInfo);

            var subscriptionModel = new Models.AdEntityModels.SubscriptionModel
            {
                UserInfo = userModel,
                SubscriptionInfo = subscriptionInfoModel,
                TotalPrice = subscriptionInfo.Price,
                OrderNumber = user != null ? user.OrderNumber : 1
            };

            return View(subscriptionModel);
        }

        [HttpGet]
        public async Task<ActionResult> PayConnect(int subscriptionID, int userID)
        {
            var subscription = new Subscription
            {
                Type = MapToSubscriptionEnum(subscriptionID),
                UserID = userID
            };

            await _userService.ConnectSubscriptionToUserAsync(subscription);

            return View("SubscriptionConnectedNotify");
        }

        [HttpGet]
        public async Task<ActionResult> PayConnectError()
        {
            return View("SubscriptionConnectedErrorNotify");
        }

        private static SubscriptionEnum MapToSubscriptionEnum(int subscriptionID)
        {
            switch (subscriptionID)
            {
                case 1: return SubscriptionEnum.Base;
                case 2: return SubscriptionEnum.Optimal;
                case 3: return SubscriptionEnum.Maximal;

                default: return 0;
            }
        }
    }
}