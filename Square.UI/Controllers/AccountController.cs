﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Security;
using Newtonsoft.Json;
using Square.Common.Entities.Ad;
using Square.Common.Entities.Common;
using Square.Common.Entities.Portal;
using Square.Services.Contracts;
using Square.Services.Contracts.Services;
using Square.UI.Mappers;
using Square.UI.Models.AccountEntityModel;
using Square.UI.Models.AdEntityModels;

namespace Square.UI.Controllers
{
    /// <summary>
    /// Контроллер, для работы с пользователями приложения.
    /// </summary>
    public class AccountController : Controller
    {
        private readonly IUserService _userService;

        public AccountController(IUserService userService)
        {
            _userService = userService;
        }

        /// <summary>
        /// Возвращает страницу авторизации.
        /// </summary>
        [HttpGet]
        [Route("login")]
        public ActionResult Login()
        {
            return View(new LoginModel());
        }

        [HttpGet]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();

            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// Возвращает страницу регистрации.
        /// </summary>
        [HttpGet]
        [Route("signup")]
        public ActionResult SignUp()
        {
            return View(new RegisterModel());
        }

        /// <summary>
        /// Авторизирует пользователя.
        /// Возвращает результат авторизации.
        /// </summary>
        [HttpPost]
        [Route("login")]
        public async Task<ActionResult> Login(LoginModel loginModel)
        {
            if (!ModelState.IsValid)
            {
                return View(loginModel);
            }

            var user = await _userService.GetUserByPhoneNumberAndPasswordAsync(
                loginModel.PhoneNumberLogin,
                loginModel.Password);

            if (user == null)
            {
                var errors = loginModel
                    .Errors
                    .ToList();

                errors.Add(ErrorTypeEnum.LoginNotFound);

                loginModel.Errors = errors;
            }
            else if (ModelState.IsValid)
            {
                FormsAuthentication.SetAuthCookie(loginModel.PhoneNumberLogin, true);

                return RedirectToAction("Index", "Ad");
            }

            return View(loginModel);
        }

        /// <summary>
        /// Регистрирует пользователя.
        /// </summary>
        [HttpPost]
        [Route("signup")]
        public async Task<ActionResult> SignUp(RegisterModel registerModel)
        {
            if (!ModelState.IsValid)
            {
                return View(registerModel);
            }

            var isExisted = await _userService.IsUserExistedByPhoneNumberAsync(registerModel.PhoneNumber);
            if (isExisted)
            {
                var errors = registerModel
                    .Errors
                    .ToList();

                errors.Add(ErrorTypeEnum.PhoneNumberExisted);

                registerModel.Errors = errors;
            }
            else if (ModelState.IsValid)
            {
                var userModel = FillUserModel(registerModel);

                var user = UserMapper.MapToUser(userModel);

                await _userService.RegisterUserAsync(user);

                FormsAuthentication.SetAuthCookie(userModel.PhoneNumber, true);

                return View("Greeting");
            }

            return View(registerModel);
        }

        [Authorize]
        [HttpGet]
        public async Task<ActionResult> Profile(string phoneNumber)
        {
            var user = await _userService.GetUserByPhoneNumberAsync(phoneNumber);

            var subscriptions =  _userService.GetSubscriptionsByUserIdAsync(user.UserID);

            var subscriptionModels = subscriptions.Select(UserMapper.MapToSubscriptionModel).ToList();

            UserModel userModel = UserMapper.MapToUserModel(user);

            if (userModel == null)
            {
                throw new Exception(string.Format("польователь с номеро телефона {0} не найден", (object) phoneNumber));
            }

            userModel.Subscriptions = subscriptionModels;

            return View(userModel);
        }

        [Authorize]
        [HttpGet]
        public async Task<ActionResult> UpdatedProfile(UserModel userInfoModel)
        {
            var subscriptions = _userService.GetSubscriptionsByUserIdAsync(userInfoModel.UserID);

            var subscriptionModels = subscriptions.Select(UserMapper.MapToSubscriptionModel).ToList();

            userInfoModel.Subscriptions = subscriptionModels;

            return View("Profile", userInfoModel);
        }

        [HttpGet]
        public async Task<ActionResult> NotFound()
        {
            return View("NotFound");
        }

        [Authorize]
        [HttpGet]
        public async Task<ActionResult> UpdateUserInfo(int userId)
        {
            var user = await _userService.GetUserByIDAsync(userId);

            var userModel = UserMapper.MapToUserModel(user);

            return View(userModel);
        }

        [HttpGet]
        public async Task<ActionResult> GetFavorites()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return View(Enumerable.Empty<FavoriteModel>());
            }

            var user = await _userService.GetUserByPhoneNumberAsync(User.Identity.Name);
            var favorites = await _userService.GetFavoritesAsync(user.UserID);

            var favoriteModels = favorites
                .Select(favorite => new FavoriteModel
            {
                FavoriteID = favorite.FavoriteID,
                AdID = favorite.AdID,
                UserID = favorite.UserID,
                Ad = favorite.Ad == null
                    ? null
                    : new AdModel
                    {
                        AdID = favorite.AdID,
                        Title = favorite.Ad.Title,
                        Price = favorite.Ad.Price,
                        RoomsCount = favorite.Ad.RoomsCount
                    }
                })
                .ToList();

            return View(favoriteModels);
        }

        [Authorize]
        [HttpPost]
        public async Task AddToFavorite(int adID)
        {
            var user = await _userService.GetUserByPhoneNumberAsync(User.Identity.Name);

            var favorite = new Favorite
            {
                AdID = adID,
                UserID = user.UserID
            };

            await _userService.AddAdToFavoriteAsync(favorite);
        }

        [Authorize]
        [HttpPost]
        public async Task RemoveFromFavorite(int adID)
        {
            var user = await _userService.GetUserByPhoneNumberAsync(User.Identity.Name);
            await _userService.RemoveAdFromFavoriteAsync(user.UserID, adID);
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult> RemoveFromFavoriteWithReload(int adID)
        {
            var user = await _userService.GetUserByPhoneNumberAsync(User.Identity.Name);
            await _userService.RemoveAdFromFavoriteAsync(user.UserID, adID);

           return RedirectToAction("GetFavorites");
        }

        [Authorize]
        [HttpGet]
        public async Task<ActionResult> RemoveFromFavoriteWithReload2(int adID)
        {
            var user = await _userService.GetUserByPhoneNumberAsync(User.Identity.Name);
            await _userService.RemoveAdFromFavoriteAsync(user.UserID, adID);

            return RedirectToAction("GetFavorites");
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult> UpdateUserInfo(UserModel userInfoModel)
        {
            if (!ModelState.IsValid)
            {
                return View(userInfoModel);
            }

            var userInfo = UserMapper.MapToUser(userInfoModel);


            if (userInfoModel.Files != null && userInfoModel.Files.Any())
            {
                var mainImg = GetImageAttachmentForUser(userInfoModel);
                if (mainImg != null)
                {
                    userInfo.MainImage = mainImg.FileName;
                }
            }

            await _userService.UpdateUserInfoAsync(userInfo);

            var user = await _userService.GetUserByIDAsync(userInfo.UserID);

            return RedirectToAction("Profile", new {user.PhoneNumber});
        }

        /// <summary>
        /// Заполняет объект UserModel данными.
        /// Возвращает UserModel.
        /// </summary>
        private static UserModel FillUserModel(RegisterModel registerModel)
        {
            var userModel = new UserModel
            {
                Email = registerModel.Email,
                Password = registerModel.Password,
                FirstName = registerModel.FirstName,
                LastName = registerModel.LastName,
                PhoneNumber = registerModel.PhoneNumber
            };

            return userModel;
        }

        private ImageAttachment GetImageAttachmentForUser(UserModel userModel)
        {
            if (userModel == null || userModel.Files == null)
            {
                return null;
            }

            foreach (var fileJson in userModel.Files)
            {
                if (string.IsNullOrWhiteSpace(fileJson))
                {
                    continue;
                }

                var file = JsonConvert.DeserializeObject<ImageAttachmentModel>(fileJson);

                if (file != null || file.size != 0)
                {
                    var folderPath = Server.MapPath("~/UserImageStore/");

                    var phoneNumber = userModel.PhoneNumber.Substring(1, userModel.PhoneNumber.Length - 1);
                    phoneNumber = phoneNumber.Replace("(", "");
                    phoneNumber = phoneNumber.Replace(")", "");
                    phoneNumber = phoneNumber.Replace("-", "");

                    var fileName = phoneNumber + file.name;

                    var imagePath = folderPath + fileName;

                    using (var ms = new MemoryStream(file.data))
                    {
                        using (var img = Image.FromStream(ms))
                        {
                            img.Save(imagePath);
                        }
                    }

                    var imageAttachment = new ImageAttachment
                    {
                        FileName = fileName
                    };

                    return imageAttachment;
                }
            }

            return null;
        }

    }
}