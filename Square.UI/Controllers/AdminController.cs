﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Security;
using Newtonsoft.Json;
using Square.Common.Entities.Ad;
using Square.Common.Entities.Common;
using Square.OriginDataBase;
using Square.Services.Contracts;
using Square.Services.Contracts.Services;
using Square.UI.Mappers;
using Square.UI.Models;
using Square.UI.Models.AccountEntityModel;
using Square.UI.Models.AdEntityModels;
using Square.UI.Models.Admin;
using AdModel = Square.UI.Models.AdEntityModels.AdModel;
using CityModel = Square.UI.Models.AdEntityModels.CityModel;
using ComfortModel = Square.UI.Models.AdEntityModels.ComfortModel;
using PhoneModel = Square.UI.Models.AdEntityModels.PhoneModel;

namespace Square.UI.Controllers
{
    /// <summary>
    /// Контроллер, для работы с администраторами приложения.
    /// </summary>
    public class AdminController : Controller
    {
        private readonly IUserService _userService;
        private readonly IAdService _adService;
        private readonly ICityService _cityService;

        public AdminController(
            IUserService userService,
            IAdService adService,
            ICityService cityService)
        {
            _userService = userService;
            _adService = adService;
            _cityService = cityService;
        }

        /// <summary>
        /// Возвращает страницу авторизации.
        /// </summary>
        [HttpGet]
        [Authorize(Roles = "Admin")]
        [Route("")]
        public async Task<ActionResult> Index()
        {
            var users = await _userService.GetUsersAsync();
          
            var userModels = users
                .Select(user => UserMapper.MapToUserModel(user))
                .ToList();

            var dashBoardFullInfo = new DashBoardFullInfo
            {
                Users = userModels,
                CurrentSubscriptionsCount = users.Count(user => user.CurrentSubscription != null)
            };

            return View(dashBoardFullInfo);
        }

        //----------------------------------------


        /// <summary>
        /// Возвращает Топ-20 объявлений на странице (по фильтрам).
        /// </summary>
        [HttpGet]
        [Route("")]
        public async Task<ActionResult> AdIndex(
            int page = 1,
            int cityID = 0,
            int districtID = 0,
            int priceFrom = 0,
            int priceTo = 0,
            int roomsCount = 0,
            string search = null,
            int orderedBySelected = 0,
            string roomsCountString = null)
        {

            var roomsCountSelect = new List<string>();
            if (roomsCountString != null)
            {
                roomsCountSelect = roomsCountString.Split('|').ToList();
            }

            var filter = new AdFilter
            {
                CityID = cityID,
                DistrictID = districtID,
                PriceFrom = priceFrom,
                PriceTo = priceTo,
                RoomsCount = roomsCount,
                Search = search,
                OrderedBy = orderedBySelected,
                RoomsCountSelect = roomsCountSelect
            };

            var take = 21;

            var adDataTable = await _adService.GetAdsAsync(filter, page, take);

            var adModels = adDataTable
                .Items
                .Select(AdMapper.MapToAdModel)
                .ToList();

            var cities = await _cityService.GetCitiesAsync();

            var cityModels = cities
                .Select(CityMapper.MapToCityModel)
                .ToList();

            var pager = new Pager(adDataTable.Total, page, take);

            FillLocationInfoForAds(adModels, cityModels);

            var adDataTableModel = GetAdDateTableModel(
                adModels,
                cityModels,
                page,
                cityID,
                districtID,
                priceFrom,
                priceTo,
                roomsCount,
                search,
                adDataTable.Total,
                orderedBySelected,
                pager,
                roomsCountSelect);


            return View("AdIndex", adDataTableModel);
        }

        /// <summary>
        /// Возвращает Топ-20 заявок на странице (по фильтрам).
        /// </summary>
        [Authorize(Roles = "Admin")]
        [HttpGet]
        public async Task<ActionResult> Requests(
            int page = 1,
            int cityID = 0,
            int districtID = 0,
            int priceFrom = 0,
            int priceTo = 0,
            int roomsCount = 0,
            string search = null,
            int orderedBySelected = 0,
            string roomsCountString = null)
        {

            var roomsCountSelect = new List<string>();
            if (roomsCountString != null)
            {
                roomsCountSelect = roomsCountString.Split('|').ToList();
            }

            var filter = new AdFilter
            {
                CityID = cityID,
                DistrictID = districtID,
                PriceFrom = priceFrom,
                PriceTo = priceTo,
                RoomsCount = roomsCount,
                Search = search,
                OrderedBy = orderedBySelected,
                RoomsCountSelect = roomsCountSelect
            };

            var take = 21;

            var adDataTable = await _adService.GetAdRequestsAsync(filter, page, take);

            var adModels = adDataTable
                .Items
                .Select(AdMapper.MapToAdModel)
                .ToList();

            var cities = await _cityService.GetCitiesAsync();

            var cityModels = cities
                .Select(CityMapper.MapToCityModel)
                .ToList();

            var pager = new Pager(adDataTable.Total, page, take);

            FillLocationInfoForAds(adModels, cityModels);

            var adDataTableModel = GetAdDateTableModel(
                adModels,
                cityModels,
                page,
                cityID,
                districtID,
                priceFrom,
                priceTo,
                roomsCount,
                search,
                adDataTable.Total,
                orderedBySelected,
                pager,
                roomsCountSelect);


            return View("Requests", adDataTableModel);
        }

        /// <summary>
        /// Возвращает Топ-20 объявлений в архиве на странице (по фильтрам).
        /// </summary>
        [Authorize(Roles = "Admin")]
        [HttpGet]
        public async Task<ActionResult> Archive(
            int page = 1,
            int cityID = 0,
            int districtID = 0,
            int priceFrom = 0,
            int priceTo = 0,
            int roomsCount = 0,
            string search = null,
            int orderedBySelected = 0,
            string roomsCountString = null)
        {

            var roomsCountSelect = new List<string>();
            if (roomsCountString != null)
            {
                roomsCountSelect = roomsCountString.Split('|').ToList();
            }

            var filter = new AdFilter
            {
                CityID = cityID,
                DistrictID = districtID,
                PriceFrom = priceFrom,
                PriceTo = priceTo,
                RoomsCount = roomsCount,
                Search = search,
                OrderedBy = orderedBySelected,
                RoomsCountSelect = roomsCountSelect
            };

            var take = 21;

            var adDataTable = await _adService.GetAdArchiveAsync(filter, page, take);

            var adModels = adDataTable
                .Items
                .Select(AdMapper.MapToAdModel)
                .ToList();

            var cities = await _cityService.GetCitiesAsync();

            var cityModels = cities
                .Select(CityMapper.MapToCityModel)
                .ToList();

            var pager = new Pager(adDataTable.Total, page, take);

            FillLocationInfoForAds(adModels, cityModels);

            var adDataTableModel = GetAdDateTableModel(
                adModels,
                cityModels,
                page,
                cityID,
                districtID,
                priceFrom,
                priceTo,
                roomsCount,
                search,
                adDataTable.Total,
                orderedBySelected,
                pager,
                roomsCountSelect);


            return View("Archive", adDataTableModel);
        }

        /// <summary>
        /// Возвращает объявление по идентификатору.
        /// </summary>
        [HttpGet]
        public async Task<ActionResult> GetAdInfoByIdAsync(int adId)
        {
            var ad = await _adService.GetAdByIDAsync(adId);

            await _adService.UpdateViewsCountAsync(adId);

            var adModel = AdMapper.MapToAdModel(ad);

            var similarAdModels = ad.SimilarAds.Select(AdMapper.MapToAdModel).ToList();

            adModel.SimilarAds = similarAdModels;


            var cities = await _cityService.GetCitiesAsync();

            var cityModels = cities
                .Select(CityMapper.MapToCityModel)
                .ToList();

            FillLocationInfoForAds(new List<AdModel> { adModel }, cityModels);

            return View("AdInfo", adModel);
        }

        /// <summary>
        /// Возвращает заявку по идентификатору.
        /// </summary>
        [HttpGet]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> GetAdRequestInfoByIdAsync(int adId)
        {
            var ad = await _adService.GetAdRequestByIDAsync(adId);

            var adModel = AdMapper.MapToAdModel(ad);

            var cities = await _cityService.GetCitiesAsync();

            var cityModels = cities
                .Select(CityMapper.MapToCityModel)
                .ToList();

            FillLocationInfoForAds(new List<AdModel> { adModel }, cityModels);

            return View("AdInfo", adModel);
        }

        /// <summary>
        /// Возвращает шаблон для формы добавления заявки (вкл. список городов и районов).
        /// </summary>
        [Authorize(Roles = "Admin")]
        [HttpGet]
        public async Task<ActionResult> Add()
        {
            var cities = await _cityService.GetCitiesAsync();

            var cityModels = cities
                .Select(CityMapper.MapToCityModel)
                .ToList();

            var adModel = new AdModel
            {
                Cities = cityModels,
                Phone = new PhoneModel(),
                ComfortModel = new ComfortModel(),
                LocationInfo = new AdLocationInfoModel()
            };

            return View(adModel);
        }

        /// <summary>
        /// Возвращает окно с подтверждением об удалении объявления.
        /// </summary>
        [Authorize(Roles = "Admin")]
        [HttpGet]
        public async Task<ActionResult> Delete(int adId)
        {
            await _adService.DeleteAdByIDAsync(adId);

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Возвращает страницу для редактирования объявления.
        /// </summary>
        [Authorize(Roles = "Admin")]
        [HttpGet]
        public async Task<ActionResult> Edit(int adId)
        {
            var ad = await _adService.GetAdByIDAsync(adId);

            var cities = await _cityService.GetCitiesAsync();

            var cityModels = cities
                .Select(CityMapper.MapToCityModel)
                .ToList();

            var adModel = AdMapper.MapToAdModel(ad);

            adModel.Cities = cityModels;

            return View("Edit", adModel);
        }

        /// <summary>
        /// Возвращает страницу для редактирования заявки.
        /// </summary>
        [Authorize(Roles = "Admin, Subscriber")]
        [HttpGet]
        public async Task<ActionResult> EditRequest(int adId)
        {
            var ad = await _adService.GetAdRequestByIDAsync(adId);

            var cities = await _cityService.GetCitiesAsync();

            var cityModels = cities
                .Select(CityMapper.MapToCityModel)
                .ToList();

            var adModel = AdMapper.MapToAdModel(ad);

            adModel.Cities = cityModels;

            return View("Edit", adModel);
        }

        /// <summary>
        /// Перемещает объявление из заявок в общий список.
        /// </summary>
        [Authorize(Roles = "Admin")]
        [HttpGet]
        public async Task<ActionResult> MoveAdFromRequestsToAds(AdModel adModel)
        {
            await _adService.MoveAdFromRequestsToAdsAsync(adModel.AdID);

            return RedirectToAction("Requests");
        }

        /// <summary>
        /// Перемещает объявление из архива в общий список.
        /// </summary>
        [Authorize(Roles = "Admin")]
        [HttpGet]
        public async Task<ActionResult> MoveAdFromArchiveToAds(AdModel adModel)
        {
            await _adService.MoveAdFromArchiveToAdsAsync(adModel.AdID);

            return RedirectToAction("Archive");
        }

        /// <summary>
        /// Возвращает Топ-20 объявлений на странице (по фильтрам).
        /// </summary>
        [HttpPost]
        public async Task<ActionResult> AdIndex(DataTableModel<AdModel> dataTableModelFrom)
        {
            var filerModel = dataTableModelFrom.Filter;

            var filter = new AdFilter
            {
                CityID = filerModel.CityID,
                DistrictID = filerModel.DistrictID,
                PriceFrom = filerModel.PriceFrom,
                PriceTo = filerModel.PriceTo,
                RoomsCount = filerModel.RoomsCount,
                Search = filerModel.Search,
                OrderedBy = filerModel.OrderedBySelected,
                PhoneNumber = filerModel.PhoneNumber,
                RoomsCountSelect = filerModel.RoomsCountSelect
            };

            var take = 21;
            var adDataTable = await _adService.GetAdsAsync(filter, dataTableModelFrom.Page, take);

            var adModels = adDataTable
                .Items
                .Select(AdMapper.MapToAdModel)
                .ToList();

            var cities = await _cityService.GetCitiesAsync();

            var cityModels = cities
                .Select(CityMapper.MapToCityModel)
                .ToList();
            var pager = new Pager(adDataTable.Total, dataTableModelFrom.Page, take);

            FillLocationInfoForAds(adModels, cityModels);

            var adDataTableModel = GetAdDataTableModelForPost(
                adModels,
                cityModels,
                filerModel,
                dataTableModelFrom.Page,
                adDataTable.Total,
                pager);

            return View("AdIndex", adDataTableModel);
        }

        /// <summary>
        /// Возвращает Топ-20 заявок на странице (по фильтрам).
        /// </summary>
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<ActionResult> Requests(DataTableModel<AdModel> dataTableModelFrom)
        {
            var filerModel = dataTableModelFrom.Filter;

            var filter = new AdFilter
            {
                CityID = filerModel.CityID,
                DistrictID = filerModel.DistrictID,
                PriceFrom = filerModel.PriceFrom,
                PriceTo = filerModel.PriceTo,
                RoomsCount = filerModel.RoomsCount,
                Search = filerModel.Search,
                OrderedBy = filerModel.OrderedBySelected,
                PhoneNumber = filerModel.PhoneNumber,
                RoomsCountSelect = filerModel.RoomsCountSelect
            };

            var take = 21;
            var adDataTable = await _adService.GetAdRequestsAsync(filter, dataTableModelFrom.Page, take);

            var adModels = adDataTable
                .Items
                .Select(AdMapper.MapToAdModel)
                .ToList();

            var cities = await _cityService.GetCitiesAsync();

            var cityModels = cities
                .Select(CityMapper.MapToCityModel)
                .ToList();
            var pager = new Pager(adDataTable.Total, dataTableModelFrom.Page, take);

            FillLocationInfoForAds(adModels, cityModels);

            var adDataTableModel = GetAdDataTableModelForPost(
                adModels,
                cityModels,
                filerModel,
                dataTableModelFrom.Page,
                adDataTable.Total,
                pager);

            return View("Requests", adDataTableModel);
        }

        /// <summary>
        /// Добавляет новое объявление.
        /// </summary>
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<ActionResult> Add(AdModel adModel)
        {
            if (!ModelState.IsValid)
            {
                var cities = await _cityService.GetCitiesAsync();

                var cityModels = cities
                    .Select(CityMapper.MapToCityModel)
                    .ToList();

                adModel.Cities = cityModels;

                return View(adModel);
            }

            var ad = AdMapper.MapToAd(adModel);

            if (adModel.Files != null && adModel.Files.Any())
            {
                ad.ImageAttachments = GetImageAttachmentsForAd(adModel, ad);
            }

            await _adService.AddNewAdAsync(ad);

            return RedirectToAction("AdIndex");
        }

        /// <summary>
        /// Редактирует объявление.
        /// </summary>
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<ActionResult> Edit(AdModel adModel)
        {
            var ad = AdMapper.MapToAd(adModel);

            await _adService.UpdateAdAsync(ad);

            return RedirectToAction("GetAdInfoByIdAsync", new { adId = ad.AdID });
        }


        [Authorize(Roles = "Admin")]
        [HttpGet]
        public async Task<ActionResult> GetAdReports()
        {

            var adReports = await _adService.GetAdReportsAsync();

            var adReportModels = adReports
                .Select(adReport => new AddReportModel
            {
                AdId = adReport.AdId,
                IncorrectPhoneNumber = adReport.IncorrectPhoneNumber,
                AdLeased = adReport.AdLeased,
                AdIncorrectData = adReport.AdIncorrectData,
                AdFromRealtor = adReport.AdFromRealtor,
                AdReportId = adReport.AdReportId

            }).ToList();

            return View("GetAdReports", adReportModels);
        }


        private static void FillLocationInfoForAds(
            IEnumerable<AdModel> adModels,
            IEnumerable<CityModel> cityModels)
        {
            if (adModels == null || adModels.Count() == 0)
            {
                return;
            }

            if (cityModels == null || cityModels.Count() == 0)
            {
                return;
            }

            foreach (var adModel in adModels)
            {
                var city = cityModels.FirstOrDefault(cityModel => cityModel.CityID == adModel.LocationInfo.CityID);
                if (city != null)
                {
                    adModel.LocationInfo.CityName = city.Name;

                    var district = city.Districts.FirstOrDefault(districtModel =>
                        districtModel.DistrictID == adModel.LocationInfo.DistrictID);
                    if (district != null)
                    {
                        adModel.LocationInfo.DistrictName = district.Name;
                    }
                }
            }
        }
        private static DataTableModel<AdModel> GetAdDateTableModel(
            IEnumerable<AdModel> adModels,
            IEnumerable<CityModel> cityModels,
            int page,
            int cityID,
            int districtID,
            int priceFrom,
            int priceTo,
            int roomsCount,
            string search,
            int adTotalItems,
            int orderedBySelected,
            Pager pager = null,
            List<string> roomsCountSelect = null)
        {
            var adDataTableModel = new DataTableModel<AdModel>
            {
                Items = adModels,
                PageInfo = new PageInfoModel
                {
                    PageNumber = page,
                    PageSize = 20,
                    TotalItems = adTotalItems
                },
                Filter = new FilterModel
                {
                    Cities = cityModels,
                    CityID = cityID,
                    DistrictID = districtID,
                    PriceFrom = priceFrom,
                    PriceTo = priceTo,
                    RoomsCount = roomsCount,
                    Search = search,
                    OrderedBySelected = orderedBySelected,
                    RoomsCountSelect = roomsCountSelect
                },
                Pager = pager
            };

            return adDataTableModel;
        }

        private static DataTableModel<AdModel> GetAdDataTableModelForPost(
            IEnumerable<AdModel> adModels,
            IEnumerable<CityModel> cityModels,
            FilterModel filerModel,
            int page,
            int adTotalItems,
            Pager pager = null)
        {
            var adDataTableModel = new DataTableModel<AdModel>
            {
                Items = adModels,
                PageInfo = new PageInfoModel
                {
                    PageNumber = page,
                    PageSize = 20,
                    TotalItems = adTotalItems
                },
                Filter = new FilterModel
                {
                    Cities = cityModels,
                    CityID = filerModel.CityID,
                    DistrictID = filerModel.DistrictID,
                    PriceFrom = filerModel.PriceFrom,
                    PriceTo = filerModel.PriceTo,
                    RoomsCount = filerModel.RoomsCount,
                    Search = string.IsNullOrWhiteSpace(filerModel.Search)
                            ? string.Empty
                            : filerModel.Search,
                    OrderedBySelected = filerModel.OrderedBySelected,
                    PhoneNumber = filerModel.PhoneNumber,
                    RoomsCountSelect = filerModel.RoomsCountSelect
                },
                Pager = pager
            };

            return adDataTableModel;
        }

        private List<ImageAttachment> GetImageAttachmentsForAd(AdModel adModel, Ad ad)
        {
            if (adModel == null || ad == null || adModel.Files == null)
            {
                return new List<ImageAttachment>();
            }

            var imageAttachments = new List<ImageAttachment>();
            var num = 13;
            var ismain = true;

            foreach (var fileJson in adModel.Files)
            {
                if (string.IsNullOrWhiteSpace(fileJson))
                {
                    continue;
                }

                var file = JsonConvert.DeserializeObject<ImageAttachmentModel>(fileJson);

                if (file != null || file.size != 0)
                {
                    var folderPath = Server.MapPath($"~/AdImagesStore/");

                    var phoneNumber = ad.Phone.PhoneNumber.Substring(1, ad.Phone.PhoneNumber.Length - 1);
                    phoneNumber = phoneNumber.Replace("(", "");
                    phoneNumber = phoneNumber.Replace(")", "");
                    phoneNumber = phoneNumber.Replace("-", "");

                    var fileName = phoneNumber + file.name;

                    var imagePath = folderPath + fileName;

                    using (var ms = new MemoryStream(file.data))
                    {
                        using (var img = Image.FromStream(ms))
                        {
                            if (ismain)
                            {
                                var mainFolderPath = Server.MapPath($@"~/MainImages/");
                                var newFName = mainFolderPath + fileName;

                                img.Save(newFName);
                            }

                            img.Save(imagePath);
                        }
                    }

                    var imageAttachment = new ImageAttachment
                    {
                        FileName = $@"AdImagesStore/{fileName}",
                        IsMain = ismain
                    };

                    if (ismain)
                    {
                        imageAttachment.NewFileName = $@"MainImages/{fileName}";
                        ismain = false;
                    }

                    imageAttachments.Add(imageAttachment);
                }
            }

            return imageAttachments;
        }
    }
}