﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

using Newtonsoft.Json;
using Square.Common.Entities.Ad;
using Square.Common.Entities.Common;
using Square.Services.Contracts.Services;
using Square.UI.Mappers;
using Square.UI.Models;
using Square.UI.Models.AccountEntityModel;
using Square.UI.Models.AdEntityModels;

namespace Square.UI.Controllers
{
    /// <summary>
    /// Контроллер, для работы с объявлениями.
    /// </summary>
    [RoutePrefix("ads")]
    public class AdController : Controller
    {
        private readonly IAdService _adService;
        private readonly ICityService _cityService;
        private readonly IUserService _userService;

        public AdController(
            IAdService adService,
            ICityService cityService,
            IUserService userService)
        {
            _adService = adService;
            _cityService = cityService;
            _userService = userService;
        }

        /// <summary>
        /// Возвращает Топ-20 объявлений на странице (по фильтрам).
        /// </summary>
        [HttpGet]
        [Route("")]
        public async Task<ActionResult> Index(
            int page = 1,
            int cityID = 0,
            int districtID = 0,
            int priceFrom = 0,
            int priceTo = 0,
            int roomsCount = 0,
            string search = null,
            int orderedBySelected = 0,
            string roomsCountString = null)
        {
        
            var roomsCountSelect = new List<string>();
            if (roomsCountString != null)
            {
                roomsCountSelect = roomsCountString.Split('|').ToList();
            }

            var userID = 0;
            if (User.Identity.IsAuthenticated)
            {
                var user = await _userService.GetUserByPhoneNumberAsync(User.Identity.Name);
                userID = user.UserID;
            }

            var filter = new AdFilter
            {
                CityID = cityID,
                DistrictID = districtID,
                PriceFrom = priceFrom,
                PriceTo = priceTo,
                RoomsCount = roomsCount,
                Search = search,
                OrderedBy = orderedBySelected,
                RoomsCountSelect = roomsCountSelect,
                UserID = userID
            };

            var take = 24;

            var adDataTable = await _adService.GetAdsAsync(filter, page, take);

            var adModels = adDataTable
                .Items
                .Select(AdMapper.MapToAdModel)
                .ToList();

            var cities = await _cityService.GetCitiesAsync();

            var cityModels = cities
                .Select(CityMapper.MapToCityModel)
                .ToList();

            var pager = new Pager(adDataTable.Total, page, take);

            FillLocationInfoForAds(adModels, cityModels);

            var adDataTableModel = GetAdDateTableModel(
                adModels,
                cityModels,
                page,
                cityID,
                districtID,
                priceFrom,
                priceTo,
                roomsCount,
                search,
                adDataTable.Total,
                orderedBySelected,
                pager,
                roomsCountSelect);

            return View(adDataTableModel);
        }

        /// <summary>
        /// Возвращает Топ-20 объявлений из архива на странице (по фильтрам).
        /// </summary>
        [HttpGet]
        public async Task<ActionResult> Archive(
            int page = 1,
            int cityID = 0,
            int districtID = 0,
            int priceFrom = 0,
            int priceTo = 0,
            int roomsCount = 0,
            string search = null,
            int orderedBySelected = 0,
            string roomsCountString = null)
        {

            var roomsCountSelect = new List<string>();
            if (roomsCountString != null)
            {
                roomsCountSelect = roomsCountString.Split('|').ToList();
            }

            var filter = new AdFilter
            {
                CityID = cityID,
                DistrictID = districtID,
                PriceFrom = priceFrom,
                PriceTo = priceTo,
                RoomsCount = roomsCount,
                Search = search,
                OrderedBy = orderedBySelected,
                RoomsCountSelect = roomsCountSelect
            };

            var take = 21;

            var adDataTable = await _adService.GetAdsFromArchiveAsync(filter, page, take);

            var adModels = adDataTable
                .Items
                .Select(AdMapper.MapToAdModel)
                .ToList();

            var cities = await _cityService.GetCitiesAsync();

            var cityModels = cities
                .Select(CityMapper.MapToCityModel)
                .ToList();

            var pager = new Pager(adDataTable.Total, page, take);

            FillLocationInfoForAds(adModels, cityModels);

            var adDataTableModel = GetAdDateTableModel(
                adModels,
                cityModels,
                page,
                cityID,
                districtID,
                priceFrom,
                priceTo,
                roomsCount,
                search,
                adDataTable.Total,
                orderedBySelected,
                pager,
                roomsCountSelect);


            return View(adDataTableModel);
        }

        [HttpGet]
        public async Task<ActionResult> AdCommerce()
        {
            return View();
        }

        /// <summary>
        /// Возвращает объявление по идентификатору.
        /// </summary>
        [HttpGet]
        public async Task<ActionResult> GetAdInfoByIdAsync(int adId)
        {
            if (User.IsInRole("Guest"))
            {
                return RedirectToAction("Index");
            }

            var ad = await _adService.GetAdByIDAsync(adId);

            await _adService.UpdateViewsCountAsync(adId);

            if (User.Identity.IsAuthenticated && User.IsInRole("User"))
            {
                var user = await _userService.GetUserByPhoneNumberAsync(User.Identity.Name);
                await _userService.UpdateFreeViewsAsync(user.UserID);
            }

            var adModel = AdMapper.MapToAdModel(ad);
            
            var similarAdModels = ad.SimilarAds.Select(AdMapper.MapToAdModel).ToList();

            adModel.SimilarAds = similarAdModels;


            var cities = await _cityService.GetCitiesAsync();

            var cityModels = cities
                .Select(CityMapper.MapToCityModel)
                .ToList();

            FillLocationInfoForAds(new List<AdModel> { adModel }, cityModels);


            if (adModel.IsAvito)
            {
                var ph = adModel.Phone.PhoneNumber;
                ph = ph.Insert(2, "(");
                ph = ph.Insert(6, ")");
                ph = ph.Insert(10, "-");
                ph = ph.Insert(13, "-");

                adModel.Phone.PhoneNumber = ph;
            }

            return View("Info", adModel);
        }

        /// <summary>
        /// Возвращает объявление из архива по идентификатору.
        /// </summary>
        [Authorize]
        [HttpGet]
        public async Task<ActionResult> GetAdArchiveInfoByIdAsync(int adId)
        {
            var ad = await _adService.GetAdArchiveByIDAsync(adId);

            await _adService.UpdateViewsCountAsync(adId);

            var adModel = AdMapper.MapToAdModel(ad);

            var similarAdModels = ad.SimilarAds.Select(AdMapper.MapToAdModel).ToList();

            adModel.SimilarAds = similarAdModels;


            var cities = await _cityService.GetCitiesAsync();

            var cityModels = cities
                .Select(CityMapper.MapToCityModel)
                .ToList();

            FillLocationInfoForAds(new List<AdModel> { adModel }, cityModels);

            return View("InfoArchive", adModel);
        }


        /// <summary>
        /// Возвращает окно с подтверждением об удалении объявления.
        /// </summary>
        [Authorize(Roles = "Admin")]
        [HttpGet]
        public async Task<ActionResult> Delete(int adId)
        {
            await _adService.DeleteAdByIDAsync(adId);

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Возвращает страницу для добавления заявки на объявление.
        /// </summary>
        [HttpGet]
        public async Task<ActionResult> AddRequest()
        {
            var cities = await _cityService.GetCitiesAsync();

            var cityModels = cities
                .Select(CityMapper.MapToCityModel)
                .ToList();

            var adModel = new AdModel
            {
                Cities = cityModels,
                Phone = new PhoneModel(),
                ComfortModel = new ComfortModel(),
                LocationInfo = new AdLocationInfoModel(),
                RoomsCountModel = new RoomsCountModel
                {
                    One = true
                }
            };

            return View("AddRequest", adModel);
        }

        /// <summary>
        /// Добавляет новое объявление.
        /// </summary>
        [Authorize]
        [HttpPost]
        public async Task<ActionResult> AddRequest(AdModel adModel)
        {
            var errors = new List<ErrorTypeEnum>();


            if (!ModelState.IsValid)
            {
                var cities = await _cityService.GetCitiesAsync();

                var cityModels = cities
                    .Select(CityMapper.MapToCityModel)
                    .ToList();

                adModel.Cities = cityModels;

                if (!IsRoomsCountNotNull(adModel.RoomsCountModel))
                {
                    errors.Add(ErrorTypeEnum.RoomsCountNull);

                    adModel.Errors = errors;
                }

                return View(adModel);
            } 

            if (!IsRoomsCountNotNull(adModel.RoomsCountModel))
            {
                errors.Add(ErrorTypeEnum.RoomsCountNull);

                adModel.Errors = errors;
                return View(adModel);
            }

            var ad = AdMapper.MapToAd(adModel);

            if (adModel.Files != null && adModel.Files.Any())
            {
                ad.ImageAttachments = GetImageAttachmentsForAd(adModel, ad);
            }

            await _adService.AddAdRequestAsync(ad);

            return View("RequestAddedNotify");
        }

        private static bool IsRoomsCountNotNull(RoomsCountModel roomsCountModel)
        {
            if (roomsCountModel == null)
            {
                return false;
            }

            return roomsCountModel.One || roomsCountModel.Two || roomsCountModel.Tree || roomsCountModel.FourAndMore;
        }


        /// <summary>
        /// Возвращает Топ-20 объявлений на странице (по фильтрам).
        /// </summary>
        [HttpPost]
        public async Task<ActionResult> Index(DataTableModel<AdModel> dataTableModelFrom)
        {
            var filerModel = dataTableModelFrom.Filter;

            var userID = 0;
            if (User.Identity.IsAuthenticated)
            {
                var user = await _userService.GetUserByPhoneNumberAsync(User.Identity.Name);
                userID = user.UserID;
            }

            var filter = new AdFilter
            {
                CityID = filerModel.CityID,
                DistrictID = filerModel.DistrictID,
                PriceFrom = filerModel.PriceFrom,
                PriceTo = filerModel.PriceTo,
                RoomsCount = filerModel.RoomsCount,
                Search = filerModel.Search,
                OrderedBy = filerModel.OrderedBySelected,
                PhoneNumber = filerModel.PhoneNumber,
                RoomsCountSelect =  filerModel.RoomsCountSelect,
                UserID = userID
            };

            var take = 24;
            var adDataTable = await _adService.GetAdsAsync(filter, dataTableModelFrom.Page, take);

            var adModels = adDataTable
                .Items
                .Select(AdMapper.MapToAdModel)
                .ToList();

            var cities = await _cityService.GetCitiesAsync();

            var cityModels = cities
                .Select(CityMapper.MapToCityModel)
                .ToList();
            var pager = new Pager(adDataTable.Total, dataTableModelFrom.Page, take);

            FillLocationInfoForAds(adModels, cityModels);

            var adDataTableModel = GetAdDataTableModelForPost(
                adModels,
                cityModels, 
                filerModel,
                dataTableModelFrom.Page,
                adDataTable.Total,
                pager);

            return View(adDataTableModel);
        }

        /// <summary>
        /// Возвращает Топ-20 объявлений из арихва на странице (по фильтрам).
        /// </summary>
        [HttpPost]
        public async Task<ActionResult> Archive(DataTableModel<AdModel> dataTableModelFrom)
        {
            var filerModel = dataTableModelFrom.Filter;

            var filter = new AdFilter
            {
                CityID = filerModel.CityID,
                DistrictID = filerModel.DistrictID,
                PriceFrom = filerModel.PriceFrom,
                PriceTo = filerModel.PriceTo,
                RoomsCount = filerModel.RoomsCount,
                Search = filerModel.Search,
                OrderedBy = filerModel.OrderedBySelected,
                PhoneNumber = filerModel.PhoneNumber,
                RoomsCountSelect = filerModel.RoomsCountSelect
            };

            var take = 21;
            var adDataTable = await _adService.GetAdsFromArchiveAsync(filter, dataTableModelFrom.Page, take);

            var adModels = adDataTable
                .Items
                .Select(AdMapper.MapToAdModel)
                .ToList();

            var cities = await _cityService.GetCitiesAsync();

            var cityModels = cities
                .Select(CityMapper.MapToCityModel)
                .ToList();
            var pager = new Pager(adDataTable.Total, dataTableModelFrom.Page, take);

            FillLocationInfoForAds(adModels, cityModels);

            var adDataTableModel = GetAdDataTableModelForPost(
                adModels,
                cityModels,
                filerModel,
                dataTableModelFrom.Page,
                adDataTable.Total,
                pager);

            return View(adDataTableModel);
        }

        /// <summary>
        /// Редактирует объявление.
        /// </summary>
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<ActionResult> Edit(AdModel adModel)
        {
            var ad = AdMapper.MapToAd(adModel);

            await _adService.UpdateAdAsync(ad);

            return RedirectToAction("GetAdInfoByIdAsync", new {adId = ad.AdID} );
        }


        [Authorize]
        [HttpGet]
        public async Task<ActionResult> AdReport(int adId)
        {
            return View(adId);
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult> AdReportPost(AddReportModel addReport)
        {
            var adReportInfo = new AdReport
            {
                AdId = addReport.AdId,
                AdLeased = addReport.AdLeased,
                AdIncorrectData = addReport.AdIncorrectData,
                AdFromRealtor = addReport.AdFromRealtor,
                IncorrectPhoneNumber = addReport.IncorrectPhoneNumber
            };


            await _adService.AddAdReportAsync(adReportInfo);

            return RedirectToAction("GetAdInfoByIdAsync", new { adId = addReport.AdId });
        }

        private static void FillLocationInfoForAds(
            IEnumerable<AdModel> adModels,
            IEnumerable<CityModel> cityModels)
        {
            if (adModels == null || adModels.Count() == 0)
            {
                return;
            }

            if (cityModels == null || cityModels.Count() == 0)
            {
                return;
            }

            foreach (var adModel in adModels)
            {
                var city = cityModels.FirstOrDefault(cityModel => cityModel.CityID == adModel.LocationInfo.CityID);
                if (city != null)
                {
                    adModel.LocationInfo.CityName = city.Name;

                    var district = city.Districts.FirstOrDefault(districtModel =>
                        districtModel.DistrictID == adModel.LocationInfo.DistrictID);
                    if (district != null)
                    {
                        adModel.LocationInfo.DistrictName = district.Name;
                    }
                }
            }
        }

        private static DataTableModel<AdModel> GetAdDateTableModel(
            IEnumerable<AdModel> adModels,
            IEnumerable<CityModel> cityModels,
            int page,
            int cityID,
            int districtID,
            int priceFrom,
            int priceTo,
            int roomsCount,
            string search,
            int adTotalItems,
            int orderedBySelected,
            Pager pager = null,
            List<string> roomsCountSelect = null)
        {
            var adDataTableModel = new DataTableModel<AdModel>
            {
                Items = adModels,
                PageInfo = new PageInfoModel
                {
                    PageNumber = page,
                    PageSize = 20,
                    TotalItems = adTotalItems
                },
                Filter = new FilterModel
                {
                    Cities = cityModels,
                    CityID = cityID,
                    DistrictID = districtID,
                    PriceFrom = priceFrom,
                    PriceTo = priceTo,
                    RoomsCount = roomsCount,
                    Search = search,
                    OrderedBySelected = orderedBySelected,
                    RoomsCountSelect = roomsCountSelect
                },
                Pager = pager
            };

            return adDataTableModel;
        }

        private static DataTableModel<AdModel> GetAdDataTableModelForPost(
            IEnumerable<AdModel> adModels,
            IEnumerable<CityModel> cityModels,
            FilterModel filerModel,
            int page,
            int adTotalItems,
            Pager pager = null)
        {
            var adDataTableModel = new DataTableModel<AdModel>
            {
                Items = adModels,
                PageInfo = new PageInfoModel
                {
                    PageNumber = page,
                    PageSize = 20,
                    TotalItems = adTotalItems
                },
                Filter = new FilterModel
                {
                    Cities = cityModels,
                    CityID = filerModel.CityID,
                    DistrictID = filerModel.DistrictID,
                    PriceFrom = filerModel.PriceFrom,
                    PriceTo = filerModel.PriceTo,
                    RoomsCount = filerModel.RoomsCount,
                    Search = string.IsNullOrWhiteSpace(filerModel.Search)
                            ? string.Empty
                            : filerModel.Search,
                    OrderedBySelected = filerModel.OrderedBySelected,
                    PhoneNumber = filerModel.PhoneNumber,
                    RoomsCountSelect = filerModel.RoomsCountSelect
                },
                Pager = pager
            };

            return adDataTableModel;
        }

        private List<ImageAttachment> GetImageAttachmentsForAd(AdModel adModel, Ad ad)
        {
            if (adModel == null || ad == null || adModel.Files == null)
            {
                return new List<ImageAttachment>();
            }

            var imageAttachments = new List<ImageAttachment>();
            var ismain = true;

            foreach (var fileJson in adModel.Files)
            {
                if (string.IsNullOrWhiteSpace(fileJson))
                {
                    continue;
                }

                var file = JsonConvert.DeserializeObject<ImageAttachmentModel>(fileJson);

                if (file != null || file.size != 0)
                {
                    var folderPath = Server.MapPath($"~/AdImagesStore/");
                    
                    var phoneNumber = ad.Phone.PhoneNumber.Substring(1, ad.Phone.PhoneNumber.Length - 1);
                    phoneNumber = phoneNumber.Replace("(", "");
                    phoneNumber = phoneNumber.Replace(")", "");
                    phoneNumber = phoneNumber.Replace("-", "");

                    var fileName = phoneNumber + file.name;
                 
                    var imagePath = folderPath + fileName;

                    using (var ms = new MemoryStream(file.data))
                    {
                        using (var img = Image.FromStream(ms))
                        {
                            if (ismain)
                            {
                                var mainFolderPath = Server.MapPath($@"~/MainImages/");
                                var newFName = mainFolderPath + fileName;

                                img.Save(newFName);
                            }

                            img.Save(imagePath);
                        }
                    }
                    
                    var imageAttachment = new ImageAttachment
                    {
                        FileName = $@"AdImagesStore/{fileName}",
                        IsMain = ismain
                    };

                    if (ismain)
                    {
                        imageAttachment.NewFileName = $@"MainImages/{fileName}";
                        ismain = false;
                    }

                    imageAttachments.Add(imageAttachment);
                }
            }

            return imageAttachments;
        }
    }
}