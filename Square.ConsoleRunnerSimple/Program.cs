﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Square.ConsoleRunnerSimple
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var ap = new Common.Entities.Ad.Ad();
            var s = new Square
            await ap.ParseAsync();
        }
    }

    public class AvitoXlsx
    {
        public string Title { get; set; }

        public string PhoneNumber { get; set; }

        public string Fio { get; set; }

        public bool IsOwner { get; set; }

        public string Description { get; set; }

        public double Price { get; set; }

        public string Metro { get; set; }

        public string Address { get; set; }

        public int RoomsCount { get; set; }

        public double Longtitude { get; set; }

        public double Latitude { get; set; }

        public List<string> ImgLinks { get; set; }
    }

    public class AvitoParser
    {


        public AvitoParser()
        {
        }

        private const string originalFileName = @"C:\temp\avito_211498.xlsx";

        public async Task ParseAsync()
        {
            using (var stream = System.IO.File.Open(originalFileName, System.IO.FileMode.Open, System.IO.FileAccess.Read))
            {
                ExcelDataReader.IExcelDataReader reader;

                reader = ExcelDataReader.ExcelReaderFactory.CreateReader(stream);

                var conf = new ExcelDataSetConfiguration
                {
                    ConfigureDataTable = _ => new ExcelDataTableConfiguration
                    {
                        UseHeaderRow = true
                    }
                };

                var dataSet = reader.AsDataSet(conf);

                var dataTable = dataSet.Tables[0];

                var list = new List<AvitoXlsx>();

                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    var avitoAd = new AvitoXlsx();

                    avitoAd.PhoneNumber = dataTable.Rows[i]["Телефон"] != DBNull.Value
                        ? (string)dataTable.Rows[i]["Телефон"]
                        : null;

                    avitoAd.Description = dataTable.Rows[i]["Описание"] != DBNull.Value
                        ? (string)dataTable.Rows[i]["Описание"]
                        : null;


                    avitoAd.Fio = dataTable.Rows[i]["Продавец"] != DBNull.Value
                        ? (string)dataTable.Rows[i]["Продавец"]
                        : null;

                    avitoAd.IsOwner = dataTable.Rows[i]["Тип продавца"] != DBNull.Value
                        ? (string)dataTable.Rows[i]["Тип продавца"] == "Арендодатель" ? true : false
                        : false;



                    if (dataTable.Rows[i]["Адрес"] != DBNull.Value)
                    {
                        var address = (string)dataTable.Rows[i]["Адрес"];
                        address = address.Replace("Москва, ", "");
                        avitoAd.Address = address;
                    }
                    else
                    {
                        avitoAd.Address = null;
                    }

                    avitoAd.Price = dataTable.Rows[i]["Цена"] != DBNull.Value
                        ? (double)dataTable.Rows[i]["Цена"]
                        : 0;


                    if (dataTable.Rows[i]["Метро"] != DBNull.Value)
                    {
                        var metro = (string)dataTable.Rows[i]["Метро"];
                        metro = metro.Replace("м. ", "");
                        avitoAd.Metro = metro;
                    }
                    else
                    {
                        avitoAd.Metro = null;
                    }


                    if (dataTable.Rows[i]["Ссылки на картинки"] != DBNull.Value)
                    {
                        var linksImgs = (string)dataTable.Rows[i]["Ссылки на картинки"];

                        avitoAd.ImgLinks = linksImgs.Replace(" ", "").Split(',').ToList();

                    }
                    else
                    {
                        avitoAd.ImgLinks = null;
                    }

                    string roomsCountStr = dataTable.Rows[i]["Параметры"] != DBNull.Value
                        ? (string)dataTable.Rows[i]["Параметры"]
                        : null;
                    if (!string.IsNullOrEmpty(roomsCountStr))
                    {
                        var ar = roomsCountStr.Split(';');
                        var room = ar.FirstOrDefault(r => r.Contains("Количество комнат"));
                        if (!string.IsNullOrEmpty(room))
                        {
                            var temp = room.Split(':');
                            var roomCountParsedStr = temp[1].Trim();
                            int.TryParse(roomCountParsedStr, out int roomc);

                            if (roomc != 0)
                            {
                                avitoAd.RoomsCount = roomc;
                            }
                            else
                            {
                                avitoAd.RoomsCount = 1;
                            }

                        }
                        else
                        {
                            avitoAd.RoomsCount = 1;
                        }
                    }
                    else
                    {
                        avitoAd.RoomsCount = 1;
                    }



                    list.Add(avitoAd);
                }

                list = list.Where(l => l.IsOwner && l.RoomsCount > 0).ToList();

                var ads = await MapAvitoAdsToAds(list);

                var te = ads;
            }
        }

        public async Task<List<Common.Entities.Ad.Ad>> MapAvitoAdsToAds(List<AvitoXlsx> avitoAds)
        {
            var r = new Services.Implementation.Repositories.AdRepository();
            var c = new Services.Implementation.Repositories.CityRepository();

            var cities = await c.GetCitiesAsync();
            var city = cities.First();
            var metroes = city.Districts;

            var ads = new List<Common.Entities.Ad.Ad>();

            foreach (var avitoAd in avitoAds)
            {
                var metro = metroes.FirstOrDefault(m => m.Name == avitoAd.Metro);
                if (metro == null)
                {
                    continue;
                }

                var ad = new Common.Entities.Ad.Ad
                {
                    Title = avitoAd.Address,
                    Description = avitoAd.Description,
                    Phone = new Common.Entities.Ad.AdPhone
                    {
                        PhoneNumber = avitoAd.PhoneNumber
                    },
                    Fio = avitoAd.Fio,
                    Price = (int)avitoAd.Price,
                    RoomsCount = avitoAd.RoomsCount,
                    Comfort = new Common.Entities.Ad.Comfort
                    {
                        Internet = true,
                        Bath = true,
                        Fridge = true,
                        KitchenFurniture = true,
                        Washer = true
                    },
                    IsAvito = true,

                    ImageAttachments = avitoAd.ImgLinks
                        .Select(item => new Common.Entities.Common.ImageAttachment
                        {
                            NewFileName = item,
                            OldFileName = item
                        })
                        .ToList(),

                    LocationInfo = new Common.Entities.Ad.AdLocationInfo
                    {
                        CityID = 1,
                        DistrictID = metro.DistrictID
                    }
                };

                ads.Add(ad);
            }

            return ads;
        }
    }
}
